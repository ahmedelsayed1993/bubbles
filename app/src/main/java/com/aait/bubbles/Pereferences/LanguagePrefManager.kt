package com.aait.bubbles.Pereferences

import android.content.Context

class LanguagePrefManager(private val mContext: Context) {

    // return sharedPreferences.getString(App_LANGUAGE, Locale.getDefault().getLanguage());
    var appLanguage: String
        get() {
            val sharedPreferences = mContext.getSharedPreferences(
                SHARED_PREF_NAME, 0
            )
            return sharedPreferences.getString(App_LANGUAGE, "ar")!!
        }
        set(language) {
            val sharedPreferences = mContext.getSharedPreferences(SHARED_PREF_NAME, 0)
            val editor = sharedPreferences.edit()
            editor.putString(App_LANGUAGE, language)
            editor.apply()
        }

    companion object {
        private val SHARED_PREF_NAME = "Mazad_pref"
        private val App_LANGUAGE = "Mazad_language"
    }
}
