package com.aait.bubbles.Pereferences

import android.content.Context
import android.content.SharedPreferences

import com.aait.bubbles.Models.UserModel
import com.google.gson.Gson


/**
 * Created by Ahmed El_sayed on 16/1/2019.
 */


class SharedPrefManager(internal var context:Context) {

    internal var mSharedPreferences: SharedPreferences

    internal var mEditor: SharedPreferences.Editor


    var loginStatus: Boolean?
        get() = mSharedPreferences.getBoolean("car_login", false)
        set(status) {
            mEditor.putBoolean("car_login", status!!)
            mEditor.commit()
        }

    var notificationStatus: Boolean?
        get() = mSharedPreferences.getBoolean("Notification", true)
        set(status) {
            mEditor.putBoolean("Notification", status!!)
            mEditor.commit()
        }

    var userData: UserModel
        get() {
            val gson = Gson()
            return gson.fromJson(mSharedPreferences.getString("USER", null), UserModel::class.java)
        }
        set(userModel) {
            mEditor.putString("USER", Gson().toJson(userModel))
            mEditor.apply()
        }

    init {
        mSharedPreferences = context.getSharedPreferences("Wasite_pref", Context.MODE_PRIVATE)
        mEditor = mSharedPreferences.edit()
    }


    fun Logout() {
        mEditor.clear()
        mEditor.apply()
    }
}
