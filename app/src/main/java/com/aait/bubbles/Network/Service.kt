package com.aait.bubbles.Network

import com.aait.bubbles.Models.*
import com.google.android.material.chip.ChipDrawable
import okhttp3.MultipartBody
import retrofit2.Call
import retrofit2.http.*
import java.lang.AssertionError

interface Service {
    @POST("about")
    fun About(@Header("lang") lang: String): Call<TermsResponse>

    @POST("terms")
    fun Terms(@Header("lang") lang: String): Call<TermsResponse>
    @POST("contact-us")
    fun CallUs(@Header("lang") lang:String):Call<CallUsResponse>
    @FormUrlEncoded
    @POST("complaints")
    fun Complaints(@Header("lang") lang:String,
                   @Field("name") name:String,
                   @Field("phone") phone:String,
                   @Field("subject") title:String,
                   @Field("message") complaint:String):Call<TermsResponse>

    @FormUrlEncoded
    @POST("sign-up-user")
    fun SignUp(@Field("name") name:String,
               @Field("phone") phone:String,
               @Field("email") email:String,
               @Field("address") address:String,
               @Field("lat") lat:String,
               @Field("lng") lng:String,
               @Field("password") password:String,
               @Field("device_id") device_id:String,
               @Field("device_type") device_type:String,
               @Header("lang") lang:String): Call<UserResponse>
    @Multipart
    @POST("sign-up-delegate")
    fun SignUpDel(@Query("name") name:String,
                  @Query("phone") phone:String,
                  @Query("email") email:String,
                  @Query("address") address:String,
                  @Query("lat") lat:String,
                  @Query("lng") lng:String,
                  @Query("password") password:String,
                  @Query("device_id") device_id:String,
                  @Query("device_type") device_type:String,
                  @Header("lang") lang:String,
                  @Part id_image:MultipartBody.Part,
                  @Part driving_license:MultipartBody.Part):Call<UserResponse>

    @FormUrlEncoded
    @POST("check-code")
    fun CheckCode(@Header("Authorization") Authorization:String,
                  @Field("code") code:String,
                  @Header("lang") lang: String):Call<UserResponse>

    @POST("resend-code")
    fun Resend(@Header("Authorization") Authorization:String,
               @Header("lang") lang: String):Call<UserResponse>

    @FormUrlEncoded
    @POST("sign-in")
    fun Login(@Field("phone") phone:String,
              @Field("password") password:String,
              @Field("device_id") device_id:String,
              @Field("device_type") device_type:String,
              @Header("lang") lang: String):Call<UserResponse>

    @FormUrlEncoded
    @POST("forget-password")
    fun ForGot(@Field("phone") phone:String,
               @Header("lang") lang:String):Call<UserResponse>

    @FormUrlEncoded
    @POST("update-password")
    fun NewPass(@Header("Authorization") Authorization:String,
                @Field("password") password:String,
                @Field("code") code:String,
                @Header("lang") lang: String):Call<UserResponse>

    @POST("main-user")
    fun Main(@Header("lang") lang: String):Call<HomeResponse>
    @POST("category-products")
    fun Products(@Header("lang") lang: String,
                 @Header("Authorization") Authorization:String?,
    @Query("category_id") category_id:Int,
    @Query("lat") lat:String,
    @Query("lng") lng: String,
    @Query("subcategory_id") subcategory_id:Int?,
    @Query("mac_address_id") mac_address_id:String,
    @Query("favorite") favorite:Int?,
    @Query("sort") sort:String?):Call<CategoryResponse>

    @POST("product-services")
    fun Services(@Header("lang") lang:String,
    @Query("product_id") product_id:Int,
    @Query("lat") lat:String,
    @Query("lng") lng:String,
    @Query("mac_address_id") mac_address_id:String):Call<ServicesResponse>

    @POST("edit-product-service")
    fun EditServices(@Header("Authorization") Authorization:String,
                     @Header("lang") lang:String,
                     @Query("product_id") product_id:Int,
                     @Query("order_id") order_id:Int,
                     @Query("services") services:String?):Call<ServicesResponse>
    @POST("add-to-cart")
    fun AddToCart(@Header("lang") lang:String,
    @Query("mac_address_id") mac_address_id:String,
    @Query("category_id") category_id:Int,
    @Query("services") services:String):Call<CartResponse>

    @POST("add-order")
    fun AddOrder(@Header("lang") lang: String,
                 @Header("Authorization") Authorization:String,
                 @Query("address") address:String,
                 @Query("lat") lat:String,
                 @Query("lng") lng: String,
                 @Query("order_id") order_id:Int,
                 @Query("category_id") category_id:Int):Call<BasketResponse>

    @POST("add-order")
    fun AddOrder(@Header("lang") lang: String,
                 @Header("Authorization") Authorization:String,
                 @Query("received_date") received_date:String,
                 @Query("delivery_date") delivery_date:String?,
                 @Query("address") address:String,
                 @Query("lat") lat:String,
                 @Query("lng") lng: String,
                 @Query("total") total:Int,
                 @Query("final_total") final_total:Int,
                 @Query("total_tax") total_tax:Int,
                 @Query("total_delivery") total_delivery:Int,
                 @Query("total_additional") total_additional:Int,
                 @Query("additionals") additionals:String?,
                 @Query("notes") notes:String,
                 @Query("order_id") order_id:Int,
                 @Query("category_id") category_id:Int):Call<BasketResponse>

    @POST("favorite")
    fun Favourite(@Header("lang") lang: String,
                  @Header("Authorization") Authorization:String,
                  @Query("product_id") product_id: Int):Call<FavResponse>

    @POST("payment-order")
    fun Payment(@Header("lang") lang: String,
                @Header("Authorization") Authorization:String,
                @Query("order_id") order_id: Int,
                @Query("payment") payment:String):Call<TermsResponse>

    @POST("delete-service-cart")
    fun DeleteService(@Header("lang") lang: String,
                      @Query("service_cart_id") service_cart_id:Int):Call<TermsResponse>

    @POST("delete-order")
    fun DeleteOrder(@Header("lang") lang: String,
                    @Header("Authorization") Authorization:String,
                    @Query("order_id") order_id: Int):Call<TermsResponse>
    @POST("user-addresses")
    fun MyAddress(@Header("lang") lang: String,
                  @Header("Authorization") Authorization:String):Call<MyAddressResponse>
    @POST("add-address")
    fun AddAddress(@Header("lang") lang: String,
                   @Header("Authorization") Authorization:String,
                   @Query("type") type:String,
                   @Query("address") address:String,
                   @Query("lat") lat:String,
                   @Query("lng") lng:String):Call<TermsResponse>

    @POST("delete-address")
    fun DeleteAddress(@Header("lang") lang: String,
                      @Header("Authorization") Authorization:String,
                      @Query("address_id") address_id:Int):Call<TermsResponse>

    @POST("user-orders")
    fun Orders(@Header("lang") lang: String,
               @Header("Authorization") Authorization:String,
               @Query("status") status:String):Call<OrdersResponse>
    @POST("delegate-orders")
    fun OrdersDelegates(@Header("lang") lang: String,
               @Header("Authorization") Authorization:String,
               @Query("status") status:String):Call<OrdersResponse>
    @POST("order-details")
    fun OrderDetails(@Header("lang") lang: String,
                     @Header("Authorization") Authorization:String,
                     @Query("order_id") order_id: Int,
                     @Query("action") status:String?):Call<OrderDetailsResponse>
    @POST("delegate-order-details")
    fun DelegateOrderDetails(@Header("lang") lang: String,
                     @Header("Authorization") Authorization:String,
                     @Query("order_id") order_id: Int,
                     @Query("action") status:String?):Call<DelegateOrderDetailsResponse>

    @POST("log-out")
    fun logOut(@Header("Authorization") Authorization:String,
               @Query("device_id") device_id:String,
               @Query("device_type") device_type:String,
               @Header("lang") lang:String):Call<TermsResponse>

    @FormUrlEncoded
    @POST("edit-profile")
    fun Edit(@Header("Authorization") Authorization:String,
             @Header("lang") lang:String,
             @Field("name") name:String?,
             @Field("phone") phone: String?,
             @Field("email") email:String?,
             @Field("lat") lat:String?,
             @Field("lng") lng:String?,
             @Field("address") address:String?):Call<UserResponse>

    @Multipart
    @POST("edit-profile")
    fun AddImage(@Header("Authorization") Authorization:String,
                 @Header("lang") lang:String,
                 @Part avatar:MultipartBody.Part):Call<UserResponse>
    @FormUrlEncoded
    @POST("reset-password")
    fun resetPassword(@Header("lang") lang:String,
                      @Header("Authorization") Authorization:String,
                      @Field("current_password") current_password:String,
                      @Field("password") password:String):Call<BaseResponse>
    @POST("product-details")
    fun getProduct(@Header("Authorization") Authorization:String?,
                   @Header("lang") lang:String,
                   @Query("product_id") product_id:Int,
                   @Query("lat") lat:String,
                   @Query("lng") lng:String):Call<ProductDetailsResponse>

    @POST("packages")
    fun Packages(@Header("lang") lang:String):Call<PackagesResponse>
    @POST("add-rate")
    fun AddRate(@Header("Authorization") Authorization:String?,
                @Header("lang") lang:String,
                @Query("order_id") order_id: Int,
                @Query("service_rate") service_rate:Int,
                @Query("delegate_rate") delegate_rate:Int):Call<TermsResponse>

    @POST("my-dates")
    fun MyDates(@Header("Authorization") Authorization:String?,
                @Header("lang") lang:String):Call<MyTimesModel>

    @POST("bank-transfers")
    fun Banks(@Header("Authorization") Authorization:String,@Header("lang") lang: String):Call<BanksResponse>

    @Multipart
    @POST("package-payment")
    fun PackagePayment(@Header("Authorization") Authorization:String,@Header("lang") lang: String,
                       @Query("package_id") package_id:Int,
                       @Query("payment_method") payment_method:String,
                       @Query("bank_name") bank_name:String,
                       @Query("amount") amount:String,
                       @Part image:MultipartBody.Part):Call<TermsResponse>
    @Multipart
    @POST("payment-order")
    fun orderPayment(@Header("Authorization") Authorization:String,@Header("lang") lang: String,
                       @Query("order_id") package_id:Int,
                       @Query("payment") payment_method:String,
                       @Query("bank_name") bank_name:String,
                       @Query("amount") amount:String,
                       @Part image:MultipartBody.Part):Call<TermsResponse>
    @POST("add-date")
    fun AddDate(@Header("Authorization") Authorization:String,@Header("lang") lang: String,
                @Query("date") date:String,
                @Query("time") time:String):Call<TermsResponse>
    @POST("notifications")
    fun Notification(@Header("lang") lang: String,
                     @Header("Authorization") Authorization:String):Call<NotificationResponse>
    @POST("delete-notification")
    fun delete(
            @Header("Authorization") Authorization:String,
            @Header("lang") lang:String,
            @Query("notification_id") notification_id:Int):Call<TermsResponse>

    @POST("register-delegate")
    fun CheckRegister(@Header("lang") lang:String):Call<FavResponse>

    @POST("request-delivery")
    fun RequestDelivery(@Header("Authorization") Authorization:String,
                        @Header("lang") lang:String,
                        @Query("category_id") category_id:Int,
    @Query("bill_number") bill_number:String,
    @Query("address") address:String,
    @Query("lat") lat:String,
    @Query("lng") lng:String,
    @Query("date") date:String,
    @Query("time") time:String,
    @Query("notes") notes:String):Call<TermsResponse>

    @POST("my-favorites")
    fun MyFavs(@Header("Authorization") Authorization:String,
               @Header("lang") lang:String,
               @Query("lat") lat:String,
               @Query("lng") lng:String):Call<MyFavResponse>
}