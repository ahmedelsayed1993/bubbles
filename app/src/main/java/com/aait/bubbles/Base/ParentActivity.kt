package com.aait.bubbles.Base

import android.app.ProgressDialog
import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar


//import androidx.lifecycle.ViewModelProviders
//import androidx.recyclerview.widget.RecyclerView
//import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
//import com.aait.coffee.Cart.AddCartViewModel
////import com.aait.coffee.Cart.AddCartViewModel

import com.aait.bubbles.Pereferences.LanguagePrefManager
import com.aait.bubbles.Pereferences.SharedPrefManager

import com.aait.bubbles.R
import com.aait.bubbles.UI.Views.Toaster
import com.aait.bubbles.Utils.CommonUtil
import com.aait.bubbles.Utils.DialogUtil
import com.pnikosis.materialishprogress.ProgressWheel



/**
 * Created by Ahmed El_sayed on 16/1/2019.
 */
abstract class ParentActivity : AppCompatActivity() {
    lateinit var mActivity: AppCompatActivity

    lateinit var user: SharedPrefManager

    lateinit var lang: LanguagePrefManager

   // open lateinit var addCartViewModel: AddCartViewModel
    internal var toolbar: Toolbar? = null

    lateinit var mContext: Context

    private var menuId: Int = 0
 //   open lateinit var addCartViewModel: AddCartViewModel

    lateinit var mToaster: Toaster

    var mSavedInstanceState: Bundle? = null

    private var mProgressDialog: ProgressDialog? = null



    /**
     * this method is responsible for configure toolbar
     * it is called when I enable toolbar in my activity
     */
    //    private void configureToolbar() {
    //      //  toolbar = (Toolbar) findViewById(R.id.toolbar);
    //        setSupportActionBar(toolbar);
    //        getSupportActionBar().setDisplayShowTitleEnabled(false);
    //      //  toolbar.setTitleTextColor(ContextCompat.getColor(mContext, R.color.colorOrange));
    //        // check if enable back
    ////        if (isEnableBack()) {
    ////            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    //////             toolbar.setNavigationIcon(R.mipmap.back);
    ////        }
    //    }


    /**
     * @return the layout resource id
     */
    protected abstract val layoutResource: Int


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mContext = this
        mActivity = this
//        addCartViewModel = ViewModelProviders.of(this).get(AddCartViewModel::class.java)
//        addCartViewModel = AddCartViewModel(application)

        user = SharedPrefManager(mContext)
        lang = LanguagePrefManager(mContext)

        CommonUtil.setConfig(lang.appLanguage, this)
        mToaster = Toaster(mContext)

        //        if (isFullScreen()) {
        //            requestWindowFeature(Window.FEATURE_NO_TITLE);
        //            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
        //                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        //        }
        //        if (hideInputType()) {
        //            hideInputTyping();
        //        }
        // set layout resources
        setContentView(layoutResource)
        this.mSavedInstanceState = savedInstanceState



        initializeComponents()

    }


    protected abstract fun initializeComponents()

    /**
     * it is a boolean method which tell my if toolbar
     * is enabled or not
     */
    //    protected abstract boolean isEnableToolbar();

    /**
     * it is a boolean method which tell if full screen mode
     * is enabled or not
     */
    //    protected abstract boolean isFullScreen();

    /**
     * it is a boolean method which tell if back button
     * is enabled or not
     */
    //    protected abstract boolean isEnableBack();

    /**
     * it is a boolean method which tell if input is
     * is appeared  or not
     */
    //    protected abstract boolean hideInputType();

    /**
     * it the current activity is a recycle
     */


    /**
     * this method allowed me to create option menu
     */
    fun createOptionsMenu(menuId: Int) {
        Log.e("test", "test")
        this.menuId = menuId
        invalidateOptionsMenu()
    }

    /**
     * this method allowed me to remove option menu
     */
    fun removeOptionsMenu() {
        menuId = 0
        invalidateOptionsMenu()
    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        super.onCreateOptionsMenu(menu)
        if (menuId != 0) {
            menuInflater.inflate(menuId, menu)
        }
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()
            return true
        } else {
            return super.onOptionsItemSelected(item)
        }
    }


    override fun onStop() {
        super.onStop()
        overridePendingTransition(R.anim.enter_from_right, R.anim.exit_out_left)
    }

    override fun onResume() {
        super.onResume()
        overridePendingTransition(R.anim.enter_from_right, R.anim.exit_out_left)
    }

    //    public void hideInputTyping() {
    //        if (getCurrentFocus() != null) {
    //            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
    //            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
    //        }
    //    }

    protected fun showProgressDialog(message: String) {
        mProgressDialog = DialogUtil.showProgressDialog(this, message, false)
    }

    protected fun hideProgressDialog() {
        if (mProgressDialog != null) {
            mProgressDialog!!.dismiss()
        }
    }

}





