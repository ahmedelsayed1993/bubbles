package com.aait.bubbles.Utils

import android.app.ActivityManager
import android.app.ActivityManager.RunningTaskInfo
import android.app.DatePickerDialog
import android.content.ContentUris
import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.database.Cursor
import android.graphics.Paint
import android.net.Uri
import android.os.Build.VERSION
import android.os.Build.VERSION_CODES
import android.os.Environment
import android.provider.DocumentsContract
import android.provider.MediaStore
import android.text.format.DateUtils
import android.util.Log
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.DatePicker
import android.widget.EditText
import android.widget.TextView

import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity


import com.aait.bubbles.R
import com.aait.bubbles.UI.Views.Toaster
import com.google.android.material.appbar.AppBarLayout
import com.google.gson.Gson

import java.net.BindException
import java.net.ConnectException
import java.net.NoRouteToHostException
import java.net.PortUnreachableException
import java.net.SocketTimeoutException
import java.net.UnknownHostException
import java.net.UnknownServiceException
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Date
import java.util.Locale
import java.util.regex.Matcher
import java.util.regex.Pattern

import android.text.format.DateUtils.MINUTE_IN_MILLIS

//import com.bumptech.glide.DrawableRequestBuilder;


class CommonUtil {

    fun showParElevation(showHide: Boolean, app_bar: AppBarLayout, elevation: Float) {
        if (VERSION.SDK_INT >= VERSION_CODES.LOLLIPOP) {
            if (showHide) {
                app_bar.elevation = elevation

            } else {
                app_bar.elevation = 0.0.toFloat()
            }
        }
    }

    companion object {

        var isALog = true

        fun onPrintLog(o: Any) {
            if (isALog) {
                Log.e("Response >>>>", Gson().toJson(o))
            }
        }

        fun PrintLogE(print: String) {
            //        if (BuildConfig.DEBUG) {
            //            Log.e(AppController.TAG, print);
            //        }
            Log.e("TAG", print)
        }

        fun makeURL(
            sourceLat: Double,
            sourceLog: Double,
            destLat: Double,
            destLog: Double
        ): String {
            val urlString = StringBuilder()
            urlString.append("http://maps.googleapis.com/maps/api/directions/json")
            urlString.append("?origin=")// from
            urlString.append(java.lang.Double.toString(sourceLat))
            urlString.append(",")
            urlString.append(java.lang.Double.toString(sourceLog))
            urlString.append("&destination=")// to
            urlString.append(java.lang.Double.toString(destLat))
            urlString.append(",")
            urlString.append(java.lang.Double.toString(destLog))
            urlString.append("&sensor=false&mode=driving&alternatives=true")
            return urlString.toString()
        }

        val language: String
            get() = Locale.getDefault().displayLanguage

        fun requestFocus(view: View, window: Window) {
            if (view.requestFocus()) {
                window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE)
            }
        }

        fun ShareApp(context: Context) {
            val sendIntent = Intent()
            val appPackageName = context.packageName
            sendIntent.action = Intent.ACTION_SEND
            sendIntent.putExtra(
                Intent.EXTRA_TEXT,
                "Hey check the app at: https://play.google.com/store/apps/details?id=$appPackageName"
            )
            sendIntent.type = "text/plain"
            sendIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK

            context.startActivity(sendIntent)
        }

        fun ShareProductName(context: Context, name: String,link:String) {
            val sendIntent = Intent()
            sendIntent.action = Intent.ACTION_SEND
            sendIntent.putExtra(
                Intent.EXTRA_TEXT,
                name
            )
            sendIntent.type = "text/plain"
            sendIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            sendIntent.setPackage(link)
            context.startActivity(sendIntent)

        }

        fun RateApp(context: AppCompatActivity) {
            val appPackageName =
                context.packageName // getPackageName() from Context or Activity object
            try {
                context.startActivity(
                    Intent(
                        Intent.ACTION_VIEW,
                        Uri.parse("market://details?id=$appPackageName")
                    )
                )
            } catch (anfe: android.content.ActivityNotFoundException) {
                context.startActivity(
                    Intent(
                        Intent.ACTION_VIEW,
                        Uri.parse("https://play.google.com/store/apps/details?id=$appPackageName")
                    )
                )
            }

        }

        fun handleException(context: Context, t: Throwable): Int {
            if (t is SocketTimeoutException) {
                makeToast(context, R.string.time_out_error)
                return R.string.time_out_error
            } else if (t is UnknownHostException) {
                makeToast(context, R.string.connection_error)
                return R.string.connection_error
            } else if (t is ConnectException) {
                makeToast(context, R.string.connection_error)
                return R.string.connection_error
            } else if (t is NoRouteToHostException) {
                makeToast(context, R.string.connection_error)
                return R.string.connection_error
            } else if (t is PortUnreachableException) {
                makeToast(context, R.string.connection_error)
                return R.string.connection_error
            } else if (t is UnknownServiceException) {
                makeToast(context, R.string.connection_error)
                return R.string.connection_error
            } else if (t is BindException) {
                makeToast(context, R.string.connection_error)
                return R.string.connection_error
            } else {
                makeToast(context, R.string.connection_error)
                return R.string.connection_error
            }
        }

        fun makeToast(context: Context, msgId: Int) {
            val toaster = Toaster(context)
            toaster.makeToast(context.getString(msgId))

        }

        fun makeToast(context: Context, msg: String) {
            val toaster = Toaster(context)
            toaster.makeToast(msg)

        }


        fun setConfig(language: String, context: Context) {
            val locale = Locale(language)
            Locale.setDefault(locale)
            val config = Configuration()
            config.locale = locale
            context.resources.updateConfiguration(
                config,
                context.resources.displayMetrics
            )

        }

        fun chooseDate(context: Context, textView: TextView) {
            val c = Calendar.getInstance()
            val year = c.get(Calendar.YEAR)
            val month = c.get(Calendar.MONTH)
            val day = c.get(Calendar.DAY_OF_MONTH)
            val dlg: DatePickerDialog
            dlg = DatePickerDialog(
                context,
                DatePickerDialog.OnDateSetListener { view, year, month, dayOfMonth ->
                    val cal = Calendar.getInstance()
                    cal.timeInMillis = 0
                    cal.set(year, month, dayOfMonth, 0, 0, 0)
                    val chosenDate = cal.time
                    val date = year.toString() + "-" + (month + 1) + "-" + dayOfMonth
                    val sdf = SimpleDateFormat("dd/MM/yyyy")

                    textView.text = sdf.format(chosenDate)
                },
                year,
                month,
                day
            )
            //        dlg.setTitle(getString(R.string.dtect_date));
            dlg.show()
        }


        fun getFormattedTime(date: String): String {
            var parse: Date? = null
            val sdf = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
            try {
                parse = sdf.parse(date)
            } catch (e: ParseException) {
                e.printStackTrace()
            }

            val calendar = Calendar.getInstance()
            calendar.time = parse!!
            val updated = calendar.timeInMillis
            return DateUtils.getRelativeTimeSpanString(
                updated,
                System.currentTimeMillis(),
                MINUTE_IN_MILLIS
            ).toString()
        }


        // Glide url
        //    public static DrawableRequestBuilder<String> loadImage(Context context, String posterPath) {
        //        return Glide
        //                .with(context)
        //                .load(posterPath)
        //                .diskCacheStrategy(DiskCacheStrategy.ALL);
        //    }

        fun openWhatsappContact(context: AppCompatActivity, number: String) {
            val uri = Uri.parse("smsto:$number")
            val mWhatsAppIntent = Intent(Intent.ACTION_SENDTO, uri)
            mWhatsAppIntent.setPackage("com.whatsapp")
            context.startActivity(Intent.createChooser(mWhatsAppIntent, ""))
        }


//        @RequiresApi(api = VERSION_CODES.Q)
//        fun getStakenumbers(context: Context): Int {
//            val m = context
//                .getSystemService(context.ACTIVITY_SERVICE) as ActivityManager
//            val runningTaskInfoList = m.getRunningTasks(10)
//            val itr = runningTaskInfoList.iterator()
//            var numOfActivities = 0
//            while (itr.hasNext()) {
//                val runningTaskInfo = itr.next() as RunningTaskInfo
//                val id = runningTaskInfo.id
//                val desc = runningTaskInfo.description
//                numOfActivities = runningTaskInfo.numActivities
//                val topActivity = runningTaskInfo.topActivity!!
//                    .shortClassName
//                CommonUtil.PrintLogE("Activities number : $numOfActivities Top Activies : $topActivity")
//                return numOfActivities
//            }
//            return numOfActivities
//        }


        fun setStrokInText(textView: TextView) {
            textView.paintFlags = textView.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
        }


//        @RequiresApi(api = VERSION_CODES.KITKAT)
//        fun getPathFromUri(context: Context, uri: Uri): String? {
//
//            val isKitKat = VERSION.SDK_INT >= VERSION_CODES.KITKAT
//
//            // DocumentProvider
//            if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
//                // ExternalStorageProvider
//                if (isExternalStorageDocument(uri)) {
//                    val docId = DocumentsContract.getDocumentId(uri)
//                    val split =
//                        docId.split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
//                    val type = split[0]
//
//                    if ("primary".equals(type, ignoreCase = true)) {
//                        return Environment.getExternalStorageDirectory().toString() + "/" + split[1]
//                    }
//                } else if (isDownloadsDocument(uri)) {
//
//                    val id = DocumentsContract.getDocumentId(uri)
//                    val contentUri = ContentUris.withAppendedId(
//                        Uri.parse("content://downloads/public_downloads"),
//                        java.lang.Long.valueOf(id)
//                    )
//
//                    return getDataColumn(context, contentUri, null, null)
//                } else if (isMediaDocument(uri)) {
//                    val docId = DocumentsContract.getDocumentId(uri)
//                    val split =
//                        docId.split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
//                    val type = split[0]
//
//                    var contentUri: Uri? = null
//                    if ("image" == type) {
//                        contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI
//                    } else if ("video" == type) {
//                        contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI
//                    } else if ("audio" == type) {
//                        contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI
//                    }
//
//                    val selection = "_id=?"
//                    val selectionArgs = arrayOf(split[1])
//
//                    return getDataColumn(context, contentUri, selection, selectionArgs)
//                }// MediaProvider
//            } else if ("content".equals(uri.scheme!!, ignoreCase = true)) {
//                return if (isGooglePhotosUri(uri)) uri.lastPathSegment else getDataColumn(
//                    context,
//                    uri,
//                    null,
//                    null
//                )
//            } else if ("file".equals(uri.scheme!!, ignoreCase = true)) {
//                return uri.path
//            }
//            return null
//        }

        fun getDataColumn(
            context: Context,
            uri: Uri?,
            selection: String?,
            selectionArgs: Array<String>?
        ): String? {
            var cursor: Cursor? = null
            val column = "_data"
            val projection = arrayOf(column)
            try {
                cursor =
                    context.contentResolver.query(uri!!, projection, selection, selectionArgs, null)
                if (cursor != null && cursor.moveToFirst()) {
                    val index = cursor.getColumnIndexOrThrow(column)
                    return cursor.getString(index)
                }
            } finally {
                cursor?.close()
            }
            return null
        }

        fun isExternalStorageDocument(uri: Uri): Boolean {
            return "com.android.externalstorage.documents" == uri.authority
        }

        fun isDownloadsDocument(uri: Uri): Boolean {
            return "com.android.providers.downloads.documents" == uri.authority
        }

        fun isMediaDocument(uri: Uri): Boolean {
            return "com.android.providers.media.documents" == uri.authority
        }

        fun isGooglePhotosUri(uri: Uri): Boolean {
            return "com.google.android.apps.photos.content" == uri.authority
        }

        fun checkEditError(

            editText: EditText,
            message: String
        ): Boolean {
            if (editText.text.toString().isEmpty()) {
                editText.error = message
                editText.requestFocus()
                return true
            }
            else {
                editText.error = null
                return false
            }
        }

        fun checkTextError(textView: TextView, message: String): Boolean {
            if (textView.text.toString().isEmpty()) {
                textView.error = message
                return true
            }else {
                textView.error = null
                return false
            }
        }

        fun checkLength(editText: EditText, message: String, i: Int): Boolean {
            if (editText.text.toString().length < i) {
                editText.error = message
                return true
            }else{
                editText.error = null
                return false
            }

        }

        fun checkLength1(editText: EditText, message: String, i: Int): Boolean {
            if (editText.text.toString().length != i) {
                editText.error = message
                return true
            }else {
                editText.error = null
                return false
            }
        }

        fun checkEmail(editText: EditText, message: String): Boolean {
            if (editText.text.toString().contains(".") && editText.text.toString().contains("@")) {
                editText.error = message
                return true
            }
            return false
        }

        fun isEmailValid(email: EditText, message: String): Boolean {
            val emailString = email.text.toString()
            val expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$"
            val pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE)
            val matcher = pattern.matcher(emailString)
            if (matcher.matches() == true) {
                email.error = null
                return true
            } else {
                email.error = message
                return false
            }
        }

//        @RequiresApi(api = VERSION_CODES.KITKAT)
//        fun getPath(context: Context, uri: Uri): String? {
//
//            val isKitKat = VERSION.SDK_INT >= VERSION_CODES.KITKAT
//
//            // DocumentProvider
//            if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
//                // ExternalStorageProvider
//                if (isExternalStorageDocument(uri)) {
//                    val docId = DocumentsContract.getDocumentId(uri)
//                    val split =
//                        docId.split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
//                    val type = split[0]
//
//                    if ("primary".equals(type, ignoreCase = true)) {
//                        return Environment.getExternalStorageState() + "/" + split[1]
//                    }
//
//
//                } else if (isDownloadsDocument(uri)) {
//
//                    val id = DocumentsContract.getDocumentId(uri)
//                    val contentUri = ContentUris.withAppendedId(
//                        Uri.parse("content://downloads/public_downloads"),
//                        java.lang.Long.valueOf(id)
//                    )
//
//                    return getDataColumn(context, contentUri, null, null)
//                } else if (isMediaDocument(uri)) {
//                    val docId = DocumentsContract.getDocumentId(uri)
//                    val split =
//                        docId.split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
//                    val type = split[0]
//
//                    var contentUri: Uri? = null
//                    if ("image" == type) {
//                        contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI
//                    } else if ("video" == type) {
//                        contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI
//                    } else if ("audio" == type) {
//                        contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI
//
//                    }
//
//                    val selection = "_id=?"
//                    val selectionArgs = arrayOf(split[1])
//
//                    return getDataColumn(context, contentUri, selection, selectionArgs)
//                }// MediaProvider
//                // DownloadsProvider
//            } else if ("content".equals(uri.scheme!!, ignoreCase = true)) {
//
//                // Return the remote address
//                return if (isGooglePhotosUri(uri)) uri.lastPathSegment else getDataColumn(
//                    context,
//                    uri,
//                    null,
//                    null
//                )
//
//            } else if ("file".equals(uri.scheme!!, ignoreCase = true)) {
//                return uri.path
//            }// File
//            // MediaStore (and general)
//
//            return null
//        }
    }
}
