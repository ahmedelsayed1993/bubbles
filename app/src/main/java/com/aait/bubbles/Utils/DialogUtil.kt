package com.aait.bubbles.Utils

import android.app.ProgressDialog
import android.content.Context
import android.content.DialogInterface

import androidx.appcompat.app.AlertDialog

import com.aait.bubbles.R


/**
 * Created by Ahmed El_sayed on 16/1/2019.
 */

object DialogUtil {

    fun showProgressDialog(context: Context, message: String, cancelable: Boolean): ProgressDialog {
        val dialog = ProgressDialog(context)
        dialog.setMessage(message)
        dialog.setCancelable(cancelable)
        dialog.show()
        return dialog
    }


    fun showAlertDialog(
        context: Context, message: String,
        negativeClickListener: DialogInterface.OnClickListener?
    ): AlertDialog {
        // create the dialog builder & set message
        val dialogBuilder = AlertDialog.Builder(context)
        dialogBuilder.setTitle(message)
        // check negative click listener
        if (negativeClickListener != null) {
            // not null
            // add negative click listener
            dialogBuilder.setNegativeButton(context.getString(R.string.yes), negativeClickListener)
        } else {
            // null
            // add new click listener to dismiss the dialog
            dialogBuilder.setNegativeButton(context.getString(R.string.no)) { dialog, which -> dialog.dismiss() }
        }
        // create and show the dialog

        val dialog = dialogBuilder.create()
        dialog.show()
        return dialog
    }

    fun showconfirm(
        context: Context,
        message: String,
        cancble: Boolean,
        negativeClickListener: DialogInterface.OnClickListener?,
        positiveClickListener: DialogInterface.OnClickListener?
    ): AlertDialog {
        // create the dialog builder & set message
        val dialogBuilder = AlertDialog.Builder(context)


        dialogBuilder.setMessage(message)


        // check negative click listener
        if (negativeClickListener != null) {
            // not null
            // add negative click listener
            dialogBuilder.setNegativeButton(
                context.getString(R.string.cancel),
                negativeClickListener
            )
        }
        if (positiveClickListener != null) {
            // not null
            // add negative click listener
            dialogBuilder.setPositiveButton(
                context.getString(R.string.confirm),
                positiveClickListener
            )

        }
        // create and show the dialog
        dialogBuilder.setCancelable(cancble)
        val dialog = dialogBuilder.create()
        dialog.show()

        return dialog
    }
}
