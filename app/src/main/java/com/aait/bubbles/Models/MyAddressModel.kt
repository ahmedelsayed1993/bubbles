package com.aait.bubbles.Models

import java.io.Serializable

class MyAddressModel:Serializable {
    var id:Int?=null
    var type:String?=null
    var address:String?=null
    var lat:String?=null
    var lng:String?=null
}