package com.aait.bubbles.Models

import java.io.Serializable

class CallUsResponse:BaseResponse(),Serializable {
    var data:CallUsModel?=null
    var socials:ArrayList<SocialModel>?=null
}