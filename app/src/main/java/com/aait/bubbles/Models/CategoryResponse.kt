package com.aait.bubbles.Models

import java.io.Serializable

class CategoryResponse:BaseResponse(),Serializable {
    var subcategories:ArrayList<SubCatModel>?=null
    var data:ArrayList<ProductModel>?=null
    var pieces:Int?=null
    var total:Int?=null
    var order_id:Int?=null
}