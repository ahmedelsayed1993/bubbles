package com.aait.bubbles.Models

import java.io.Serializable

class PackageModel:Serializable {
    var id:Int?=null
    var price:String?=null
    var days:String?=null
    var number_orders:String?=null
    var title:String?=null
    var desc:String?=null
}