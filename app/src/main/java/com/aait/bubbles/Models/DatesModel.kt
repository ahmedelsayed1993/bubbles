package com.aait.bubbles.Models

import java.io.Serializable

class DatesModel:Serializable {
    var id:Int?=null
    var date:String?=null
    var time:String?=null
}