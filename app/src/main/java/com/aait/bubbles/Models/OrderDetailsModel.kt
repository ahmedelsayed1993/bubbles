package com.aait.bubbles.Models

import java.io.Serializable

class OrderDetailsModel:Serializable {
    var status:String?=null
    var category:String?=null
    var is_rate:Int?=null
    var products:ArrayList<CartModel>?=null
    var id:Int?=null
    var total:String?=null
    var date:String?=null
    var time:String?=null
    var delegate_avatar:String?=null
    var delegate_name:String?=null
    var delegate_phone:String?=null
}