package com.aait.bubbles.Models

import java.io.Serializable
import java.util.*

class AdditionsModel:Serializable {
    var id:Int?=null
    var name:String?=null
    var text:String?=null
    var extras:ArrayList<ServicesModel>?=null
}