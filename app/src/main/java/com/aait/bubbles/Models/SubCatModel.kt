package com.aait.bubbles.Models

import java.io.Serializable

class SubCatModel:Serializable {
    var id:Int?=null
    var name:String?=null
    var selected:Boolean?=false
}