package com.aait.bubbles.Models

import java.io.Serializable

class DelegateOrderDetailsModel:Serializable {
    var id:Int?=null
    var username:String?=null
    var category:String?=null
    var phone:String?=null
    var received_date:String?=null
    var delivery_date:String?=null
    var products:ArrayList<CartModel>?=null
    var address:String?=null
    var lat:String?=null
    var lng:String?=null
    var payment:String?=null
    var notes:String?=null
    var total:String?=null
    var total_tax:String?=null
    var total_delivery:String?=null
    var final_total:String?=null
    var status:String?=null
    var tax:String?=null
}