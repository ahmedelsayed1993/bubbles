package com.aait.bubbles.Models

import java.io.Serializable

class ProductModel:Serializable {
    var id:Int?=null
    var name:String?=null
    var favorite:Int?=null
    var category:String?=null
    var category_id:Int?=null
    var image:String?=null
    var desc:String?=null
    var price:String?=null
    var in_cart:Int?=null
    var services:ArrayList<ServicesModel>?=null
}