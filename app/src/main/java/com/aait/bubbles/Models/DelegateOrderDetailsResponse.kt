package com.aait.bubbles.Models

import java.io.Serializable

class DelegateOrderDetailsResponse:BaseResponse(),Serializable {
    var data:DelegateOrderDetailsModel?=null
}