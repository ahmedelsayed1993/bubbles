package com.aait.bubbles.Models

import java.io.Serializable

open class BaseResponse:Serializable {
    var key:String?=null
    var value:String?=null
    var msg:String?=null
}