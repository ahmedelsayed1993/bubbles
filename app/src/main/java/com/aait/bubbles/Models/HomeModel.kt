package com.aait.bubbles.Models

import java.io.Serializable

class HomeModel:Serializable {
    var id:Int?=null
    var name:String?=null
    var type:String?=null
    var image:String?=null
}