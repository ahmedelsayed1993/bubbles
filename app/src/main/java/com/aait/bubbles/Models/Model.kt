package com.aait.bubbles.Models

import java.io.Serializable

class Model:Serializable {
    var product_id:Int?=null
    var service_id:Int?=null
    var price:String?=null
    var count:Int?=null

    constructor(product_id: Int?, service_id: Int?, price: String?, count: Int?) {
        this.product_id = product_id
        this.service_id = service_id
        this.price = price
        this.count = count
    }

    constructor(product_id: Int?, price: String?, count: Int?) {
        this.product_id = product_id
        this.price = price
        this.count = count
    }

}