package com.aait.bubbles.Models

import java.io.Serializable

class ProductDetailsModel:Serializable {
    var images:ArrayList<ImageModel>?=null
    var name:String?=null
    var desc:String?=null
    var price:String?=null
    var favorite:Int?=null
    var count:Int?=null
}