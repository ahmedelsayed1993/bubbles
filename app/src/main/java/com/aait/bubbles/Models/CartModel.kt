package com.aait.bubbles.Models

import java.io.Serializable

class CartModel:Serializable {
    var name:String?=null
    var image:String?=null
    var product_id:Int?=null
    var services:ArrayList<ServicesModel>?=null
}