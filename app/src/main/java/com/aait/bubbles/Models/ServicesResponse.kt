package com.aait.bubbles.Models

import java.io.Serializable

class ServicesResponse:BaseResponse(),Serializable {
    var data:ArrayList<ServicesModel>?=null
}