package com.aait.bubbles.Models

import java.io.Serializable

class TimeModel:Serializable {
     var time:String?=null
    var actual:String?=null
    var selected:Boolean?=null

    constructor(time: String?, actual: String?) {
        this.time = time
        this.actual = actual
    }
}