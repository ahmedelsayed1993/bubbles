package com.aait.bubbles.Models

import java.io.Serializable

class OrdersResponse:BaseResponse(),Serializable {
    var data:ArrayList<OrderModel>?=null
}