package com.aait.bubbles.Models

import java.io.Serializable

class HomeResponse:BaseResponse(),Serializable {
    var banners:ArrayList<String>?=null
    var categories:ArrayList<HomeModel>?=null
}