package com.aait.bubbles.Models

import java.io.Serializable

class OrderModel:Serializable {
    var id:Int?=null
    var total:String?=null
    var date:String?=null
    var time:String?=null
    var status:String?=null
}