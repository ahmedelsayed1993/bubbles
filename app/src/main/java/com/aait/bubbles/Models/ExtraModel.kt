package com.aait.bubbles.Models

import java.io.Serializable

class ExtraModel:Serializable {
    var extra_id:Int?=null
    var price:String?=null
    var id:Int?=null
    var name:String?=null

    constructor(extra_id: Int?, price: String?) {
        this.extra_id = extra_id
        this.price = price
    }

    constructor(extra_id: Int?, price: String?, id: Int?,name:String?) {
        this.extra_id = extra_id
        this.price = price
        this.id = id
        this.name = name
    }

}