package com.aait.bubbles.Models

import java.io.Serializable

class NotificationModel:Serializable {
    var id:Int?=null
    var content:String?=null
    var created:String?=null
    var type:String?=null
    var order_id:Int?=null
    var reservation_id:Int?=null
}