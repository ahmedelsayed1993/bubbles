package com.aait.bubbles.Models

import java.io.Serializable

class ServicesModel:Serializable {
    var id:Int?=null
    var name:String?=null
    var price:String?=null
    var count:Int?=null
    var service:String?=null
    var service_id:Int?=null
}