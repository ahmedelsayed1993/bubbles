package com.aait.bubbles.UI.Controllers

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.aait.bubbles.Base.ParentRecyclerAdapter
import com.aait.bubbles.Base.ParentRecyclerViewHolder

import com.aait.bubbles.R
import com.bumptech.glide.Glide
import de.hdodenhof.circleimageview.CircleImageView

import android.view.animation.AnimationUtils.loadAnimation
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import com.aait.bubbles.Models.HomeModel
import java.security.AccessController.getContext


class CatsAdapter (context: Context, data: MutableList<HomeModel>, layoutId: Int) :
    ParentRecyclerAdapter<HomeModel>(context, data, layoutId) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParentRecyclerViewHolder {
        val itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false)
        return ViewHolder(itemView)
    }
    var TextView.textColor: Int
        get() = currentTextColor
        set(v) = setTextColor(v)



    override fun onBindViewHolder(holder: ParentRecyclerViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder
        val questionModel = data.get(position)
        viewHolder.name!!.setText(questionModel.name)
        Glide.with(mcontext).load(questionModel.image).into(viewHolder.image)
       // viewHolder.itemView.animation = mcontext.resources.
       // val animation = mcontext.resources.getAnimation(R.anim.item_animation_from_right)
//        val animation = AnimationUtils.loadAnimation(mcontext, R.anim.item_animation_from_bottom_slow)
//        animation.setDuration(250)
//        viewHolder.itemView.startAnimation(animation)

        viewHolder.itemView.setOnClickListener(View.OnClickListener { view -> onItemClickListener.onItemClick(view,position)

        })




    }
    inner class ViewHolder internal constructor(itemView: View) :
        ParentRecyclerViewHolder(itemView) {





        internal var name=itemView.findViewById<TextView>(R.id.name)
        internal var image = itemView.findViewById<ImageView>(R.id.image)




    }
}