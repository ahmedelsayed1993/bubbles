package com.aait.bubbles.UI.Activities.Main.Client

import android.content.Intent
import android.os.Build
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aait.bubbles.Base.ParentActivity
import com.aait.bubbles.Listeners.OnItemClickListener
import com.aait.bubbles.Models.DatesModel
import com.aait.bubbles.Models.MyTimesModel
import com.aait.bubbles.Models.TermsResponse
import com.aait.bubbles.Models.TimeModel
import com.aait.bubbles.Network.Client
import com.aait.bubbles.Network.Service
import com.aait.bubbles.R
import com.aait.bubbles.UI.Controllers.MyTimesAdapter
import com.aait.bubbles.UI.Controllers.TimesAdapter
import com.aait.bubbles.Utils.CommonUtil
import com.google.android.gms.common.api.Api
import com.google.android.gms.common.api.CommonStatusCodes
import com.vivekkaushik.datepicker.DatePickerTimeline
import com.vivekkaushik.datepicker.OnDateSelectedListener
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.*
import kotlin.collections.ArrayList

class MyTimesActivity:ParentActivity() ,OnItemClickListener{
    override val layoutResource: Int
        get() = R.layout.activity_my_times
    lateinit var back:ImageView
    lateinit var title:TextView
    lateinit var msg:TextView
    lateinit var set:Button
    lateinit var my_times:RecyclerView
    lateinit var set_pakage:Button
    lateinit var add:TextView
    lateinit var time_lay:LinearLayout
    lateinit var datePickerTimeline:DatePickerTimeline
    lateinit var times_list:RecyclerView
    lateinit var confirm:Button
    lateinit var timelinearLayoutManager: LinearLayoutManager
    var times = ArrayList<TimeModel>()
    lateinit var timesAdapter: TimesAdapter
    lateinit var timeModel: TimeModel
    lateinit var linearLayoutManager: LinearLayoutManager
    lateinit var myTimesAdapter:MyTimesAdapter
    var dates = ArrayList<DatesModel>()
    var date = ""
    @RequiresApi(Build.VERSION_CODES.O)
    override fun initializeComponents() {
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        msg = findViewById(R.id.msg)
        set = findViewById(R.id.set)
        my_times = findViewById(R.id.my_times)
        set_pakage = findViewById(R.id.set_package)
        add = findViewById(R.id.add)
        time_lay = findViewById(R.id.time_lay)
        datePickerTimeline = findViewById(R.id.datePickerTimeline)
        times_list = findViewById(R.id.times_list)
        confirm = findViewById(R.id.confirm)
        back.setOnClickListener { onBackPressed()
        finish()}
        date =(LocalDateTime.now()).format(DateTimeFormatter.ofPattern("yyyy-MM-dd"))
        title.text = getString(R.string.my_times)
        timelinearLayoutManager = LinearLayoutManager(mContext,LinearLayoutManager.HORIZONTAL,false)
        timesAdapter = TimesAdapter(mContext,ArrayList<TimeModel>(),R.layout.recycle_times)
        timesAdapter.setOnItemClickListener(this)
        times_list.layoutManager = timelinearLayoutManager
        times_list.adapter = timesAdapter
        times.add(TimeModel("00:00"+getString(R.string.am),"00:00"))
        times.add(TimeModel("01:00"+getString(R.string.am),"01:00"))
        times.add(TimeModel("02:00"+getString(R.string.am),"02:00"))
        times.add(TimeModel("03:00"+getString(R.string.am),"03:00"))
        times.add(TimeModel("04:00"+getString(R.string.am),"04:00"))
        times.add(TimeModel("05:00"+getString(R.string.am),"05:00"))
        times.add(TimeModel("06:00"+getString(R.string.am),"06:00"))
        times.add(TimeModel("07:00"+getString(R.string.am),"07:00"))
        times.add(TimeModel("08:00"+getString(R.string.am),"08:00"))
        times.add(TimeModel("09:00"+getString(R.string.am),"09:00"))
        times.add(TimeModel("10:00"+getString(R.string.am),"10:00"))
        times.add(TimeModel("11:00"+getString(R.string.am),"11:00"))
        times.add(TimeModel("12:00"+getString(R.string.am),"12:00"))
        times.add(TimeModel("01:00"+getString(R.string.pm),"13:00"))
        times.add(TimeModel("02:00"+getString(R.string.pm),"14:00"))
        times.add(TimeModel("03:00"+getString(R.string.pm),"15:00"))
        times.add(TimeModel("04:00"+getString(R.string.pm),"16:00"))
        times.add(TimeModel("05:00"+getString(R.string.pm),"17:00"))
        times.add(TimeModel("06:00"+getString(R.string.pm),"18:00"))
        times.add(TimeModel("07:00"+getString(R.string.pm),"19:00"))
        times.add(TimeModel("08:00"+getString(R.string.pm),"20:00"))
        times.add(TimeModel("09:00"+getString(R.string.pm),"21:00"))
        times.add(TimeModel("10:00"+getString(R.string.pm),"22:00"))
        times.add(TimeModel("11:00"+getString(R.string.pm),"23:00"))
        times.add(TimeModel("12:00"+getString(R.string.pm),"24:00"))
        timeModel = times.get(0)
        set.setOnClickListener { startActivity(Intent(this,PackagesActivity::class.java)) }
        set_pakage.setOnClickListener { startActivity(Intent(this,PackagesActivity::class.java)) }
        add.setOnClickListener { time_lay.visibility = View.VISIBLE
        timesAdapter.updateAll(times)}
        linearLayoutManager = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        myTimesAdapter = MyTimesAdapter(mContext,dates,R.layout.recycle_my_times)
        my_times.layoutManager = linearLayoutManager
        my_times.adapter = myTimesAdapter

        datePickerTimeline.setInitialDate(Calendar.getInstance().get(Calendar.YEAR), Calendar.getInstance().get(Calendar.MONTH), Calendar.getInstance().get(Calendar.DAY_OF_MONTH))
        getData()
        datePickerTimeline.setOnDateSelectedListener(object : OnDateSelectedListener {
            override fun onDateSelected(year: Int, month: Int, day: Int, dayOfWeek: Int) {
                Log.e("date",year.toString()+"-"+(month+1).toString()+"-"+day)
                date = year.toString()+"-"+(month+1).toString()+"-"+day
                // Do Something
            }

            override fun onDisabledDateSelected(
                    year: Int,
                    month: Int,
                    day: Int,
                    dayOfWeek: Int,
                    isDisabled: Boolean
            ) {
                // Do Something
            }
        })

// Disable date

// Disable date
        val dates: Array<Date> = arrayOf<Date>(Calendar.getInstance().getTime())
        datePickerTimeline.deactivateDates(dates)

        confirm.setOnClickListener {
            showProgressDialog(getString(R.string.please_wait))
            Client.getClient()?.create(Service::class.java)?.AddDate("Bearer"+user.userData.token,lang.appLanguage,date,timeModel.actual!!)
                    ?.enqueue(object :Callback<TermsResponse>{
                        override fun onFailure(call: Call<TermsResponse>, t: Throwable) {
                            hideProgressDialog()
                            CommonUtil.handleException(mContext,t)
                            t.printStackTrace()
                            time_lay.visibility = View.GONE
                        }

                        override fun onResponse(call: Call<TermsResponse>, response: Response<TermsResponse>) {
                            hideProgressDialog()
                            time_lay.visibility = View.GONE
                            if (response.isSuccessful){
                                if (response.body()?.value.equals("1")){
                                    CommonUtil.makeToast(mContext,response.body()?.data!!)
                                    getData()
                                }else{
                                    CommonUtil.makeToast(mContext,response.body()?.msg!!)
                                }
                            }
                        }

                    })
        }

    }


    fun getData(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.MyDates("Bearer"+user.userData.token,lang.appLanguage)
                ?.enqueue(object : Callback<MyTimesModel> {
                    override fun onFailure(call: Call<MyTimesModel>, t: Throwable) {
                        hideProgressDialog()
                        CommonUtil.handleException(mContext,t)
                        t.printStackTrace()
                    }

                    override fun onResponse(call: Call<MyTimesModel>, response: Response<MyTimesModel>) {
                        hideProgressDialog()
                        if (response.isSuccessful){
                            if (response.body()?.value.equals("1")){
                                msg.text = response.body()?.msg
                                if (response.body()?.subscription==0){
                                    add.visibility = View.GONE
                                    if (response.body()?.msg!!.equals("")){
                                        set_pakage.visibility = View.VISIBLE
                                    }else{
                                        set.visibility= View.VISIBLE
                                    }
                                }else{
                                    add.visibility = View.VISIBLE
                                }
                                myTimesAdapter.updateAll(response.body()?.data!!)
                            }
                        }
                    }

                })
    }

    override fun onItemClick(view: View, position: Int) {
        if (view.id == R.id.time){
            timesAdapter.selected = position
            times.get(position).selected = true
            timeModel = times.get(position)
            timesAdapter.notifyDataSetChanged()
        }
    }
}