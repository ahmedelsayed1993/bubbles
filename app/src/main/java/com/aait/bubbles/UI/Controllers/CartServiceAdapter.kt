package com.aait.bubbles.UI.Controllers

import android.content.Context
import android.content.Intent
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.annotation.RequiresApi
import com.aait.bubbles.Base.ParentRecyclerAdapter
import com.aait.bubbles.Base.ParentRecyclerViewHolder
import com.aait.bubbles.Models.ServicesModel
import com.aait.bubbles.Models.TermsResponse
import com.aait.bubbles.Network.Client
import com.aait.bubbles.Network.Service
import com.aait.bubbles.R
import com.aait.bubbles.UI.Activities.Main.Client.MainActivity
import com.aait.bubbles.Utils.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class CartServiceAdapter (context: Context, data: MutableList<ServicesModel>, layoutId: Int) :
        ParentRecyclerAdapter<ServicesModel>(context, data, layoutId) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParentRecyclerViewHolder {
        val itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false)
        return ViewHolder(itemView)
    }
    var TextView.textColor: Int
        get() = currentTextColor
        set(v) = setTextColor(v)


    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onBindViewHolder(holder: ParentRecyclerViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder
        val questionModel = data.get(position)
        viewHolder.product!!.setText(questionModel.name)
        viewHolder.price.text = questionModel.price+mcontext.getString(R.string.rs)
        viewHolder.count.text = questionModel.count.toString()
        viewHolder.delete.setOnClickListener(View.OnClickListener {
//            view -> onItemClickListener.onItemClick(view,position)
            Client.getClient()?.create(Service::class.java)?.DeleteService(lang.appLanguage,questionModel.id!!)?.enqueue(object : Callback<TermsResponse>{
                override fun onFailure(call: Call<TermsResponse>, t: Throwable) {
                    CommonUtil.handleException(mcontext,t)
                    t.printStackTrace()
                }

                override fun onResponse(call: Call<TermsResponse>, response: Response<TermsResponse>) {
                    if (response.isSuccessful){
                        if (response.body()?.value.equals("1")){
                            CommonUtil.makeToast(mcontext,response.body()?.data!!)
                            mcontext.startActivity(Intent(mcontext,MainActivity::class.java))
                        }else{
                            CommonUtil.makeToast(mcontext,response.body()?.msg!!)
                        }
                    }
                }

            })

        })
    }
    inner class ViewHolder internal constructor(itemView: View) :
            ParentRecyclerViewHolder(itemView) {

        internal var product=itemView.findViewById<TextView>(R.id.product)
        internal var count=itemView.findViewById<TextView>(R.id.count)
        internal var price=itemView.findViewById<TextView>(R.id.price)
        internal var delete=itemView.findViewById<TextView>(R.id.delete)

    }
}