package com.aait.bubbles.UI.Fragments.Delegate

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.aait.bubbles.Base.BaseFragment
import com.aait.bubbles.Listeners.OnItemClickListener
import com.aait.bubbles.Models.BaseResponse
import com.aait.bubbles.Models.OrderModel
import com.aait.bubbles.Models.OrdersResponse
import com.aait.bubbles.Network.Client
import com.aait.bubbles.Network.Service
import com.aait.bubbles.R
import com.aait.bubbles.UI.Activities.Main.Client.PaymentActivity
import com.aait.bubbles.UI.Activities.Main.Delegate.NewOrderDetailsActivity
import com.aait.bubbles.UI.Controllers.OrderAdapter
import com.aait.bubbles.UI.Fragments.Delegate.HomeFragment
import com.aait.bubbles.Utils.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class HomeFragment :BaseFragment(),OnItemClickListener {
    override val layoutResource: Int
        get() = R.layout.app_recycle
    companion object {
        fun newInstance(): HomeFragment {
            val args = Bundle()
            val fragment = HomeFragment()
            fragment.setArguments(args)
            return fragment
        }
    }
    lateinit var rv_recycle: RecyclerView
    internal var layNoInternet: RelativeLayout? = null

    internal var layNoItem: RelativeLayout? = null

    internal var tvNoContent: TextView? = null

    var swipeRefresh: SwipeRefreshLayout? = null
    lateinit var linearLayoutManager: LinearLayoutManager
    lateinit var orderAdapter: OrderAdapter
    var orderModels = ArrayList<OrderModel>()
    override fun initializeComponents(view: View) {
        rv_recycle = view.findViewById(R.id.rv_recycle)
        layNoInternet = view.findViewById(R.id.lay_no_internet)
        layNoItem = view.findViewById(R.id.lay_no_item)
        tvNoContent = view.findViewById(R.id.tv_no_content)
        swipeRefresh = view.findViewById(R.id.swipe_refresh)
        linearLayoutManager = LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL,false)
        orderAdapter = OrderAdapter(mContext!!,orderModels, R.layout.recycle_orders)
        orderAdapter.setOnItemClickListener(this)
        rv_recycle.layoutManager = linearLayoutManager
        rv_recycle.adapter = orderAdapter
        swipeRefresh!!.setColorSchemeResources(
                R.color.colorPrimary,
                R.color.colorPrimaryDark,
                R.color.colorAccent
        )
        swipeRefresh!!.setOnRefreshListener {
            getData()

        }

        getData()
    }
    fun getData(){
        layNoInternet!!.visibility = View.GONE
        layNoItem!!.visibility = View.GONE
        Client.getClient()?.create(Service::class.java)?.OrdersDelegates(lang.appLanguage,"Bearer"+user.userData.token,"home")?.enqueue(object:
                Callback<OrdersResponse> {
            override fun onFailure(call: Call<OrdersResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext!!,t)
                t.printStackTrace()
                layNoInternet!!.visibility = View.VISIBLE
                layNoItem!!.visibility = View.GONE
                swipeRefresh!!.isRefreshing = false
            }

            override fun onResponse(
                    call: Call<OrdersResponse>,
                    response: Response<OrdersResponse>
            ) {
                hideProgressDialog()
                swipeRefresh!!.isRefreshing = false
                if(response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        if (response.body()!!.data?.isEmpty()!!) {
                            layNoItem!!.visibility = View.VISIBLE
                            layNoInternet!!.visibility = View.GONE
                            tvNoContent!!.setText(R.string.content_not_found_you_can_still_search_the_app_freely)

                        } else {
//
                            orderAdapter.updateAll(response.body()!!.data!!)
                        }
                    }else{
                        CommonUtil.makeToast(mContext!!,response.body()?.msg!!)
                    }
                }
            }

        })
    }
    override fun onItemClick(view: View, position: Int) {
        val intent = Intent(activity, NewOrderDetailsActivity::class.java)
        intent.putExtra("id",orderModels.get(position).id)
        startActivity(intent)
    }

}