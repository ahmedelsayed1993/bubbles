package com.aait.bubbles.UI.Activities.Main.Client

import android.content.Intent
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.aait.bubbles.Base.ParentActivity
import com.aait.bubbles.Listeners.OnItemClickListener
import com.aait.bubbles.Models.MyAddressModel
import com.aait.bubbles.Models.MyAddressResponse
import com.aait.bubbles.Models.TermsResponse
import com.aait.bubbles.Network.Client
import com.aait.bubbles.Network.Service
import com.aait.bubbles.R
import com.aait.bubbles.UI.Controllers.AdditionAdapter
import com.aait.bubbles.UI.Controllers.AddressesAdapter
import com.aait.bubbles.Utils.CommonUtil
import com.google.android.gms.common.api.Api
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MyAddressesActivity:ParentActivity() ,OnItemClickListener{
    override val layoutResource: Int
        get() = R.layout.activity_my_address
    lateinit var back:ImageView
    lateinit var title:TextView
    lateinit var rv_recycle: RecyclerView
    internal var layNoInternet: RelativeLayout? = null
    lateinit var add:Button
    internal var layNoItem: RelativeLayout? = null
    lateinit var iv_no_item:ImageView
    internal var tvNoContent: TextView? = null

    var swipeRefresh: SwipeRefreshLayout? = null
    lateinit var linearLayoutManager: LinearLayoutManager
    lateinit var addressesAdapter: AddressesAdapter
    lateinit var myAddressModel: MyAddressModel
    var addresses = ArrayList<MyAddressModel>()
    lateinit var text:TextView
    lateinit var icon:ImageView
    lateinit var text1:TextView
    override fun initializeComponents() {
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        rv_recycle = findViewById(R.id.rv_recycle)
        layNoInternet = findViewById(R.id.lay_no_internet)
        add = findViewById(R.id.add)
        text = findViewById(R.id.Text)
        icon = findViewById(R.id.icon)
        text1 = findViewById(R.id.text1)
        iv_no_item = findViewById(R.id.iv_no_item)
        layNoItem = findViewById(R.id.lay_no_item)
        tvNoContent = findViewById(R.id.tv_no_content)
        swipeRefresh = findViewById(R.id.swipe_refresh)
        linearLayoutManager = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        addressesAdapter = AddressesAdapter(mContext,addresses,R.layout.recycle_address)
        addressesAdapter.setOnItemClickListener(this)
        rv_recycle.layoutManager = linearLayoutManager
        rv_recycle.adapter = addressesAdapter
        back.setOnClickListener { onBackPressed()
            finish()}
        title.text = getString(R.string.my_addresses)
        swipeRefresh!!.setColorSchemeResources(
                R.color.colorPrimary,
                R.color.colorPrimaryDark,
                R.color.colorAccent
        )
        swipeRefresh!!.setOnRefreshListener {
            getData()

        }

        getData()
        add.setOnClickListener { startActivity(Intent(this,AddAddressActivity::class.java)) }

    }

    override fun onResume() {
        super.onResume()
        layNoInternet!!.visibility = View.GONE
        layNoItem!!.visibility = View.GONE
        Client.getClient()?.create(Service::class.java)?.MyAddress(lang.appLanguage,"Bearer"+user.userData.token)
                ?.enqueue(object : Callback<MyAddressResponse>{
                    override fun onFailure(call: Call<MyAddressResponse>, t: Throwable) {
                        hideProgressDialog()
                        CommonUtil.handleException(mContext,t)
                        t.printStackTrace()
                        layNoInternet!!.visibility = View.VISIBLE
                        layNoItem!!.visibility = View.GONE
                        swipeRefresh!!.isRefreshing = false
                    }

                    override fun onResponse(call: Call<MyAddressResponse>, response: Response<MyAddressResponse>) {
                        hideProgressDialog()
                        swipeRefresh!!.isRefreshing = false
                        if(response.isSuccessful){

                            if (response.body()?.value.equals("1")){
                                if (response.body()!!.data?.isEmpty()!!) {
                                    layNoItem!!.visibility = View.VISIBLE
                                    layNoInternet!!.visibility = View.GONE
                                    tvNoContent!!.setText(R.string.No_address_has_been_added_to_your_account)
                                    iv_no_item.setImageResource(R.mipmap.del)
                                    text.text = getString(R.string.Add_your_address_to_take_advantage_of_the_delivery_service)
                                    text1.visibility = View.GONE
                                    icon.visibility = View.GONE
                                } else {
//
                                    addressesAdapter.updateAll(response.body()!!.data!!)
                                }
                            }else{
                                CommonUtil.makeToast(mContext,response.body()?.msg!!)
                            }
                        }
                    }

                })
    }

    fun getData(){
        layNoInternet!!.visibility = View.GONE
        layNoItem!!.visibility = View.GONE
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.MyAddress(lang.appLanguage,"Bearer"+user.userData.token)
                ?.enqueue(object : Callback<MyAddressResponse>{
                    override fun onFailure(call: Call<MyAddressResponse>, t: Throwable) {
                        hideProgressDialog()
                        CommonUtil.handleException(mContext,t)
                        t.printStackTrace()
                        layNoInternet!!.visibility = View.VISIBLE
                        layNoItem!!.visibility = View.GONE
                        swipeRefresh!!.isRefreshing = false
                    }

                    override fun onResponse(call: Call<MyAddressResponse>, response: Response<MyAddressResponse>) {
                        hideProgressDialog()
                        swipeRefresh!!.isRefreshing = false
                        if(response.isSuccessful){

                            if (response.body()?.value.equals("1")){
                                if (response.body()!!.data?.isEmpty()!!) {
                                    layNoItem!!.visibility = View.VISIBLE
                                    layNoInternet!!.visibility = View.GONE
                                    tvNoContent!!.setText(R.string.No_address_has_been_added_to_your_account)
                                    iv_no_item.setImageResource(R.mipmap.del)
                                    text.text = getString(R.string.Add_your_address_to_take_advantage_of_the_delivery_service)
                                    text1.visibility = View.GONE
                                    icon.visibility = View.GONE
                                } else {
//
                                    addressesAdapter.updateAll(response.body()!!.data!!)
                                }
                            }else{
                                CommonUtil.makeToast(mContext,response.body()?.msg!!)
                            }
                        }
                    }

                })
    }

    override fun onItemClick(view: View, position: Int) {
        if (view.id == R.id.delete){
            showProgressDialog(getString(R.string.please_wait))
            Client.getClient()?.create(Service::class.java)?.DeleteAddress(lang.appLanguage,"Bearer"+user.userData.token,addresses.get(position).id!!)
                    ?.enqueue(object :Callback<TermsResponse>{
                        override fun onFailure(call: Call<TermsResponse>, t: Throwable) {
                            hideProgressDialog()
                            CommonUtil.handleException(mContext,t)
                            t.printStackTrace()
                        }

                        override fun onResponse(call: Call<TermsResponse>, response: Response<TermsResponse>) {
                            hideProgressDialog()
                            if (response.isSuccessful){
                                if (response.body()?.value.equals("1")){
                                    CommonUtil.makeToast(mContext,response.body()?.data!!)
                                    getData()
                                }else{
                                    CommonUtil.makeToast(mContext,response.body()?.msg!!)
                                }
                            }
                        }

                    })

        }else{

                val returnIntent = Intent()
                returnIntent.putExtra("result", addresses.get(position).address)
                returnIntent.putExtra("lat", addresses.get(position).lat)
                returnIntent.putExtra("lng", addresses.get(position).lng)
                setResult(1, returnIntent)
                finish()
        }
    }
}