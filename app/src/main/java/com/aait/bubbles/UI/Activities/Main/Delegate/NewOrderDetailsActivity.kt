package com.aait.bubbles.UI.Activities.Main.Delegate

import android.content.Intent
import android.net.Uri
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aait.bubbles.Base.ParentActivity
import com.aait.bubbles.Models.CartModel
import com.aait.bubbles.Models.DelegateOrderDetailsResponse
import com.aait.bubbles.Network.Client
import com.aait.bubbles.Network.Service
import com.aait.bubbles.R
import com.aait.bubbles.UI.Controllers.OrderProductAdapter
import com.aait.bubbles.Utils.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class NewOrderDetailsActivity:ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_order_delegate_details
    var id = 0
    lateinit var title:TextView
    lateinit var back:ImageView
    lateinit var name:TextView
    lateinit var phone:TextView
    lateinit var receive_date:TextView
    lateinit var delivery_date:TextView
    lateinit var orders:RecyclerView
    lateinit var location:TextView
    lateinit var payment:TextView
    lateinit var notes:TextView
    lateinit var prods_value:TextView
    lateinit var added_value:TextView
    lateinit var added:TextView
    lateinit var delivery_value:TextView
    lateinit var total:TextView
    lateinit var accept:Button
    lateinit var refuse:Button
    lateinit var orderProductAdapter: OrderProductAdapter
    lateinit var linearLayoutManager: LinearLayoutManager
    var cartModels = ArrayList<CartModel>()
    var lat = ""
    var lng = ""
    override fun initializeComponents() {
        id = intent.getIntExtra("id",0)
        title = findViewById(R.id.title)
        back = findViewById(R.id.back)
        name = findViewById(R.id.name)
        phone = findViewById(R.id.phone)
        receive_date = findViewById(R.id.receive_date)
        delivery_date = findViewById(R.id.delivery_date)
        orders = findViewById(R.id.orders)
        location = findViewById(R.id.location)
        payment = findViewById(R.id.payment)
        notes = findViewById(R.id.notes)
        prods_value = findViewById(R.id.prods_value)
        added_value = findViewById(R.id.added_value)
        added = findViewById(R.id.added)
        delivery_value = findViewById(R.id.delivery_value)
        total = findViewById(R.id.total)
        accept = findViewById(R.id.accept)
        refuse = findViewById(R.id.refuse)
        back.setOnClickListener { onBackPressed()
        finish()}
        title.text = getString(R.string.order_details)
        linearLayoutManager = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        orderProductAdapter = OrderProductAdapter(mContext,cartModels,R.layout.recycle_cart_products)
        orders.layoutManager = linearLayoutManager
        orders.adapter = orderProductAdapter
        getData()

        location.setOnClickListener {
            startActivity(
                    Intent(
                            Intent.ACTION_VIEW,
                            Uri.parse("http://maps.google.com/maps?saddr=" + user.userData.lat + "," + user.userData.lng + "&daddr=" + lat + "," + lng)
                    )
            )

        }
        accept.setOnClickListener {
            showProgressDialog(getString(R.string.please_wait))
            Client.getClient()?.create(Service::class.java)?.DelegateOrderDetails(lang.appLanguage,"Bearer"+user.userData.token,id,"delegate_accepted")
                    ?.enqueue(object : Callback<DelegateOrderDetailsResponse>{
                        override fun onFailure(call: Call<DelegateOrderDetailsResponse>, t: Throwable) {
                            hideProgressDialog()
                            CommonUtil.handleException(mContext,t)
                            t.printStackTrace()
                        }

                        override fun onResponse(call: Call<DelegateOrderDetailsResponse>, response: Response<DelegateOrderDetailsResponse>) {
                            hideProgressDialog()
                            if (response.isSuccessful){
                                if (response.body()?.value.equals("1")){
                                  //  CommonUtil.makeToast(mContext,response.body()?.msg!!)
                                    val intent = Intent(this@NewOrderDetailsActivity,OrderAcceptedActivity::class.java)
                                    intent.putExtra("id",id)
                                    startActivity(intent)
                                    finish()
                                }else{
                                    CommonUtil.makeToast(mContext,response.body()?.msg!!)
                                }
                            }
                        }

                    })
        }
        refuse.setOnClickListener {
            showProgressDialog(getString(R.string.please_wait))
            Client.getClient()?.create(Service::class.java)?.DelegateOrderDetails(lang.appLanguage,"Bearer"+user.userData.token,id,"delegate_refused")
                    ?.enqueue(object : Callback<DelegateOrderDetailsResponse>{
                        override fun onFailure(call: Call<DelegateOrderDetailsResponse>, t: Throwable) {
                            hideProgressDialog()
                            CommonUtil.handleException(mContext,t)
                            t.printStackTrace()
                        }

                        override fun onResponse(call: Call<DelegateOrderDetailsResponse>, response: Response<DelegateOrderDetailsResponse>) {
                            hideProgressDialog()
                            if (response.isSuccessful){
                                if (response.body()?.value.equals("1")){
                                   // CommonUtil.makeToast(mContext,response.body()?.msg!!)
                                    startActivity(Intent(this@NewOrderDetailsActivity,MainActivity::class.java))
                                    finish()
                                }else{
                                    CommonUtil.makeToast(mContext,response.body()?.msg!!)
                                }
                            }
                        }

                    })
        }


    }

    fun getData(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.DelegateOrderDetails(lang.appLanguage,"Bearer"+user.userData.token,id,null)
                ?.enqueue(object : Callback<DelegateOrderDetailsResponse>{
                    override fun onFailure(call: Call<DelegateOrderDetailsResponse>, t: Throwable) {
                        hideProgressDialog()
                        CommonUtil.handleException(mContext,t)
                        t.printStackTrace()
                    }

                    override fun onResponse(call: Call<DelegateOrderDetailsResponse>, response: Response<DelegateOrderDetailsResponse>) {
                        hideProgressDialog()
                        if (response.isSuccessful){
                            if (response.body()?.value.equals("1")){
                                name.text = response.body()?.data?.username
                                phone.text = response.body()?.data?.phone
                                delivery_date.text = response.body()?.data?.delivery_date
                                receive_date.text = response.body()?.data?.received_date
                                location.text = response.body()?.data?.address
                                lat = response.body()?.data?.lat!!
                                lng = response.body()?.data?.lng!!
                                notes.text = response.body()?.data?.notes
                                payment.text = response.body()?.data?.payment
                                prods_value.text = response.body()?.data?.total+getString(R.string.rs)
                                added_value.text = getString(R.string.Value_Added)+"("+response.body()?.data?.tax+"%)"
                                added.text = response.body()?.data?.total_tax+getString(R.string.rs)
                                delivery_value.text = response.body()?.data?.total_delivery+getString(R.string.rs)
                                total.text = response.body()?.data?.final_total+getString(R.string.rs)
                                orderProductAdapter.updateAll(response.body()?.data?.products!!)
                            }else{
                                CommonUtil.makeToast(mContext,response.body()?.msg!!)
                            }
                        }
                    }

                })
    }
}