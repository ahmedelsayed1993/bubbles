package com.aait.bubbles.UI.Activities.Main.Client

import android.Manifest
import android.content.DialogInterface
import android.content.Intent
import android.location.Address
import android.location.Geocoder
import android.os.Build
import android.provider.Settings
import android.util.Log
import android.view.View
import android.widget.*
import androidx.appcompat.app.AlertDialog
//import androidx.sqlite.db.SupportSQLiteOpenHelper
import com.aait.bubbles.Base.ParentActivity
import com.aait.bubbles.GPS.GPSTracker
import com.aait.bubbles.GPS.GpsTrakerListener
import com.aait.bubbles.Models.TermsResponse
import com.aait.bubbles.Network.Client
import com.aait.bubbles.Network.Service
import com.aait.bubbles.R
import com.aait.bubbles.Utils.CommonUtil
import com.aait.bubbles.Utils.DialogUtil
import com.aait.bubbles.Utils.PermissionUtils
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException
import java.util.*

class AddAddressActivity:ParentActivity(), OnMapReadyCallback, GoogleMap.OnMapClickListener,
        GpsTrakerListener {
    override val layoutResource: Int
        get() = R.layout.activity_add_address
    lateinit var map: MapView
    lateinit var choose: Button
    lateinit var address:TextView
    lateinit var location:TextView
    lateinit var lay:LinearLayout
    lateinit var loc_switch:Switch
    lateinit var home:LinearLayout
    lateinit var work:LinearLayout
    lateinit var rest:LinearLayout
    lateinit var type_lay:LinearLayout
    internal lateinit var googleMap: GoogleMap
    internal lateinit var myMarker: Marker
    internal lateinit var geocoder: Geocoder
    internal lateinit var gps: GPSTracker
    internal var startTracker = false

    var mLang = ""
    var mLat = ""
    var result = ""
    private var mAlertDialog: AlertDialog? = null
    override fun initializeComponents() {
        map = findViewById(R.id.map)
        choose = findViewById(R.id.choose)
        address = findViewById(R.id.address)
        location = findViewById(R.id.location)
        lay = findViewById(R.id.lay)
        loc_switch = findViewById(R.id.loc_switch)
        home = findViewById(R.id.home)
        work = findViewById(R.id.work)
        rest = findViewById(R.id.rest)
        type_lay = findViewById(R.id.type_lay)
        map.onCreate(mSavedInstanceState)
        map.onResume()
        map.getMapAsync(this)

        try {
            MapsInitializer.initialize(mContext)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        lay.visibility = View.GONE
        type_lay.visibility = View.GONE
        choose.setOnClickListener {

                lay.visibility = View.VISIBLE
            choose.visibility = View.GONE

        }


        loc_switch.setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener { buttonView, isChecked ->
            // do something, the isChecked will be
            // true if the switch is in the On position
            if (isChecked) {
                // theme.appTheme =
               type_lay.visibility = View.VISIBLE

//                activity?.finishAffinity()
//                startActivity(Intent(activity, SplashActivity::class.java))
            }else{
                //  theme.appTheme =
               type_lay.visibility = View.GONE

//                activity?.finishAffinity()
//                startActivity(Intent(activity, SplashActivity::class.java))
            }
        })
        home.setOnClickListener { Add("home") }
        work.setOnClickListener { Add("work") }
        rest.setOnClickListener { Add("rest") }
    }

    fun Add(type:String){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.AddAddress(lang.appLanguage,"Bearer"+user.userData.token
        ,type,result,mLat,mLang)?.enqueue(object : Callback<TermsResponse> {
            override fun onFailure(call: Call<TermsResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
            }

            override fun onResponse(call: Call<TermsResponse>, response: Response<TermsResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        CommonUtil.makeToast(mContext,response.body()?.data!!)
                        onBackPressed()
                        finish()
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }

    override fun onMapReady(p0: GoogleMap) {
        this.googleMap = p0!!
        googleMap.setOnMapClickListener(this)
        getLocationWithPermission()
    }

    override fun onMapClick(p0: LatLng) {
        Log.e("LatLng", p0.toString())
        mLang =  java.lang.Double.toString(p0?.latitude!!)
        mLat =  java.lang.Double.toString(p0?.longitude!!)
        if (myMarker != null) {
            myMarker.remove()
            putMapMarker(p0?.latitude, p0?.longitude)
        } else {
            putMapMarker(p0?.latitude, p0?.longitude)
        }

        if (p0?.latitude != 0.0 && p0?.longitude != 0.0) {
            putMapMarker(p0?.latitude, p0?.longitude)
            mLat = p0?.latitude.toString()
            mLang = p0?.longitude.toString()
            val addresses: List<Address>

            geocoder = Geocoder(this, Locale.getDefault())

            try {
                addresses = geocoder.getFromLocation(
                        java.lang.Double.parseDouble(mLat),
                        java.lang.Double.parseDouble(mLang),
                        1
                )


                if (addresses.isEmpty()) {
                    Toast.makeText(
                            this,
                            resources.getString(R.string.detect_location),
                            Toast.LENGTH_SHORT
                    ).show()
                } else {
                    result = addresses[0].getAddressLine(0)
                    Log.e("address",result)
                    location.text = result
                    address.text = result
                    CommonUtil.makeToast(mContext,addresses[0].getAddressLine(0))
                }


            } catch (e: IOException) {
            }

            googleMap.clear()
            putMapMarker(p0?.latitude, p0?.longitude)
        }
    }

    override fun onTrackerSuccess(lat: Double?, log: Double?) {
        if (startTracker) {
            if (lat != 0.0 && log != 0.0) {
                hideProgressDialog()
                putMapMarker(lat, log)
            }
        }
    }

    override fun onStartTracker() {
        startTracker = true
    }

    fun putMapMarker(lat: Double?, log: Double?) {
        val latLng = LatLng(lat!!, log!!)
        myMarker = googleMap.addMarker(
                MarkerOptions()
                        .position(latLng)
                        .title("موقعي")
                        .icon(BitmapDescriptorFactory.fromResource(R.mipmap.location))
        )!!
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 10f))
        // kkjgj

    }


    fun getLocationWithPermission() {
        gps = GPSTracker(mContext, this)
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!(PermissionUtils.hasPermissions(mContext, Manifest.permission.ACCESS_FINE_LOCATION)&&
                            (PermissionUtils.hasPermissions(mContext,
                                    Manifest.permission.ACCESS_COARSE_LOCATION)))) {
                CommonUtil.PrintLogE("Permission not granted")
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(
                            PermissionUtils.GPS_PERMISSION,
                            800
                    )
                    Log.e("GPS", "1")
                }
            } else {
                getCurrentLocation()
                Log.e("GPS", "2")
            }
        } else {
            Log.e("GPS", "3")
            getCurrentLocation()
        }

    }

    internal fun getCurrentLocation() {
        gps.getLocation()
        if (!gps.canGetLocation()) {
            mAlertDialog = DialogUtil.showAlertDialog(mContext,
                    getString(R.string.gps_detecting),
                    DialogInterface.OnClickListener { dialogInterface, i ->
                        mAlertDialog?.dismiss()
                        val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                        startActivityForResult(intent, 300)
                    })
        } else {
            if (gps.getLatitude() !== 0.0 && gps.getLongitude() !== 0.0) {
                putMapMarker(gps.getLatitude(), gps.getLongitude())
                mLat = gps.getLatitude().toString()
                mLang = gps.getLongitude().toString()
                val addresses: List<Address>
                geocoder = Geocoder(this, Locale.getDefault())
                try {
                    addresses = geocoder.getFromLocation(
                            java.lang.Double.parseDouble(mLat),
                            java.lang.Double.parseDouble(mLang),
                            1
                    )
                    if (addresses.isEmpty()) {
                        Toast.makeText(
                                mContext,
                                resources.getString(R.string.detect_location),
                                Toast.LENGTH_SHORT
                        ).show()
                    } else {
                        result = addresses[0].getAddressLine(0)
                        Log.e("address",result)
                        location.text = result
                        address.text = result
                        CommonUtil.makeToast(mContext,addresses[0].getAddressLine(0))



                    }

                } catch (e: IOException) {
                }

                googleMap.clear()
                putMapMarker(gps.getLatitude(), gps.getLongitude())


            }
        }
    }
}