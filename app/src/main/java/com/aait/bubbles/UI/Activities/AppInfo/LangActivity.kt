package com.aait.bubbles.UI.Activities.AppInfo

import android.content.Intent
import android.view.View
import android.widget.ImageView
import android.widget.RadioButton
import android.widget.RadioGroup
import android.widget.TextView
import com.aait.bubbles.Base.ParentActivity
import com.aait.bubbles.R
import com.aait.bubbles.UI.Activities.SplashActivity

class LangActivity:ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_lang
    lateinit var back:ImageView
    lateinit var title:TextView

    lateinit var lay:RadioGroup
    lateinit var arabic:RadioButton
    lateinit var english:RadioButton


    override fun initializeComponents() {
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)

        lay = findViewById(R.id.lay)
        arabic = findViewById(R.id.arabic)
        english = findViewById(R.id.english)
        back.setOnClickListener { onBackPressed()
        finish()}
        title.text = getString(R.string.language)
        if (lang.appLanguage == "ar"){

            arabic.isChecked = true
        }else{

            english.isChecked = true
        }
        arabic.setOnClickListener {
            lang.appLanguage = "ar"
            finishAffinity()
            startActivity(Intent(this, SplashActivity::class.java))
        }
        english.setOnClickListener {
            lang.appLanguage = "en"
            finishAffinity()
            startActivity(Intent(this, SplashActivity::class.java))
        }

    }
}