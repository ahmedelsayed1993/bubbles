package com.aait.bubbles.UI.Activities.Main.Client

import android.content.Intent
import android.widget.TextView
import com.aait.bubbles.Base.ParentActivity
import com.aait.bubbles.R

class BackToMainActivity:ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_order_done

    lateinit var bac:TextView
    override fun initializeComponents() {
        bac = findViewById(R.id.bac)
        bac.setOnClickListener { startActivity(Intent(this,MainActivity::class.java))
        finish()}

    }

    override fun onBackPressed() {
        super.onBackPressed()
        startActivity(Intent(this,MainActivity::class.java))
        finish()
    }
}