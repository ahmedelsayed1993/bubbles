package com.aait.bubbles.UI.Fragments

import android.Manifest
import android.content.DialogInterface
import android.content.Intent
import android.location.Address
import android.location.Geocoder
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.aait.bubbles.Base.BaseFragment
import com.aait.bubbles.GPS.GPSTracker
import com.aait.bubbles.GPS.GpsTrakerListener
import com.aait.bubbles.Listeners.OnItemClickListener
import com.aait.bubbles.Models.CategoryResponse
import com.aait.bubbles.Models.FavResponse
import com.aait.bubbles.Models.MyFavResponse
import com.aait.bubbles.Models.ProductModel
import com.aait.bubbles.Network.Client
import com.aait.bubbles.Network.Service
import com.aait.bubbles.R
import com.aait.bubbles.UI.Activities.Main.Client.ProductDetailsActivity
import com.aait.bubbles.UI.Activities.Main.Client.StoreActivity
import com.aait.bubbles.UI.Activities.Main.Client.SubCategoryActivity
import com.aait.bubbles.UI.Controllers.FavouriteAdapter
import com.aait.bubbles.Utils.CommonUtil
import com.aait.bubbles.Utils.DialogUtil
import com.aait.bubbles.Utils.PermissionUtils
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.Marker
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException
import java.util.*
import kotlin.collections.ArrayList

class FavFragment:BaseFragment(),OnItemClickListener,GpsTrakerListener {
    override val layoutResource: Int
        get() = R.layout.fragment_fav
    companion object {
        fun newInstance(): FavFragment {
            val args = Bundle()
            val fragment = FavFragment()
            fragment.setArguments(args)
            return fragment
        }
    }
    lateinit var rv_recycle: RecyclerView
    internal var layNoInternet: RelativeLayout? = null

    internal var layNoItem: RelativeLayout? = null

    internal var tvNoContent: TextView? = null
    lateinit var iv_no_item:ImageView

    var swipeRefresh: SwipeRefreshLayout? = null
    lateinit var linearLayoutManager: LinearLayoutManager
    lateinit var favouriteAdapter: FavouriteAdapter
    var productModels = ArrayList<ProductModel>()
    private var mAlertDialog: AlertDialog? = null
    internal  var googleMap: GoogleMap?=null
    internal lateinit var myMarker: Marker
    internal lateinit var geocoder: Geocoder
    internal lateinit var gps: GPSTracker
    internal var startTracker = false
    var mLang = ""
    var mLat = ""
    var result = ""
    lateinit var text:TextView
    lateinit var icon:ImageView
    lateinit var text1:TextView
    override fun initializeComponents(view: View) {
        rv_recycle = view.findViewById(R.id.rv_recycle)
        layNoInternet = view.findViewById(R.id.lay_no_internet)
        layNoItem = view.findViewById(R.id.lay_no_item)
        tvNoContent = view.findViewById(R.id.tv_no_content)
        swipeRefresh = view.findViewById(R.id.swipe_refresh)
        iv_no_item = view.findViewById(R.id.iv_no_item)
        text = view.findViewById(R.id.Text)
        icon = view.findViewById(R.id.icon)
        text1 = view.findViewById(R.id.text1)
        linearLayoutManager = LinearLayoutManager(mContext!!,LinearLayoutManager.VERTICAL,false)
        favouriteAdapter = FavouriteAdapter(mContext!!,productModels,R.layout.recycle_favourite)
        favouriteAdapter.setOnItemClickListener(this)
        rv_recycle.layoutManager = linearLayoutManager
        rv_recycle.adapter = favouriteAdapter
        swipeRefresh!!.setColorSchemeResources(
                R.color.colorPrimary,
                R.color.colorPrimaryDark,
                R.color.colorAccent
        )
        swipeRefresh!!.setOnRefreshListener {
            getLocationWithPermission(null)

        }

        getLocationWithPermission(null)

    }

    override fun onItemClick(view: View, position: Int) {
        if (view.id == R.id.fav){
            if (user.loginStatus!!){
                showProgressDialog(getString(R.string.please_wait))
                Client.getClient()?.create(Service::class.java)?.Favourite(lang.appLanguage,"Bearer"+user.userData.token
                        ,productModels.get(position).id!!)?.enqueue(object :Callback<FavResponse>{
                    override fun onFailure(call: Call<FavResponse>, t: Throwable) {
                        hideProgressDialog()
                        CommonUtil.handleException(mContext!!,t)
                        t.printStackTrace()
                    }

                    override fun onResponse(call: Call<FavResponse>, response: Response<FavResponse>) {
                        hideProgressDialog()
                        if (response.isSuccessful){
                            if (response.body()?.value.equals("1")){
                                getLocationWithPermission(null)
                                CommonUtil.makeToast(mContext!!,response.body()?.msg!!)
                            }else{
                                CommonUtil.makeToast(mContext!!,response.body()?.msg!!)
                            }
                        }
                    }

                })
            }
        }else{
            if (productModels.get(position).category.equals("category")) {
                val intent = Intent(activity, SubCategoryActivity::class.java)
                intent.putExtra("category", productModels.get(position).category_id)
                intent.putExtra("favo", 1)
                startActivity(intent)
            }else{
                val intent = Intent(activity, ProductDetailsActivity::class.java)
                intent.putExtra("id",productModels.get(position).id)
                intent.putExtra("cat",productModels.get(position).category_id)
                startActivity(intent)
            }
        }

    }

    override fun onTrackerSuccess(lat: Double?, log: Double?) {
        if (startTracker) {
            if (lat != 0.0 && log != 0.0) {
                hideProgressDialog()
                // putMapMarker(lat, log)
            }
        }
    }

    override fun onStartTracker() {
        startTracker = true
    }
    fun getLocationWithPermission(cat:Int?) {
        gps = GPSTracker(mContext!!, this)
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!(PermissionUtils.hasPermissions(mContext, Manifest.permission.ACCESS_FINE_LOCATION)&&
                            (PermissionUtils.hasPermissions(mContext,
                                    Manifest.permission.ACCESS_COARSE_LOCATION)))) {
                CommonUtil.PrintLogE("Permission not granted")
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(
                            PermissionUtils.GPS_PERMISSION,
                            800
                    )
                    Log.e("GPS", "1")
                }
            } else {
                getCurrentLocation(cat)
                Log.e("GPS", "2")
            }
        } else {
            Log.e("GPS", "3")
            getCurrentLocation(cat)
        }

    }

    internal fun getCurrentLocation(cat:Int?) {
        gps.getLocation()
        if (!gps.canGetLocation()) {
            mAlertDialog = DialogUtil.showAlertDialog(mContext!!,
                    getString(R.string.gps_detecting),
                    DialogInterface.OnClickListener { dialogInterface, i ->
                        mAlertDialog?.dismiss()
                        val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                        startActivityForResult(intent, 300)
                    })
        } else {
            if (gps.getLatitude() !== 0.0 && gps.getLongitude() !== 0.0) {
                // putMapMarker(gps.getLatitude(), gps.getLongitude())
                if (user.loginStatus!!) {
                    getData("Bearer"+user.userData.token,gps.getLatitude().toString(), gps.getLongitude().toString(),cat)
                }else{

                }
                mLat = gps.getLatitude().toString()
                mLang = gps.getLongitude().toString()
                val addresses: List<Address>
                geocoder = Geocoder(mContext!!, Locale.getDefault())
                try {
                    addresses = geocoder.getFromLocation(
                            java.lang.Double.parseDouble(mLat),
                            java.lang.Double.parseDouble(mLang),
                            1
                    )
                    if (addresses.isEmpty()) {
                        Toast.makeText(
                                mContext,
                                resources.getString(R.string.detect_location),
                                Toast.LENGTH_SHORT
                        ).show()
                    } else {
                        result = addresses[0].getAddressLine(0)
                        Log.e("address",result)
                        // CommonUtil.makeToast(mContext,addresses[0].getAddressLine(0))



                    }

                } catch (e: IOException) {
                }


                //putMapMarker(gps.getLatitude(), gps.getLongitude())


            }
        }
    }
    fun getData(token:String,lat:String,lng:String,id:Int?){
        layNoInternet!!.visibility = View.GONE
        layNoItem!!.visibility = View.GONE
        Client.getClient()?.create(Service::class.java)?.MyFavs(token,lang.appLanguage,lat,lng)?.enqueue(object:
                Callback<MyFavResponse> {
            override fun onFailure(call: Call<MyFavResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext!!,t)
                t.printStackTrace()
                layNoInternet!!.visibility = View.VISIBLE
                layNoItem!!.visibility = View.GONE
                swipeRefresh!!.isRefreshing = false
            }

            override fun onResponse(
                    call: Call<MyFavResponse>,
                    response: Response<MyFavResponse>
            ) {
                hideProgressDialog()
                swipeRefresh!!.isRefreshing = false
                if(response.isSuccessful){

                    if (response.body()?.value.equals("1")){
                        if (response.body()!!.data?.isEmpty()!!) {
                            layNoItem!!.visibility = View.VISIBLE
                            layNoInternet!!.visibility = View.GONE
                            tvNoContent!!.setText(R.string.Nothing_has_been_added_to_favourites)
                            iv_no_item.setImageResource(R.mipmap.favo)
                            text.setText( mContext!!.getString(R.string.Click_on_the_sign))
                            icon.setImageResource(R.mipmap.favactive)
                            text1.text =mContext!!.getString(R.string.to_add)
                        } else {
//
                            favouriteAdapter.updateAll(response.body()!!.data!!)
                        }

                    }else{
                        CommonUtil.makeToast(mContext!!,response.body()?.msg!!)
                    }
                }
            }

        })
    }
}