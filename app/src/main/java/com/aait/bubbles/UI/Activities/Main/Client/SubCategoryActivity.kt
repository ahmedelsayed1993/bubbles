package com.aait.bubbles.UI.Activities.Main.Client

import android.Manifest
import android.content.DialogInterface
import android.content.Intent
import android.location.Address
import android.location.Geocoder
import android.os.Build
import android.provider.Settings
import android.util.Log
import android.view.View
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.aait.bubbles.Base.ParentActivity
import com.aait.bubbles.GPS.GPSTracker
import com.aait.bubbles.GPS.GpsTrakerListener
import com.aait.bubbles.Listeners.OnItemClickListener
import com.aait.bubbles.Models.*
import com.aait.bubbles.Network.Client
import com.aait.bubbles.Network.Service
import com.aait.bubbles.R
import com.aait.bubbles.UI.Controllers.ProductAdapter
import com.aait.bubbles.UI.Controllers.ServicesAdapter
import com.aait.bubbles.UI.Controllers.SubCatsAdapter
import com.aait.bubbles.Utils.CommonUtil
import com.aait.bubbles.Utils.DialogUtil
import com.aait.bubbles.Utils.PermissionUtils
import com.bumptech.glide.Glide
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.Marker
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException
import java.util.*
import kotlin.collections.ArrayList

class SubCategoryActivity:ParentActivity(),OnItemClickListener, GpsTrakerListener {
    override val layoutResource: Int
        get() = R.layout.activity_subcats
    lateinit var back:ImageView
    lateinit var title:TextView
    lateinit var subs:RecyclerView
    lateinit var rv_recycle: RecyclerView
    internal var layNoInternet: RelativeLayout? = null

    internal var layNoItem: RelativeLayout? = null

    internal var tvNoContent: TextView? = null

    var swipeRefresh: SwipeRefreshLayout? = null
    lateinit var linearLayoutManager: LinearLayoutManager
    lateinit var request:Button
    private var mAlertDialog: AlertDialog? = null
    internal  var googleMap: GoogleMap?=null
    internal lateinit var myMarker: Marker
    internal lateinit var geocoder: Geocoder
    internal lateinit var gps: GPSTracker
    internal var startTracker = false
    var mLang = ""
    var mLat = ""
    var result = ""
    lateinit var subCatsAdapter: SubCatsAdapter
    lateinit var productAdapter: ProductAdapter
    lateinit var linearLayoutManager1: LinearLayoutManager
    var subCatModels = ArrayList<SubCatModel>()
    var productModels = ArrayList<ProductModel>()
    lateinit var subCatModel: SubCatModel
     var cat = 0
    var ID = ""
    lateinit var service_lay:LinearLayout
    lateinit var services:RecyclerView
    lateinit var total:TextView
    lateinit var confirm:Button
    lateinit var cancel:Button
    lateinit var mlinearLayoutManager: LinearLayoutManager
    lateinit var servicesAdapter: ServicesAdapter
    var servicesModels = ArrayList<ServicesModel>()
    var tot = 0
    var models = ArrayList<Model>()
    var product = 0
    lateinit var price_lay:LinearLayout
    lateinit var piece:TextView
    lateinit var price:TextView
     var order_id = 0
    var favo = 0
    override fun initializeComponents() {
        ID =  Settings.Secure.getString(mContext.contentResolver,Settings.Secure.ANDROID_ID)
        cat = intent.getIntExtra("category",0)
        favo = intent.getIntExtra("favo",0)
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        subs = findViewById(R.id.subs)
        rv_recycle = findViewById(R.id.rv_recycle)
        tvNoContent = findViewById(R.id.tv_no_content)
        layNoInternet = findViewById(R.id.lay_no_internet)
        layNoItem = findViewById(R.id.lay_no_item)
        swipeRefresh = findViewById(R.id.swipe_refresh)
        service_lay = findViewById(R.id.service_lay)
        services = findViewById(R.id.services)
        total = findViewById(R.id.total)
        confirm = findViewById(R.id.confirm)
        cancel = findViewById(R.id.cancel)
        price_lay = findViewById(R.id.price_lay)
        piece = findViewById(R.id.piece)
        price = findViewById(R.id.price)
        request = findViewById(R.id.request)
        mlinearLayoutManager = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        servicesAdapter = ServicesAdapter(mContext,servicesModels,R.layout.recycle_services)
        servicesAdapter.setOnItemClickListener(this)
        services.layoutManager = mlinearLayoutManager
        services.adapter = servicesAdapter
        linearLayoutManager = LinearLayoutManager(mContext,LinearLayoutManager.HORIZONTAL,false)
        subCatsAdapter = SubCatsAdapter(mContext,subCatModels,R.layout.recycler_sub_cat)
        subCatsAdapter.setOnItemClickListener(this)
        subs.layoutManager = linearLayoutManager
        subs.adapter = subCatsAdapter
        linearLayoutManager1 = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        productAdapter = ProductAdapter(mContext,productModels,R.layout.recycle_products)
        productAdapter.setOnItemClickListener(this)
        rv_recycle.layoutManager = linearLayoutManager1
        rv_recycle.adapter = productAdapter
        back.setOnClickListener { onBackPressed()
        finish()}
        title.text = ""
        swipeRefresh!!.setColorSchemeResources(
                R.color.colorPrimary,
                R.color.colorPrimaryDark,
                R.color.colorAccent
        )
        swipeRefresh!!.setOnRefreshListener {
            getLocationWithPermission(null)

        }

        getLocationWithPermission(null)
        confirm.setOnClickListener {
            models.clear()
            for (i in 0..servicesAdapter.data?.size-1){
                if (servicesAdapter.data?.get(i).count==0){

                }else{
                   models.add(Model(product,servicesAdapter.data?.get(i)?.id,servicesAdapter.data?.get(i).price,servicesAdapter.data?.get(i)?.count!!.toInt()))
                }
            }
            Log.e("model",Gson().toJson(models))
            if (models.isEmpty()){
                CommonUtil.makeToast(mContext,getString(R.string.choose_type))
            }else{
                showProgressDialog(getString(R.string.please_wait))
                Client.getClient()?.create(Service::class.java)?.AddToCart(lang.appLanguage,ID,cat!!
                ,Gson().toJson(models))?.enqueue(object :Callback<CartResponse>{
                    override fun onFailure(call: Call<CartResponse>, t: Throwable) {
                        hideProgressDialog()
                        CommonUtil.handleException(mContext,t)
                        t.printStackTrace()
                    }

                    override fun onResponse(call: Call<CartResponse>, response: Response<CartResponse>) {
                        hideProgressDialog()
                        if (response.isSuccessful){
                            if (response.body()?.value.equals("1")){
                                getLocationWithPermission(null)
                                service_lay.visibility = View.GONE
                            }else{
                                CommonUtil.makeToast(mContext,response.body()?.msg!!)
                                service_lay.visibility = View.GONE
                            }
                        }
                    }

                })
            }
        }
        request.setOnClickListener {if (user.loginStatus!!) {
            val intent = Intent(this, DeliveryRequestActivity::class.java)
            intent.putExtra("cat", cat)
            startActivity(intent)
        }else{
            CommonUtil.makeToast(mContext!!,getString(R.string.sorry_visitor))
        }
        }
        cancel.setOnClickListener { service_lay.visibility = View.GONE }
        price_lay.setOnClickListener { if (user.loginStatus!!) {
            val intent = Intent(this, BasketActivity::class.java)
            intent.putExtra("id", order_id)
            intent.putExtra("cat", cat)
            startActivity(intent)
        }else{
                CommonUtil.makeToast(mContext!!,getString(R.string.sorry_visitor))
            }
        }


    }
    fun showService(id:Int){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Services(lang.appLanguage,id,mLat,mLang,ID)?.enqueue(
                object :Callback<ServicesResponse>{
                    override fun onFailure(call: Call<ServicesResponse>, t: Throwable) {
                        hideProgressDialog()
                        CommonUtil.handleException(mContext,t)
                        t.printStackTrace()
                        service_lay.visibility = View.GONE
                    }

                    override fun onResponse(call: Call<ServicesResponse>, response: Response<ServicesResponse>) {
                        hideProgressDialog()
                        if (response.isSuccessful){
                            if (response.body()?.value.equals("1")){
                                service_lay.visibility = View.VISIBLE
                                servicesAdapter.updateAll(response.body()?.data!!)
                                tot = 0
                                for (i in 0..response.body()?.data?.size!!-1){
                                    tot = tot + ((response.body()?.data?.get(i)?.count!!.toInt())*(response.body()?.data?.get(i)?.price!!.toInt()))
                                }
                                Log.e("tottal",tot.toString())
                                total.text = tot.toString()+getString(R.string.rs)
                            }else{
                                CommonUtil.makeToast(mContext,response.body()?.msg!!)
                                service_lay.visibility =  View.GONE
                            }
                        }
                    }

                }
        )

    }

    override fun onItemClick(view: View, position: Int) {
        if (view.id == R.id.name){
            subCatsAdapter.selected = position
            subCatModels.get(position).selected = true
            subCatsAdapter.notifyDataSetChanged()

                getLocationWithPermission(subCatModels.get(position).id!!)


        }else if (view.id == R.id.fav){
            if (user.loginStatus!!){
                showProgressDialog(getString(R.string.please_wait))
                Client.getClient()?.create(Service::class.java)?.Favourite(lang.appLanguage,"Bearer"+user.userData.token
                ,productModels.get(position).id!!)?.enqueue(object :Callback<FavResponse>{
                    override fun onFailure(call: Call<FavResponse>, t: Throwable) {
                        hideProgressDialog()
                        CommonUtil.handleException(mContext,t)
                        t.printStackTrace()
                    }

                    override fun onResponse(call: Call<FavResponse>, response: Response<FavResponse>) {
                        hideProgressDialog()
                        if (response.isSuccessful){
                            if (response.body()?.value.equals("1")){
                                getLocationWithPermission(null)
                                CommonUtil.makeToast(mContext,response.body()?.msg!!)
                            }else{
                                CommonUtil.makeToast(mContext,response.body()?.msg!!)
                            }
                        }
                    }

                })
            }else{
                CommonUtil.makeToast(mContext!!,getString(R.string.sorry_visitor))
            }

        }else if (view.id == R.id.lay){
            product = productModels.get(position).id!!
            showService(productModels.get(position).id!!)
        }else if (view.id == R.id.plus){
            tot = 0
            servicesModels.get(position).count  = servicesModels.get(position).count!!+1
            servicesAdapter.notifyDataSetChanged()
            for (i in 0..servicesModels?.size!!-1){
                tot = tot + ((servicesModels?.get(i)?.count!!.toInt())*(servicesModels?.get(i)?.price!!.toInt()))
            }
            total.text = tot.toString()+getString(R.string.rs)
        }else if (view.id == R.id.minus){
            tot = 0
            if (servicesModels.get(position).count!!<=0){

            }else{
                servicesModels.get(position).count  = servicesModels.get(position).count!!-1
                servicesAdapter.notifyDataSetChanged()
            }
            for (i in 0..servicesModels?.size!!-1){
                tot = tot + ((servicesModels?.get(i)?.count!!.toInt())*(servicesModels?.get(i)?.price!!.toInt()))
            }
            total.text = tot.toString()+getString(R.string.rs)
        }
    }

    override fun onTrackerSuccess(lat: Double?, log: Double?) {
        if (startTracker) {
            if (lat != 0.0 && log != 0.0) {
                hideProgressDialog()
                // putMapMarker(lat, log)
            }
        }
    }

    override fun onStartTracker() {
        startTracker = true
    }
    fun getLocationWithPermission(cat:Int?) {
        gps = GPSTracker(mContext!!, this)
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!(PermissionUtils.hasPermissions(mContext, Manifest.permission.ACCESS_FINE_LOCATION)&&
                            (PermissionUtils.hasPermissions(mContext,
                                    Manifest.permission.ACCESS_COARSE_LOCATION)))) {
                CommonUtil.PrintLogE("Permission not granted")
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(
                            PermissionUtils.GPS_PERMISSION,
                            800
                    )
                    Log.e("GPS", "1")
                }
            } else {
                getCurrentLocation(cat)
                Log.e("GPS", "2")
            }
        } else {
            Log.e("GPS", "3")
            getCurrentLocation(cat)
        }

    }

    internal fun getCurrentLocation(cat:Int?) {
        gps.getLocation()
        if (!gps.canGetLocation()) {
            mAlertDialog = DialogUtil.showAlertDialog(mContext!!,
                    getString(R.string.gps_detecting),
                    DialogInterface.OnClickListener { dialogInterface, i ->
                        mAlertDialog?.dismiss()
                        val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                        startActivityForResult(intent, 300)
                    })
        } else {
            if (gps.getLatitude() !== 0.0 && gps.getLongitude() !== 0.0) {
                // putMapMarker(gps.getLatitude(), gps.getLongitude())
                if (user.loginStatus!!) {
                    getData("Bearer"+user.userData.token,gps.getLatitude().toString(), gps.getLongitude().toString(),cat)
                }else{
                    getData(null,gps.getLatitude().toString(), gps.getLongitude().toString(),cat)
                }
                mLat = gps.getLatitude().toString()
                mLang = gps.getLongitude().toString()
                val addresses: List<Address>
                geocoder = Geocoder(mContext!!, Locale.getDefault())
                try {
                    addresses = geocoder.getFromLocation(
                            java.lang.Double.parseDouble(mLat),
                            java.lang.Double.parseDouble(mLang),
                            1
                    )
                    if (addresses.isEmpty()) {
                        Toast.makeText(
                                mContext,
                                resources.getString(R.string.detect_location),
                                Toast.LENGTH_SHORT
                        ).show()
                    } else {
                        result = addresses[0].getAddressLine(0)
                        Log.e("address",result)
                        // CommonUtil.makeToast(mContext,addresses[0].getAddressLine(0))



                    }

                } catch (e: IOException) {
                }


                //putMapMarker(gps.getLatitude(), gps.getLongitude())


            }
        }
    }
    fun getData(token:String?,lat:String,lng:String,id:Int?){
        layNoInternet!!.visibility = View.GONE
        layNoItem!!.visibility = View.GONE
        Client.getClient()?.create(Service::class.java)?.Products(lang.appLanguage,token,cat!!,lat,lng,id,ID,favo,null)?.enqueue(object:
                Callback<CategoryResponse> {
            override fun onFailure(call: Call<CategoryResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                layNoInternet!!.visibility = View.VISIBLE
                layNoItem!!.visibility = View.GONE
                swipeRefresh!!.isRefreshing = false
            }

            override fun onResponse(
                    call: Call<CategoryResponse>,
                    response: Response<CategoryResponse>
            ) {
                hideProgressDialog()
                swipeRefresh!!.isRefreshing = false
                if(response.isSuccessful){
                    subCatsAdapter.updateAll(response.body()?.subcategories!!)
                    if (response.body()?.value.equals("1")){
                        if (response.body()!!.data?.isEmpty()!!) {
                            layNoItem!!.visibility = View.VISIBLE
                            layNoInternet!!.visibility = View.GONE
                            tvNoContent!!.setText(R.string.content_not_found_you_can_still_search_the_app_freely)

                        } else {
//
                            productAdapter.updateAll(response.body()!!.data!!)
                        }
                        if (response.body()?.pieces==0){
                            price_lay.visibility = View.GONE
                        }else{
                            price_lay.visibility = View.VISIBLE
                        }
                        price.text = response.body()?.total.toString()+getString(R.string.rs)
                        piece.text = getString(R.string.process_order)+"("+response.body()?.pieces.toString()+getString(R.string.piece)+")"
                        order_id = response.body()?.order_id!!
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
}