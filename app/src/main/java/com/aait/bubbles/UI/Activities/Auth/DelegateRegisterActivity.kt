package com.aait.bubbles.UI.Activities.Auth

import android.content.Intent
import android.os.Build
import android.util.Log
import android.widget.*
import com.aait.bubbles.Base.ParentActivity
import com.aait.bubbles.Models.UserResponse
import com.aait.bubbles.Network.Client
import com.aait.bubbles.Network.Service
import com.aait.bubbles.R
import com.aait.bubbles.UI.Activities.LocationActivity
import com.aait.bubbles.Utils.CommonUtil
import com.aait.bubbles.Utils.PermissionUtils
import com.bumptech.glide.Glide
import com.fxn.pix.Options
import com.fxn.pix.Pix
import com.fxn.utility.ImageQuality
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.messaging.FirebaseMessaging

import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File

class DelegateRegisterActivity :ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_register_delegate
    var lat = ""
    var lng = ""
    var result = ""
    lateinit var back:ImageView
    lateinit var register: Button
    lateinit var phone: EditText
    lateinit var name: EditText
    lateinit var email: EditText
    lateinit var locatrion: TextView
    lateinit var password: EditText
    lateinit var view: ImageView
    lateinit var confirm_password: EditText
    lateinit var ID:ImageView
    lateinit var license:ImageView

    lateinit var login: LinearLayout

    internal var returnValue: java.util.ArrayList<String>? = java.util.ArrayList()
    internal var options = Options.init()
            .setRequestCode(100)                                                 //Request code for activity results
            .setCount(1)                                                         //Number of images to restict selection count
            .setFrontfacing(false)                                                //Front Facing camera on start
            .setImageQuality(ImageQuality.HIGH)                                  //Image Quality
            .setPreSelectedUrls(returnValue)                                     //Pre selected Image Urls
            .setScreenOrientation(Options.SCREEN_ORIENTATION_PORTRAIT)           //Orientaion
            .setPath("/pix/images")
    private var ImageBasePath: String? = null
    internal var returnValue1: java.util.ArrayList<String>? = java.util.ArrayList()
    internal var options1 = Options.init()
            .setRequestCode(200)                                                 //Request code for activity results
            .setCount(1)                                                         //Number of images to restict selection count
            .setFrontfacing(false)                                                //Front Facing camera on start
            .setImageQuality(ImageQuality.HIGH)                                  //Image Quality
            .setPreSelectedUrls(returnValue1)                                     //Pre selected Image Urls
            .setScreenOrientation(Options.SCREEN_ORIENTATION_PORTRAIT)           //Orientaion
            .setPath("/pix/images")
    private var ImageBasePath1: String? = null

    var deviceID = ""
    override fun initializeComponents() {
        FirebaseMessaging.getInstance().token.addOnCompleteListener(OnCompleteListener { task ->
            if (!task.isSuccessful) {
                Log.w("TAG", "Fetching FCM registration token failed", task.exception)
                return@OnCompleteListener
            }

            // Get new FCM registration token
            deviceID = task.result
            Log.e("device", deviceID)
            // Log and toast

        })
        register = findViewById(R.id.register)
        phone = findViewById(R.id.phone)
        name = findViewById(R.id.name)
        email = findViewById(R.id.email)
        locatrion = findViewById(R.id.location)
        password = findViewById(R.id.password)
        confirm_password = findViewById(R.id.confirm_password)
        ID = findViewById(R.id.ID)
        license = findViewById(R.id.license)
        login = findViewById(R.id.login)
        back = findViewById(R.id.back)
        back.setOnClickListener { onBackPressed()
            finish()}
        login.setOnClickListener {val intent = Intent(this,LoginActivity::class.java)
            intent.putExtra("type","provider")
            startActivity(intent)
            finish()}


        locatrion.setOnClickListener {
            startActivityForResult(Intent(this, LocationActivity::class.java),1)
        }
        ID.setOnClickListener {
            if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
                if (!(PermissionUtils.hasPermissions(mContext,
                                android.Manifest.permission.CAMERA)&& PermissionUtils.hasPermissions(mContext,
                                android.Manifest.permission.READ_EXTERNAL_STORAGE)&& PermissionUtils.hasPermissions(mContext,
                                android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                )) {
                    CommonUtil.PrintLogE("Permission not granted")
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(
                                PermissionUtils.IMAGE_PERMISSIONS,
                                400
                        )
                    }
                } else {
                    Pix.start(this, options)
                    CommonUtil.PrintLogE("Permission is granted before")
                }
            } else {
                CommonUtil.PrintLogE("SDK minimum than 23")
                Pix.start(this, options)
            }
        }

        license.setOnClickListener {
            if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
                if (!(PermissionUtils.hasPermissions(mContext,
                                android.Manifest.permission.CAMERA)&& PermissionUtils.hasPermissions(mContext,
                                android.Manifest.permission.READ_EXTERNAL_STORAGE)&& PermissionUtils.hasPermissions(mContext,
                                android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                )) {
                    CommonUtil.PrintLogE("Permission not granted")
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(
                                PermissionUtils.IMAGE_PERMISSIONS,
                                400
                        )
                    }
                } else {
                    Pix.start(this, options1)
                    CommonUtil.PrintLogE("Permission is granted before")
                }
            } else {
                CommonUtil.PrintLogE("SDK minimum than 23")
                Pix.start(this, options1)
            }
        }
        register.setOnClickListener {  if(CommonUtil.checkEditError(name,getString(R.string.enter_name))||
                CommonUtil.checkEditError(phone,getString(R.string.enter_phone))||
                CommonUtil.checkLength(phone,getString(R.string.phone_length),9)||
                CommonUtil.checkEditError(email,getString(R.string.email))||
                ! CommonUtil.isEmailValid(email,getString(R.string.correct_email))||
                CommonUtil.checkTextError(locatrion,getString(R.string.enter_location))||
                CommonUtil.checkEditError(password,getString(R.string.password))||
                CommonUtil.checkLength(password,getString(R.string.password_length),6)||
                CommonUtil.checkEditError(confirm_password,getString(R.string.confirm_password))){
            return@setOnClickListener
        } else{
            if (!password.text.toString().equals(confirm_password.text.toString())) {
                CommonUtil.makeToast(mContext, getString(R.string.password_not_match))
            } else {
                if (ImageBasePath==null){
                    CommonUtil.makeToast(mContext,getString(R.string.ID_photo))
                }else{
                    if (ImageBasePath1==null){
                        CommonUtil.makeToast(mContext,getString(R.string.A_copy_of_the_driver_license))
                    }else{
                        Register(ImageBasePath!!,ImageBasePath1!!)
                    }
                }



            }
        }
        }

    }

    fun Register(path:String,path1:String){
        showProgressDialog(getString(R.string.please_wait))
        var filePart: MultipartBody.Part? = null
        val ImageFile = File(path)
        val fileBody = RequestBody.create(MediaType.parse("*/*"), ImageFile)
        filePart = MultipartBody.Part.createFormData("id_image", ImageFile.name, fileBody)
        var filePart1: MultipartBody.Part? = null
        val ImageFile1 = File(path1)
        val fileBody1 = RequestBody.create(MediaType.parse("*/*"), ImageFile1)
        filePart1 = MultipartBody.Part.createFormData("driving_license", ImageFile1.name, fileBody1)
        Client.getClient()?.create(Service::class.java)?.SignUpDel(name.text.toString(),phone.text.toString(),email.text.toString(),locatrion.text.toString()
                ,lat,lng,password.text.toString(),deviceID,"android",lang.appLanguage,filePart,filePart1)?.enqueue(object:
                Callback<UserResponse> {
            override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(call: Call<UserResponse>, response: Response<UserResponse>) {
                hideProgressDialog()
                if(response.isSuccessful){
                    if(response.body()?.value.equals("1")){
                        val intent = Intent(this@DelegateRegisterActivity,ActivateAccountActivity::class.java)
                        intent.putExtra("user",response.body()?.data)
                        startActivity(intent)
                        finish()
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == 1) {
            if (data?.getStringExtra("result") != null) {
                result = data?.getStringExtra("result").toString()
                lat = data?.getStringExtra("lat").toString()
                lng = data?.getStringExtra("lng").toString()
                locatrion.text = result
            } else {
                result = ""
                lat = ""
                lng = ""
                locatrion.text = ""
            }
        }else  if (requestCode == 100) {
            if (resultCode==0){

            }else {
                returnValue = data!!.getStringArrayListExtra(Pix.IMAGE_RESULTS)

                ImageBasePath = returnValue!![0]

                Glide.with(mContext).load(ImageBasePath).into(ID)

                if (ImageBasePath != null) {
                    // upLoad(ImageBasePath!!)
                    //ID.text = getString(R.string.Image_attached)
                }
            }
        }else if (requestCode == 200) {
            if (resultCode==0){

            }else {
                returnValue1 = data!!.getStringArrayListExtra(Pix.IMAGE_RESULTS)

                ImageBasePath1 = returnValue1!![0]

                Glide.with(mContext).load(ImageBasePath1).into(license)

                if (ImageBasePath1 != null) {
                    // upLoad(ImageBasePath!!)
                    //ID.text = getString(R.string.Image_attached)
                }
            }
        }
    }

}