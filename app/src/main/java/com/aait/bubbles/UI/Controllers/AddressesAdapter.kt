package com.aait.bubbles.UI.Controllers

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.TextView
import com.aait.bubbles.Base.ParentRecyclerAdapter
import com.aait.bubbles.Base.ParentRecyclerViewHolder
import com.aait.bubbles.Models.HomeModel
import com.aait.bubbles.Models.MyAddressModel
import com.aait.bubbles.R
import com.bumptech.glide.Glide

class AddressesAdapter (context: Context, data: MutableList<MyAddressModel>, layoutId: Int) :
        ParentRecyclerAdapter<MyAddressModel>(context, data, layoutId) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParentRecyclerViewHolder {
        val itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false)
        return ViewHolder(itemView)
    }
    var TextView.textColor: Int
        get() = currentTextColor
        set(v) = setTextColor(v)



    override fun onBindViewHolder(holder: ParentRecyclerViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder
        val questionModel = data.get(position)
        viewHolder.address!!.setText(questionModel.address)
        if (questionModel.type.equals("home")) {
            viewHolder.image.setImageResource(R.mipmap.apartment)
            viewHolder.type.text = mcontext.getString(R.string.home)
        }else if (questionModel.type.equals("work")){
            viewHolder.image.setImageResource(R.mipmap.travel)
            viewHolder.type.text = mcontext.getString(R.string.work)
        }else if (questionModel.type.equals("rest")){
            viewHolder.image.setImageResource(R.mipmap.hommme)
            viewHolder.type.text = mcontext.getString(R.string.rest)
        }
        // viewHolder.itemView.animation = mcontext.resources.
        // val animation = mcontext.resources.getAnimation(R.anim.item_animation_from_right)
        val animation = AnimationUtils.loadAnimation(mcontext, R.anim.item_animation_from_bottom_slow)
        animation.setDuration(250)
        viewHolder.itemView.startAnimation(animation)

        viewHolder.delete.setOnClickListener(View.OnClickListener { view -> onItemClickListener.onItemClick(view,position)

        })
        viewHolder.itemView.setOnClickListener(View.OnClickListener { view -> onItemClickListener.onItemClick(view,position)

        })




    }
    inner class ViewHolder internal constructor(itemView: View) :
            ParentRecyclerViewHolder(itemView) {





        internal var type=itemView.findViewById<TextView>(R.id.type)
        internal var image = itemView.findViewById<ImageView>(R.id.image)
        internal var address = itemView.findViewById<TextView>(R.id.address)
        internal var delete = itemView.findViewById<TextView>(R.id.delete)




    }
}