package com.aait.bubbles.UI.Activities.Auth

import android.content.Intent
import android.text.InputType
import android.util.Log
import android.widget.*
import com.aait.bubbles.Base.ParentActivity
import com.aait.bubbles.Models.UserResponse
import com.aait.bubbles.Network.Client
import com.aait.bubbles.Network.Service
import com.aait.bubbles.R

import com.aait.bubbles.UI.Activities.LocationActivity
import com.aait.bubbles.Utils.CommonUtil
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.messaging.FirebaseMessaging
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class RegisterActivity:ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_register
    var lat = ""
    var lng = ""
    var result = ""
    lateinit var back:ImageView
    lateinit var register: Button
    lateinit var phone: EditText
    lateinit var name: EditText
    lateinit var email: EditText
    lateinit var locatrion: TextView
    lateinit var password: EditText
    lateinit var view: ImageView
    lateinit var confirm_password: EditText
    lateinit var view1: ImageView

    lateinit var login: LinearLayout
    var deviceID = ""
    override fun initializeComponents() {
        FirebaseMessaging.getInstance().token.addOnCompleteListener(OnCompleteListener { task ->
            if (!task.isSuccessful) {
                Log.w("TAG", "Fetching FCM registration token failed", task.exception)
                return@OnCompleteListener
            }

            // Get new FCM registration token
            deviceID = task.result
            Log.e("device", deviceID)
            // Log and toast

        })
        register = findViewById(R.id.register)
        phone = findViewById(R.id.phone)
        name = findViewById(R.id.name)
        email = findViewById(R.id.email)
        locatrion = findViewById(R.id.location)
        password = findViewById(R.id.password)
        back = findViewById(R.id.back)
        back.setOnClickListener { onBackPressed()
        finish()}

        confirm_password = findViewById(R.id.confirm_password)

        login = findViewById(R.id.login)
        login.setOnClickListener {val intent = Intent(this,LoginActivity::class.java)
            intent.putExtra("type","user")
            startActivity(intent)
            finish()}


        locatrion.setOnClickListener {
            startActivityForResult(Intent(this, LocationActivity::class.java),1)
        }
        register.setOnClickListener {  if(CommonUtil.checkEditError(name,getString(R.string.enter_name))||
                CommonUtil.checkEditError(phone,getString(R.string.enter_phone))||
                CommonUtil.checkLength(phone,getString(R.string.phone_length),9)||
                CommonUtil.checkEditError(email,getString(R.string.email))||
               ! CommonUtil.isEmailValid(email,getString(R.string.correct_email))||
                CommonUtil.checkTextError(locatrion,getString(R.string.enter_location))||
                CommonUtil.checkEditError(password,getString(R.string.password))||
                CommonUtil.checkLength(password,getString(R.string.password_length),6)||
                CommonUtil.checkEditError(confirm_password,getString(R.string.confirm_password))){
            return@setOnClickListener
        } else{
            if (!password.text.toString().equals(confirm_password.text.toString())) {
                CommonUtil.makeToast(mContext, getString(R.string.password_not_match))
            } else {


                    Register()


            }
        }
        }

    }

    fun Register(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.SignUp(name.text.toString(),phone.text.toString(),email.text.toString(),locatrion.text.toString()
                ,lat,lng,password.text.toString(),deviceID,"android",lang.appLanguage)?.enqueue(object:
                Callback<UserResponse> {
            override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(call: Call<UserResponse>, response: Response<UserResponse>) {
                hideProgressDialog()
                if(response.isSuccessful){
                    if(response.body()?.value.equals("1")){
                        val intent = Intent(this@RegisterActivity,ActivateAccountActivity::class.java)
                        intent.putExtra("user",response.body()?.data)
                        startActivity(intent)
                        finish()
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == 1) {
            if (data?.getStringExtra("result") != null) {
                result = data?.getStringExtra("result").toString()
                lat = data?.getStringExtra("lat").toString()
                lng = data?.getStringExtra("lng").toString()
                locatrion.text = result
            } else {
                result = ""
                lat = ""
                lng = ""
                locatrion.text = ""
            }
        }
    }

}