package com.aait.bubbles.UI.Fragments

import android.content.Intent
import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.aait.bubbles.Base.BaseFragment
import com.aait.bubbles.Listeners.OnItemClickListener
import com.aait.bubbles.Models.OrderModel
import com.aait.bubbles.Models.OrdersResponse
import com.aait.bubbles.Network.Client
import com.aait.bubbles.Network.Service
import com.aait.bubbles.R
import com.aait.bubbles.UI.Activities.Main.Client.PaymentActivity
import com.aait.bubbles.UI.Controllers.OrderAdapter
import com.aait.bubbles.Utils.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class PendingFragment:BaseFragment() , OnItemClickListener {
    override val layoutResource: Int
        get() = R.layout.app_recycle

    lateinit var rv_recycle: RecyclerView
    internal var layNoInternet: RelativeLayout? = null

    internal var layNoItem: RelativeLayout? = null

    internal var tvNoContent: TextView? = null
    lateinit var iv_no_item:ImageView
    var swipeRefresh: SwipeRefreshLayout? = null
    lateinit var linearLayoutManager: LinearLayoutManager
    lateinit var orderAdapter: OrderAdapter
    var orderModels = ArrayList<OrderModel>()
    lateinit var text:TextView
    lateinit var icon:ImageView
    lateinit var text1:TextView
    override fun initializeComponents(view: View) {
        rv_recycle = view.findViewById(R.id.rv_recycle)
        layNoInternet = view.findViewById(R.id.lay_no_internet)
        layNoItem = view.findViewById(R.id.lay_no_item)
        tvNoContent = view.findViewById(R.id.tv_no_content)
        swipeRefresh = view.findViewById(R.id.swipe_refresh)
        iv_no_item = view.findViewById(R.id.iv_no_item)
        text = view.findViewById(R.id.Text)
        icon = view.findViewById(R.id.icon)
        text1 = view.findViewById(R.id.text1)
        linearLayoutManager = LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL,false)
        orderAdapter = OrderAdapter(mContext!!,orderModels, R.layout.recycle_orders)
        orderAdapter.setOnItemClickListener(this)
        rv_recycle.layoutManager = linearLayoutManager
        rv_recycle.adapter = orderAdapter
        swipeRefresh!!.setColorSchemeResources(
                R.color.colorPrimary,
                R.color.colorPrimaryDark,
                R.color.colorAccent
        )
        swipeRefresh!!.setOnRefreshListener {
            getData()

        }

        getData()
    }
    fun getData(){
        layNoInternet!!.visibility = View.GONE
        layNoItem!!.visibility = View.GONE
        Client.getClient()?.create(Service::class.java)?.Orders(lang.appLanguage,"Bearer"+user.userData.token,"pending_payment")?.enqueue(object:
                Callback<OrdersResponse> {
            override fun onFailure(call: Call<OrdersResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext!!,t)
                t.printStackTrace()
                layNoInternet!!.visibility = View.VISIBLE
                layNoItem!!.visibility = View.GONE
                swipeRefresh!!.isRefreshing = false
            }

            override fun onResponse(
                    call: Call<OrdersResponse>,
                    response: Response<OrdersResponse>
            ) {
                hideProgressDialog()
                swipeRefresh!!.isRefreshing = false
                if(response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        if (response.body()!!.data?.isEmpty()!!) {
                            layNoItem!!.visibility = View.VISIBLE
                            layNoInternet!!.visibility = View.GONE
                            tvNoContent!!.setText(R.string.Nothing_has_been_added_to_orders)
                            iv_no_item.setImageResource(R.mipmap.cart_)
                            text.text = mContext!!.getString(R.string.Click_on_the_sign)
                            icon.setImageResource(R.mipmap.orderactive)
                            text1.text = mContext!!.getString(R.string.to_add)
                        } else {
//
                            orderAdapter.updateAll(response.body()!!.data!!)
                        }
                    }else{
                        CommonUtil.makeToast(mContext!!,response.body()?.msg!!)
                    }
                }
            }

        })
    }
    override fun onItemClick(view: View, position: Int) {
        val intent = Intent(activity, PaymentActivity::class.java)
        intent.putExtra("id",orderModels.get(position).id)
        startActivity(intent)
    }
}