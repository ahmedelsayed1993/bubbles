package com.aait.bubbles.UI.Activities.Main.Client

import android.content.Intent
import android.graphics.Color
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.aait.bubbles.Base.ParentActivity
import com.aait.bubbles.R
import com.aait.bubbles.UI.Fragments.FavFragment
import com.aait.bubbles.UI.Fragments.HomeFragment
import com.aait.bubbles.UI.Fragments.OrdersFragment
import com.aait.bubbles.UI.Fragments.ProfileFragment
import com.aait.bubbles.Utils.CommonUtil

class MainActivity:ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_main
    lateinit var home: LinearLayout
    lateinit var home_image: ImageView
    lateinit var home_text: TextView
    lateinit var fav: LinearLayout
    lateinit var fav_image: ImageView
    lateinit var fav_text: TextView
    lateinit var orders: LinearLayout
    lateinit var orders_image: ImageView
    lateinit var orders_text: TextView
    lateinit var my_account: LinearLayout
    lateinit var account_image: ImageView
    lateinit var account_text: TextView
    lateinit var notification:ImageView

    var TextView.textColor: Int
        get() = currentTextColor
        set(v) = setTextColor(v)

    private var fragmentManager: FragmentManager? = null
    var selected = 1
    private var transaction: FragmentTransaction? = null
    internal lateinit var homeFragment: HomeFragment
    internal lateinit var favFragment: FavFragment
    internal lateinit var ordersFragment: OrdersFragment
    internal lateinit var accountFragment: ProfileFragment

    override fun initializeComponents() {
        homeFragment = HomeFragment.newInstance()
        favFragment = FavFragment.newInstance()
        ordersFragment = OrdersFragment.newInstance()
        accountFragment = ProfileFragment.newInstance()
        fragmentManager = getSupportFragmentManager()
        transaction = fragmentManager!!.beginTransaction()
        transaction!!.add(R.id.home_fragment_container, homeFragment)
        transaction!!.add(R.id.home_fragment_container, favFragment)
        transaction!!.add(R.id.home_fragment_container,ordersFragment)
        transaction!!.add(R.id.home_fragment_container,accountFragment)
        transaction!!.commit()
        home = findViewById(R.id.home)
        home_image = findViewById(R.id.home_image)
        home_text = findViewById(R.id.home_text)
        fav = findViewById(R.id.fav)
        fav_image = findViewById(R.id.fav_image)
        fav_text = findViewById(R.id.fav_text)
        orders = findViewById(R.id.orders)
        orders_image = findViewById(R.id.orders_image)
        orders_text = findViewById(R.id.orders_text)
        my_account = findViewById(R.id.my_account)
        account_image = findViewById(R.id.account_image)
        account_text = findViewById(R.id.account_text)
        notification = findViewById(R.id.notification)
        showhome()
        home.setOnClickListener { showhome() }
        fav.setOnClickListener { if (user.loginStatus!!){showfav()}
        else{
            CommonUtil.makeToast(mContext!!,getString(R.string.sorry_visitor))
        }}
        orders.setOnClickListener { if (user.loginStatus!!){showorder()}
        else{
            CommonUtil.makeToast(mContext!!,getString(R.string.sorry_visitor))
        }}
        my_account.setOnClickListener { showprofile() }
        notification.setOnClickListener { if (user.loginStatus!!){
        startActivity(Intent(this,NotificationActivity::class.java))}
        else{
            CommonUtil.makeToast(mContext!!,getString(R.string.sorry_visitor))
        }
        }
    }
    fun showhome(){
        selected = 1
        home_image.setImageResource(R.mipmap.homeactive)
        home_text.textColor = Color.parseColor("#1EBDD3")
        fav_text.textColor = Color.parseColor("#666767")
        orders_text.textColor = Color.parseColor("#666767")
        account_text.textColor = Color.parseColor("#666767")
        fav_image.setImageResource(R.mipmap.fav)
        orders_image.setImageResource(R.mipmap.order)
        account_image.setImageResource(R.mipmap.profile)

        transaction = fragmentManager!!.beginTransaction()
        //        transaction.hide(mMoreFragment);
        //        transaction.hide(mOrdersFragment);
        //        transaction.hide(mFavouriteFragment);
        transaction!!.replace(R.id.home_fragment_container, homeFragment)
        transaction!!.commit()
    }
    fun showfav(){
        if (user.loginStatus!!) {
            selected = 1
            home_image.setImageResource(R.mipmap.home)
            fav_text.textColor = Color.parseColor("#1EBDD3")
            home_text.textColor = Color.parseColor("#666767")
            orders_text.textColor = Color.parseColor("#666767")
            account_text.textColor = Color.parseColor("#666767")
            fav_image.setImageResource(R.mipmap.favactive)
            orders_image.setImageResource(R.mipmap.order)
            account_image.setImageResource(R.mipmap.profile)

            transaction = fragmentManager!!.beginTransaction()
            //        transaction.hide(mMoreFragment);
            //        transaction.hide(mOrdersFragment);
            //        transaction.hide(mFavouriteFragment);
            transaction!!.replace(R.id.home_fragment_container, favFragment)
            transaction!!.commit()
        }else{
            CommonUtil.makeToast(mContext!!,getString(R.string.sorry_visitor))
        }
    }
    fun showorder(){
        if (user.loginStatus!!) {
            selected = 1
            home_image.setImageResource(R.mipmap.home)
            orders_text.textColor = Color.parseColor("#1EBDD3")
            fav_text.textColor = Color.parseColor("#666767")
            home_text.textColor = Color.parseColor("#666767")
            account_text.textColor = Color.parseColor("#666767")
            fav_image.setImageResource(R.mipmap.fav)
            orders_image.setImageResource(R.mipmap.orderactive)
            account_image.setImageResource(R.mipmap.profile)

            transaction = fragmentManager!!.beginTransaction()
            //        transaction.hide(mMoreFragment);
            //        transaction.hide(mOrdersFragment);
            //        transaction.hide(mFavouriteFragment);
            transaction!!.replace(R.id.home_fragment_container, ordersFragment)
            transaction!!.commit()
        }else{
            CommonUtil.makeToast(mContext!!,getString(R.string.sorry_visitor))
        }
    }
    fun showprofile(){
        selected = 1
        home_image.setImageResource(R.mipmap.home)
        account_text.textColor = Color.parseColor("#1EBDD3")
        fav_text.textColor = Color.parseColor("#666767")
        orders_text.textColor = Color.parseColor("#666767")
        home_text.textColor = Color.parseColor("#666767")
        fav_image.setImageResource(R.mipmap.fav)
        orders_image.setImageResource(R.mipmap.order)
        account_image.setImageResource(R.mipmap.profileactive)

        transaction = fragmentManager!!.beginTransaction()
        //        transaction.hide(mMoreFragment);
        //        transaction.hide(mOrdersFragment);
        //        transaction.hide(mFavouriteFragment);
        transaction!!.replace(R.id.home_fragment_container, accountFragment)
        transaction!!.commit()
    }
}