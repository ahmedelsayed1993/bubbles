package com.aait.bubbles.UI.Fragments

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aait.bubbles.Base.BaseFragment
import com.aait.bubbles.Listeners.OnItemClickListener
import com.aait.bubbles.Models.HomeModel
import com.aait.bubbles.Models.HomeResponse
import com.aait.bubbles.Network.Client
import com.aait.bubbles.Network.Service
import com.aait.bubbles.R
import com.aait.bubbles.UI.Activities.Main.Client.StoreActivity
import com.aait.bubbles.UI.Activities.Main.Client.SubCategoryActivity
import com.aait.bubbles.UI.Controllers.CatsAdapter
import com.aait.bubbles.UI.Controllers.SlidersAdapter
import com.aait.bubbles.Utils.CommonUtil
import com.github.islamkhsh.CardSliderViewPager
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class HomeFragment:BaseFragment(),OnItemClickListener {
    override val layoutResource: Int
        get() = R.layout.fragment_home
    companion object {
        fun newInstance(): HomeFragment {
            val args = Bundle()
            val fragment = HomeFragment()
            fragment.setArguments(args)
            return fragment
        }
    }
    lateinit var cats: RecyclerView
    lateinit var viewpager: CardSliderViewPager
    lateinit var linearLayoutManager: LinearLayoutManager
    lateinit var catsAdapter: CatsAdapter
    var catsModels = ArrayList<HomeModel>()
    override fun initializeComponents(view: View) {
        cats = view.findViewById(R.id.cats)
        viewpager = view.findViewById(R.id.viewPager)
        linearLayoutManager = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        catsAdapter = CatsAdapter(mContext!!,catsModels,R.layout.recycle_home)
        catsAdapter.setOnItemClickListener(this)
        cats.layoutManager = linearLayoutManager
        cats.adapter = catsAdapter
        getData()
    }
    fun getData(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Main(lang.appLanguage)?.enqueue(object:
                Callback<HomeResponse> {
            override fun onFailure(call: Call<HomeResponse>, t: Throwable) {
                CommonUtil.handleException(mContext!!,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(call: Call<HomeResponse>, response: Response<HomeResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if(response.body()?.value.equals("1")){
                        catsAdapter.updateAll(response.body()?.categories!!)
                        initSliderAds(response.body()?.banners!!)
                    }else{
                        CommonUtil.makeToast(mContext!!,response.body()?.msg!!)
                    }
                }
            }

        })
    }
    fun initSliderAds(list:ArrayList<String>){
        if(list.isEmpty()){
            viewpager.visibility= View.GONE

        }
        else{
            viewpager.visibility= View.VISIBLE

            viewpager.adapter= SlidersAdapter(mContext!!,list)

        }
    }

    override fun onItemClick(view: View, position: Int) {
        if (catsModels.get(position).type.equals("category")){
            val intent = Intent(activity,SubCategoryActivity::class.java)
            intent.putExtra("category",catsModels.get(position).id)
            intent.putExtra("favo",0)
            startActivity(intent)
        }else{
            val intent = Intent(activity,StoreActivity::class.java)
            intent.putExtra("category",catsModels.get(position).id)
            startActivity(intent)
        }

    }
}