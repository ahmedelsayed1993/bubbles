package com.aait.bubbles.UI.Activities.Main.Client

import android.content.Intent
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aait.bubbles.Base.ParentActivity
import com.aait.bubbles.Models.CartModel
import com.aait.bubbles.Models.OrderDetailsResponse
import com.aait.bubbles.Network.Client
import com.aait.bubbles.Network.Service
import com.aait.bubbles.R
import com.aait.bubbles.UI.Controllers.OrderProductAdapter
import com.aait.bubbles.Utils.CommonUtil
import com.bumptech.glide.Glide
import de.hdodenhof.circleimageview.CircleImageView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class OrderDetailsActivity:ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_order_details
    var id = 0
    lateinit var title:TextView
    lateinit var back:ImageView
    lateinit var one:ImageView
    lateinit var two:ImageView
    lateinit var three:ImageView
    lateinit var four:ImageView
    lateinit var line:TextView
    lateinit var line1:TextView
    lateinit var line2:TextView
    lateinit var text:TextView
    lateinit var text1:TextView
    lateinit var text2:TextView
    lateinit var text3:TextView
    lateinit var orders:RecyclerView
    lateinit var delegate:LinearLayout
    lateinit var image:CircleImageView
    lateinit var name:TextView
    lateinit var phone:TextView
    lateinit var num:TextView
    lateinit var price:TextView
    lateinit var date:TextView
    lateinit var time:TextView
    lateinit var finish:Button
    lateinit var rate:Button
    lateinit var orderProductAdapter: OrderProductAdapter
    lateinit var linearLayoutManager: LinearLayoutManager
    var cartModels = ArrayList<CartModel>()
    override fun initializeComponents() {
        id = intent.getIntExtra("id",0)
        one = findViewById(R.id.one)
        two = findViewById(R.id.two)
        three = findViewById(R.id.three)
        four = findViewById(R.id.four)
        line = findViewById(R.id.line)
        line1 = findViewById(R.id.line1)
        line2 = findViewById(R.id.line2)
        text = findViewById(R.id.text)
        text1 = findViewById(R.id.text1)
        text2 = findViewById(R.id.text2)
        text3 = findViewById(R.id.text3)
        orders = findViewById(R.id.orders)
        delegate = findViewById(R.id.delegate)
        name = findViewById(R.id.name)
        image = findViewById(R.id.image)
        phone = findViewById(R.id.phone)
        num = findViewById(R.id.num)
        price = findViewById(R.id.price)
        date = findViewById(R.id.date)
        time = findViewById(R.id.time)
        finish = findViewById(R.id.finish)
        rate = findViewById(R.id.rate)
        linearLayoutManager = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        orderProductAdapter = OrderProductAdapter(mContext,cartModels,R.layout.recycle_cart_products)
        orders.layoutManager = linearLayoutManager
        orders.adapter = orderProductAdapter
        title = findViewById(R.id.title)
        back = findViewById(R.id.back)
        title.text = getString(R.string.order_details)
        back.setOnClickListener { onBackPressed()
        finish()}
        getData()
        finish.setOnClickListener {
            showProgressDialog(getString(R.string.please_wait))
            Client.getClient()?.create(Service::class.java)?.OrderDetails(lang.appLanguage,"Bearer"+user.userData.token,id,"completed")
                    ?.enqueue(object : Callback<OrderDetailsResponse> {
                        override fun onFailure(call: Call<OrderDetailsResponse>, t: Throwable) {
                            CommonUtil.handleException(mContext,t)
                            t.printStackTrace()
                            hideProgressDialog()
                        }

                        override fun onResponse(call: Call<OrderDetailsResponse>, response: Response<OrderDetailsResponse>) {
                            hideProgressDialog()
                            if (response.isSuccessful){
                                if (response.body()?.value.equals("1")){
                                  val intent = Intent(this@OrderDetailsActivity,AddRateActivity::class.java)
                                    intent.putExtra("id",id)
                                    startActivity(intent)
                                    finish
                                }else{
                                    CommonUtil.makeToast(mContext,response.body()?.msg!!)
                                }
                            }
                        }

                    })
        }


        rate.setOnClickListener {
            val intent = Intent(this@OrderDetailsActivity,AddRateActivity::class.java)
            intent.putExtra("id",id)
            startActivity(intent)
            finish
        }
    }

    fun getData(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.OrderDetails(lang.appLanguage,"Bearer"+user.userData.token,id,null)
                ?.enqueue(object : Callback<OrderDetailsResponse> {
                    override fun onFailure(call: Call<OrderDetailsResponse>, t: Throwable) {
                        CommonUtil.handleException(mContext,t)
                        t.printStackTrace()
                        hideProgressDialog()
                    }

                    override fun onResponse(call: Call<OrderDetailsResponse>, response: Response<OrderDetailsResponse>) {
                        hideProgressDialog()
                        if (response.isSuccessful){
                            if (response.body()?.value.equals("1")){
                                Glide.with(mContext).load(response.body()?.data?.delegate_avatar).into(image)
                                name.text = response.body()?.data?.delegate_name
                                phone.text = response.body()?.data?.delegate_phone
                                num.text = getString(R.string.order_number)+response.body()?.data?.id.toString()
                                price.text = response.body()?.data?.total+getString(R.string.rs)
                                date.text = getString(R.string.order_date)+response.body()?.data?.date
                                time.text = getString(R.string.order_time)+response.body()?.data?.time
                                orderProductAdapter.updateAll(response.body()?.data?.products!!)
                                if (response.body()?.data?.category.equals("store")){

                                    two.visibility = View.GONE
                                    line.visibility = View.GONE
                                    text1.visibility = View.GONE


                                }else{
                                    two.visibility = View.VISIBLE
                                    line.visibility = View.VISIBLE
                                    text1.visibility = View.VISIBLE
                                }
                                Log.e("status", response.body()?.data?.status!!)
                                if (response.body()?.data?.status.equals("accepted")||response.body()?.data?.status.equals("delegate_refused")){
                                    one.setImageResource(R.mipmap.orderstatuestwo)
                                    two.setImageResource(R.mipmap.cartstatues)
                                    three.setImageResource(R.mipmap.cartstatues)
                                    four.setImageResource(R.mipmap.deliverystatues)
                                    delegate.visibility = View.GONE
                                    finish.visibility = View.GONE
                                }else if (response.body()?.data?.status.equals("delegate_accepted")){
                                    one.setImageResource(R.mipmap.orderstatuesthree)
                                    two.setImageResource(R.mipmap.cartstatues)
                                    three.setImageResource(R.mipmap.cartstatues)
                                    four.setImageResource(R.mipmap.deliverystatues)
                                    delegate.visibility = View.VISIBLE
                                    finish.visibility = View.GONE
                                }
                                else if (response.body()?.data?.status.equals("received_order_client")){
                                    one.setImageResource(R.mipmap.orderstatuesthree)
                                    two.setImageResource(R.mipmap.cartstatuesone)
                                    three.setImageResource(R.mipmap.cartstatues)
                                    four.setImageResource(R.mipmap.deliverystatues)
                                    delegate.visibility = View.VISIBLE
                                    finish.visibility = View.GONE
                                }
                                else if (response.body()?.data?.status.equals("delivery_order_provider")){
                                    one.setImageResource(R.mipmap.orderstatuesthree)
                                    two.setImageResource(R.mipmap.cartstatuesthree)
                                    three.setImageResource(R.mipmap.cartstatues)
                                    four.setImageResource(R.mipmap.deliverystatues)
                                    delegate.visibility = View.VISIBLE
                                    finish.visibility = View.GONE
                                }
                                else if (response.body()?.data?.status.equals("received_order_provider")){
                                    one.setImageResource(R.mipmap.orderstatuesthree)
                                    two.setImageResource(R.mipmap.cartstatuesthree)
                                    three.setImageResource(R.mipmap.cartstatuesone)
                                    four.setImageResource(R.mipmap.deliverystatues)
                                    delegate.visibility = View.VISIBLE
                                    finish.visibility = View.GONE
                                }
                                else if (response.body()?.data?.status.equals("delivery_order_client")){
                                    one.setImageResource(R.mipmap.orderstatuesthree)
                                    two.setImageResource(R.mipmap.cartstatuesthree)
                                    three.setImageResource(R.mipmap.cartstatuesthree)
                                    four.setImageResource(R.mipmap.deliverystatues)
                                    delegate.visibility = View.VISIBLE
                                    finish.visibility = View.VISIBLE
                                }else if (response.body()?.data?.status.equals("completed")){
                                    one.setImageResource(R.mipmap.orderstatuesthree)
                                    two.setImageResource(R.mipmap.cartstatuesthree)
                                    three.setImageResource(R.mipmap.cartstatuesthree)
                                    four.setImageResource(R.mipmap.deliverystatuesthree)
                                    delegate.visibility = View.VISIBLE
                                    finish.visibility = View.GONE
                                    if (response.body()?.data?.is_rate==0){
                                        rate.visibility = View.VISIBLE
                                    }else{
                                        rate.visibility = View.GONE
                                    }
                                }else{
                                    one.setImageResource(R.mipmap.orderstatues)
                                    two.setImageResource(R.mipmap.cartstatues)
                                    three.setImageResource(R.mipmap.cartstatues)
                                    four.setImageResource(R.mipmap.deliverystatues)
                                    delegate.visibility = View.GONE
                                    finish.visibility = View.GONE
                                    rate.visibility = View.GONE
                                }
                            }else{
                                CommonUtil.makeToast(mContext,response.body()?.msg!!)
                            }
                        }
                    }

                })
    }
}