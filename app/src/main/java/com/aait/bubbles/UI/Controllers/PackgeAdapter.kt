package com.aait.bubbles.UI.Controllers

import android.content.Context
import android.content.Intent
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import com.aait.bubbles.Models.PackageModel
import com.aait.bubbles.R
import com.aait.bubbles.UI.Activities.Main.Client.ImagesActivity
import com.aait.bubbles.UI.Activities.Main.Client.PackagePaymentActivity
import com.bumptech.glide.Glide
import com.github.islamkhsh.CardSliderAdapter

class PackgeAdapter (context: Context, list : ArrayList<PackageModel>) : CardSliderAdapter<PackageModel>(list) {


    var list = list
    var context=context

    lateinit var title:TextView
    lateinit var price:TextView
    lateinit var desc:TextView
    lateinit var subscribe:Button
    override fun bindView(position: Int, itemContentView: View, item: PackageModel?) {
        title = itemContentView.findViewById(R.id.title)
        price = itemContentView.findViewById(R.id.price)
        desc = itemContentView.findViewById(R.id.desc)
        subscribe = itemContentView.findViewById(R.id.sub)
        title.text = item?.title
        price.text = item?.price
        desc.text = item?.desc
        subscribe.setOnClickListener {
            val intent  = Intent(context, PackagePaymentActivity::class.java)
            intent.putExtra("id",item?.id)
            context.startActivity(intent)
        }


    }


    override fun getItemContentLayout(position: Int) : Int { return R.layout.recycle_packages }

}