package com.aait.bubbles.UI.Activities.Main.Client

import android.content.ClipData
import android.content.Intent
import android.widget.Button
import android.widget.ImageView
import android.widget.RadioButton
import android.widget.TextView
import com.aait.bubbles.Base.ParentActivity
import com.aait.bubbles.Models.TermsResponse
import com.aait.bubbles.Network.Client
import com.aait.bubbles.Network.Service
import com.aait.bubbles.R
import com.aait.bubbles.Utils.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class PaymentActivity:ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_pay
    lateinit var back:ImageView
    lateinit var title:TextView
    lateinit var credit:RadioButton
    lateinit var on_receipt:RadioButton
    lateinit var bank_transfer:RadioButton
    lateinit var confirm:Button
    var pay = "cash"
    var id = 0
    override fun initializeComponents() {
        id = intent.getIntExtra("id",0)
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        credit = findViewById(R.id.credit)
        on_receipt = findViewById(R.id.on_receipt)
        bank_transfer = findViewById(R.id.bank_transfer)
        confirm = findViewById(R.id.confirm)
        credit.setOnClickListener { pay = "online" }
        on_receipt.setOnClickListener { pay = "cash" }
        bank_transfer.setOnClickListener { pay = "bank" }
        title.text = getString(R.string.payment)
        back.setOnClickListener { startActivity(Intent(this,MainActivity::class.java))
        finish()}
        confirm.setOnClickListener { Payment() }

    }
    fun Payment(){
        if (pay.equals("bank")){
            val intent = Intent(this,BankPaymentActivity::class.java)
            intent.putExtra("id",id)
            startActivity(intent)
        }else {
            showProgressDialog(getString(R.string.please_wait))
            Client.getClient()?.create(Service::class.java)?.Payment(lang.appLanguage, "Bearer" + user.userData.token, id, pay)
                    ?.enqueue(object : Callback<TermsResponse> {
                        override fun onFailure(call: Call<TermsResponse>, t: Throwable) {
                            hideProgressDialog()
                            CommonUtil.handleException(mContext, t)
                            t.printStackTrace()
                        }

                        override fun onResponse(call: Call<TermsResponse>, response: Response<TermsResponse>) {
                            hideProgressDialog()
                            if (response.isSuccessful) {
                                if (response.body()?.value.equals("1")) {
                                    CommonUtil.makeToast(mContext, response.body()?.data!!)
                                    startActivity(Intent(this@PaymentActivity, BackToMainActivity::class.java))
                                    finish()
                                } else {
                                    CommonUtil.makeToast(mContext, response.body()?.msg!!)
                                }
                            }
                        }

                    })
        }
    }
}