package com.aait.bubbles.UI.Activities.Main.Client

import android.view.View
import android.widget.ImageView
import com.aait.bubbles.Base.ParentActivity
import com.aait.bubbles.R
import com.aait.bubbles.UI.Controllers.SliderAdpaters
import com.github.islamkhsh.CardSliderViewPager

class ImagesActivity:ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_show_image
    lateinit var viewpager: CardSliderViewPager
    var list = ArrayList<String>()
    lateinit var back:ImageView

    override fun initializeComponents() {
        back = findViewById(R.id.back)
        back.setOnClickListener { onBackPressed()
        finish()}
        viewpager = findViewById(R.id.viewPager)
        list = intent.getStringArrayListExtra("link") as ArrayList<String>
        initSliderAds(list)

    }
    fun initSliderAds(list:ArrayList<String>){
        if(list.isEmpty()){
            viewpager.visibility= View.GONE


        }
        else{
            viewpager.visibility= View.VISIBLE


            viewpager.adapter= SliderAdpaters(mContext!!,list)

        }
    }
}