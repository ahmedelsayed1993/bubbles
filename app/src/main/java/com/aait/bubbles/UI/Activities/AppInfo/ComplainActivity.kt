package com.aait.bubbles.UI.Activities.AppInfo

import android.content.Intent
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import com.aait.bubbles.Base.ParentActivity
import com.aait.bubbles.Models.TermsResponse
import com.aait.bubbles.Network.Client
import com.aait.bubbles.Network.Service
import com.aait.bubbles.R
import com.aait.bubbles.Utils.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ComplainActivity:ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_complain
    lateinit var title:TextView
    lateinit var back:ImageView
    lateinit var phone:EditText
    lateinit var name:EditText
    lateinit var topic:EditText
    lateinit var text:EditText
    lateinit var send:Button

    override fun initializeComponents() {
        title = findViewById(R.id.title)
        back = findViewById(R.id.back)
        name = findViewById(R.id.name)
        phone = findViewById(R.id.phone)
        topic = findViewById(R.id.topic)
        text = findViewById(R.id.text)
        send = findViewById(R.id.send)
        title.text = getString(R.string.suggestions_complaints)
        back.setOnClickListener { onBackPressed()
            finish()}
        send.setOnClickListener { if(CommonUtil.checkEditError(name,getString(R.string.user_name))||
                CommonUtil.checkEditError(phone,getString(R.string.phone_number))||
                CommonUtil.checkLength(phone,getString(R.string.phone_length),9)||
                CommonUtil.checkEditError(topic,getString(R.string.complain_title))||
                CommonUtil.checkEditError(text,getString(R.string.complain_text))){
            return@setOnClickListener
        }else{
            SendData()
        }
        }

    }

    fun SendData(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Complaints(lang.appLanguage,name.text.toString(),phone.text.toString(),topic.text.toString(),text.text.toString()
        )?.enqueue(object: Callback<TermsResponse>{
            override fun onFailure(call: Call<TermsResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
            }

            override fun onResponse(call: Call<TermsResponse>, response: Response<TermsResponse>) {
                hideProgressDialog()
                if(response.isSuccessful){
                    if(response.body()?.value.equals("1")){
                        CommonUtil.makeToast(mContext,response.body()?.data!!)
                        onBackPressed()
                        finish()
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
}