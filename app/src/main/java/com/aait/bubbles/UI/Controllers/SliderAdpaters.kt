package com.aait.bubbles.UI.Controllers

import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.ScaleGestureDetector
import android.view.View
import com.aait.bubbles.R
import com.bumptech.glide.Glide
import com.github.chrisbanes.photoview.PhotoView
import com.github.islamkhsh.CardSliderAdapter



class SliderAdpaters  (context: Context, list : ArrayList<String>) : CardSliderAdapter<String>(list) {


    var list = list
    var context=context

    lateinit var image: PhotoView
    override fun bindView(position: Int, itemContentView: View, item: String?) {
        image = itemContentView.findViewById(R.id.image)
        Log.e("image",item!!)
        Glide.with(context).load(item).into(image)
//        itemContentView.setOnClickListener {
//            val intent  = Intent(context, ImagesActivity::class.java)
//            intent.putExtra("link",list)
//            context.startActivity(intent)
//        }


    }


    override fun getItemContentLayout(position: Int) : Int { return R.layout.images }

}