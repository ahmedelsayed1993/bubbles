package com.aait.bubbles.UI.Activities.Main.Client

import android.content.Intent
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.aait.bubbles.Base.ParentActivity
import com.aait.bubbles.Listeners.OnItemClickListener
import com.aait.bubbles.Models.*
import com.aait.bubbles.Network.Client
import com.aait.bubbles.Network.Service
import com.aait.bubbles.R
import com.aait.bubbles.UI.Activities.Main.Delegate.MainActivity
import com.aait.bubbles.UI.Activities.Main.Delegate.NewOrderDetailsActivity
import com.aait.bubbles.UI.Activities.Main.Delegate.OrderFollowActivity

import com.aait.bubbles.UI.Controllers.NotificationAdapter
import com.aait.bubbles.Utils.CommonUtil
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class NotificationActivity:ParentActivity() ,OnItemClickListener{
    override val layoutResource: Int
        get() = R.layout.activity_notifications
    lateinit var rv_recycle: RecyclerView
    internal var layNoInternet: RelativeLayout? = null
    internal var layNoItem: RelativeLayout? = null
    internal var tvNoContent: TextView? = null
    var swipeRefresh: SwipeRefreshLayout? = null
    internal lateinit var linearLayoutManager: LinearLayoutManager
    internal var notificationModels = java.util.ArrayList<NotificationModel>()
    internal lateinit var notificationAdapter: NotificationAdapter
    lateinit var back: ImageView
    lateinit var title: TextView

    override fun initializeComponents() {
        title = findViewById(R.id.title)
        back = findViewById(R.id.back)

        title.text = getString(R.string.notification)
        back.setOnClickListener {if (user.userData.user_type.equals("delegate")){
            startActivity(Intent(this,MainActivity::class.java))
            finish()
        }else{
            startActivity(Intent(this,com.aait.bubbles.UI.Activities.Main.Client.MainActivity::class.java))
            finish()
        }
            }

        rv_recycle = findViewById(R.id.rv_recycle)
        layNoInternet = findViewById(R.id.lay_no_internet)
        layNoItem = findViewById(R.id.lay_no_item)
        tvNoContent = findViewById(R.id.tv_no_content)
        swipeRefresh = findViewById(R.id.swipe_refresh)
        linearLayoutManager = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        notificationAdapter =  NotificationAdapter(mContext,notificationModels,R.layout.recycle_notification)
        notificationAdapter.setOnItemClickListener(this)
        rv_recycle!!.layoutManager= linearLayoutManager
        rv_recycle!!.adapter = notificationAdapter
        swipeRefresh!!.setColorSchemeResources(
            R.color.colorPrimary,
            R.color.colorPrimaryDark,
            R.color.colorAccent
        )
        swipeRefresh!!.setOnRefreshListener { getHome() }
        getHome()


    }
    fun getHome(){
        showProgressDialog(getString(R.string.please_wait))
        layNoInternet!!.visibility = View.GONE
        layNoItem!!.visibility = View.GONE
        Client.getClient()?.create(Service::class.java)?.Notification(lang.appLanguage,"Bearer"+user.userData.token)?.enqueue(object :
            Callback<NotificationResponse> {
            override fun onResponse(call: Call<NotificationResponse>, response: Response<NotificationResponse>) {
                swipeRefresh!!.isRefreshing = false
                hideProgressDialog()
                if (response.isSuccessful) {
                    try {
                        if (response.body()?.value.equals("1")) {
                            Log.e("myJobs", Gson().toJson(response.body()!!.data))
                            if (response.body()!!.data?.isEmpty()!!) {
                                layNoItem!!.visibility = View.VISIBLE
                                layNoInternet!!.visibility = View.GONE
                            } else {
//                            initSliderAds(response.body()?.slider!!)
                                notificationAdapter.updateAll(response.body()!!.data!!)
                            }
//                            response.body()!!.data?.let { homeSeekerAdapter.updateAll(it)
                        }else {
                            CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        }

                    } catch (e: Exception) {e.printStackTrace() }

                } else {  }
            }
            override fun onFailure(call: Call<NotificationResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                layNoInternet!!.visibility = View.VISIBLE
                layNoItem!!.visibility = View.GONE
                swipeRefresh!!.isRefreshing = false
                hideProgressDialog()
                Log.e("response", Gson().toJson(t))
            }
        })


    }

    override fun onItemClick(view: View, position: Int) {
        if (view.id == R.id.delete) {
            showProgressDialog(getString(R.string.please_wait))
            Client.getClient()?.create(Service::class.java)?.delete(
                "Bearer"+user.userData.token,lang.appLanguage
                , notificationModels.get(position).id!!
            )?.enqueue(object :
                Callback<TermsResponse> {
                override fun onFailure(call: Call<TermsResponse>, t: Throwable) {
                    CommonUtil.handleException(mContext, t)
                    t.printStackTrace()
                    hideProgressDialog()
                }

                override fun onResponse(
                    call: Call<TermsResponse>,
                    response: Response<TermsResponse>
                ) {
                    hideProgressDialog()
                    if (response.isSuccessful) {
                        if (response.body()?.value.equals("1")) {
                            CommonUtil.makeToast(mContext, response.body()?.data!!)
                            getHome()
                        } else {
                            CommonUtil.makeToast(mContext, response.body()?.msg!!)
                        }
                    }
                }

            })
        }else if (view.id == R.id.lay) {
            if (user.userData.user_type.equals("delegate")) {
                if (notificationModels.get(position).type.equals("new_order")) {
                    val intent = Intent(this, NewOrderDetailsActivity::class.java)
                    intent.putExtra("id", notificationModels.get(position).order_id)
                    startActivity(intent)
                } else if (notificationModels.get(position).type.equals("order")) {
                    val intent = Intent(this, OrderFollowActivity::class.java)
                    intent.putExtra("id", notificationModels.get(position).order_id)
                    startActivity(intent)
                } else {

                }
            }else{
                 if (notificationModels.get(position).type.equals("order")) {
                    val intent = Intent(this, OrderDetailsActivity::class.java)
                    intent.putExtra("id", notificationModels.get(position).order_id)
                    startActivity(intent)
                }
            }
        }
    }
}