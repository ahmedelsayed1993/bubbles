package com.aait.bubbles.UI.Controllers

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aait.bubbles.Base.ParentRecyclerAdapter
import com.aait.bubbles.Base.ParentRecyclerViewHolder
import com.aait.bubbles.Models.HomeModel
import com.aait.bubbles.Models.ProductModel
import com.aait.bubbles.Models.ServicesModel
import com.aait.bubbles.R
import com.bumptech.glide.Glide

class ProductAdapter (context: Context, data: MutableList<ProductModel>, layoutId: Int) :
        ParentRecyclerAdapter<ProductModel>(context, data, layoutId) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParentRecyclerViewHolder {
        val itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false)
        return ViewHolder(itemView)
    }
    var TextView.textColor: Int
        get() = currentTextColor
        set(v) = setTextColor(v)



    override fun onBindViewHolder(holder: ParentRecyclerViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder
        val questionModel = data.get(position)
        viewHolder.name!!.setText(questionModel.name)
        Glide.with(mcontext).load(questionModel.image).into(viewHolder.image)
        if (questionModel.favorite==0){
            viewHolder.fav.setImageResource(R.mipmap.fav)
        }else{
            viewHolder.fav.setImageResource(R.mipmap.favactive)
        }
        if (questionModel.in_cart==0){
            viewHolder.icon.visibility = View.VISIBLE
            viewHolder.text.visibility = View.GONE
        }else{
            viewHolder.icon.visibility = View.GONE
            viewHolder.text.visibility = View.VISIBLE
        }
        viewHolder.productServicesAdapter = ProductServicesAdapter(mcontext,viewHolder.servicesModels,R.layout.recycle_product_service)
        viewHolder.services.layoutManager = LinearLayoutManager(mcontext,
                LinearLayoutManager.VERTICAL,false)
        viewHolder.services.adapter = viewHolder.productServicesAdapter
        viewHolder.productServicesAdapter.updateAll(questionModel.services!!)
        val animation = AnimationUtils.loadAnimation(mcontext, R.anim.item_animation_from_bottom_slow)
        animation.setDuration(250)
        viewHolder.itemView.startAnimation(animation)

        viewHolder.lay.setOnClickListener(View.OnClickListener { view -> onItemClickListener.onItemClick(view,position)

        })
        viewHolder.fav.setOnClickListener(View.OnClickListener { view -> onItemClickListener.onItemClick(view,position)

        })



    }
    inner class ViewHolder internal constructor(itemView: View) :
            ParentRecyclerViewHolder(itemView) {




        lateinit var productServicesAdapter: ProductServicesAdapter
        internal var servicesModels = ArrayList<ServicesModel>()
        internal var name=itemView.findViewById<TextView>(R.id.name)
        internal var image = itemView.findViewById<ImageView>(R.id.image)
        internal var services = itemView.findViewById<RecyclerView>(R.id.services)
        internal var fav = itemView.findViewById<ImageView>(R.id.fav)
        internal var icon = itemView.findViewById<ImageView>(R.id.icon)
        internal var text = itemView.findViewById<TextView>(R.id.text)
        internal var lay = itemView.findViewById<LinearLayout>(R.id.lay)




    }
}