package com.aait.bubbles.UI.Views

import android.content.Context
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import android.widget.Toast
import com.aait.bubbles.R


/**
 * Created by Ahmed El_sayed on 16/1/2019.
 */

class Toaster(internal var mContext: Context) : Toast(mContext) {

    fun makeToast(message: String) {
        val inflater = mContext
            .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater.inflate(R.layout.toast_custom_layout, null)
        val tv_toast = view.findViewById<View>(R.id.tv_toast_message) as TextView
        tv_toast.gravity = Gravity.CENTER
        val toast = Toast(mContext)
        tv_toast.text = message
        toast.view = view

        toast.setGravity(Gravity.FILL_HORIZONTAL or Gravity.BOTTOM, 0, 70)
        toast.duration = Toast.LENGTH_LONG
        toast.show()
    }
}