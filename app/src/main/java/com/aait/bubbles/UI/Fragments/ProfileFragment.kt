package com.aait.bubbles.UI.Fragments

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import com.aait.bubbles.Base.BaseFragment
import com.aait.bubbles.Models.TermsResponse
import com.aait.bubbles.Network.Client
import com.aait.bubbles.Network.Service
import com.aait.bubbles.R
import com.aait.bubbles.UI.Activities.AppInfo.*
import com.aait.bubbles.UI.Activities.Auth.LoginActivity
import com.aait.bubbles.UI.Activities.Auth.ProfileActivity
import com.aait.bubbles.UI.Activities.Main.Client.MyAddressesActivity
import com.aait.bubbles.UI.Activities.Main.Client.MyTimesActivity
import com.aait.bubbles.UI.Activities.Main.Client.PackagesActivity
import com.aait.bubbles.UI.Activities.SplashActivity
import com.aait.bubbles.Utils.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ProfileFragment:BaseFragment() {
    override val layoutResource: Int
        get() = R.layout.fragment_profile
    companion object {
        fun newInstance(): ProfileFragment {
            val args = Bundle()
            val fragment = ProfileFragment()
            fragment.setArguments(args)
            return fragment
        }
    }
    lateinit var profile: LinearLayout
    lateinit var complaint: LinearLayout
    lateinit var language: LinearLayout
    lateinit var call_us: LinearLayout
    lateinit var policy: LinearLayout
    lateinit var about_app: LinearLayout
    lateinit var logout: LinearLayout
    lateinit var my_address:LinearLayout
    lateinit var packages:LinearLayout
    lateinit var times:LinearLayout
    lateinit var text:TextView
    override fun initializeComponents(view: View) {
        profile = view.findViewById(R.id.profile)
        language = view.findViewById(R.id.language)
        call_us = view.findViewById(R.id.call_us)
        policy = view.findViewById(R.id.policy)
        about_app = view.findViewById(R.id.about_app)
        complaint = view.findViewById(R.id.complaint)
        logout = view.findViewById(R.id.logout)
        my_address = view.findViewById(R.id.my_addresses)
        packages = view.findViewById(R.id.packages)
        times = view.findViewById(R.id.times)
        text = view.findViewById(R.id.text)
        if (user.loginStatus!!){
            text.text = getString(R.string.logout)
        }else{
            text.text = getString(R.string.sign_in)
        }
        logout.setOnClickListener {
            if (user.loginStatus!!) {
                logout()
            }else{
                startActivity(Intent(activity,LoginActivity::class.java))
                activity!!.finish()
            }
        }
        about_app.setOnClickListener { startActivity(Intent(activity, AboutAppActivity::class.java)) }
        policy.setOnClickListener { startActivity(Intent(activity, TermsActivity::class.java)) }
        call_us.setOnClickListener { startActivity(Intent(activity, CallUsActivity::class.java)) }
        complaint.setOnClickListener { startActivity(Intent(activity, ComplainActivity::class.java)) }
        language.setOnClickListener { startActivity(Intent(activity, LangActivity::class.java)) }
        profile.setOnClickListener {
            if (user.loginStatus!!) {
                startActivity(Intent(activity, ProfileActivity::class.java))
            }else{
                CommonUtil.makeToast(mContext!!,getString(R.string.sorry_visitor))
            }
            }
        my_address.setOnClickListener { if (user.loginStatus!!) {
            startActivity(Intent(activity, MyAddressesActivity::class.java))
        }else{
            CommonUtil.makeToast(mContext!!,getString(R.string.sorry_visitor))
        }
        }
        packages.setOnClickListener {if (user.loginStatus!!) {

            startActivity(Intent(activity, PackagesActivity::class.java))
        }else{
            CommonUtil.makeToast(mContext!!,getString(R.string.sorry_visitor))
        }
        }
        times.setOnClickListener {
            if (user.loginStatus!!){
                startActivity(Intent(activity,MyTimesActivity::class.java))
            }
            else{
                CommonUtil.makeToast(mContext!!,getString(R.string.sorry_visitor))
            }
        }


    }
    fun logout(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.logOut("Bearer "+user.userData.token,user.userData.device_id!!,"android",lang.appLanguage)?.enqueue(object :
                Callback<TermsResponse> {
            override fun onFailure(call: Call<TermsResponse>, t: Throwable) {
                CommonUtil.handleException(mContext!!,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                    call: Call<TermsResponse>,
                    response: Response<TermsResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        user.loginStatus=false
                        user.Logout()
                        CommonUtil.makeToast(mContext!!,response.body()?.data!!)
                        startActivity(Intent(activity, SplashActivity::class.java))
                        activity!!.finish()
                    }else{
                        user.loginStatus=false
                        user.Logout()
                        // CommonUtil.makeToast(mContext!!,response.body()?.data!!)
                        startActivity(Intent(activity, SplashActivity::class.java))
                        activity!!.finish()

                    }
                }
            }
        })
    }
}