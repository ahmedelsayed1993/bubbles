package com.aait.bubbles.UI.Controllers

import android.content.Context
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.TextView
import androidx.annotation.RequiresApi
import com.aait.bubbles.Base.ParentRecyclerAdapter
import com.aait.bubbles.Base.ParentRecyclerViewHolder
import com.aait.bubbles.Models.DatesModel
import com.aait.bubbles.Models.TimeModel
import com.aait.bubbles.R

class MyTimesAdapter (context: Context, data: MutableList<DatesModel>, layoutId: Int) :
        ParentRecyclerAdapter<DatesModel>(context, data, layoutId) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParentRecyclerViewHolder {
        val itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false)
        return ViewHolder(itemView)
    }
    var TextView.textColor: Int
        get() = currentTextColor
        set(v) = setTextColor(v)
    var selected :Int = 0

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onBindViewHolder(holder: ParentRecyclerViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder
        val questionModel = data.get(position)
        viewHolder.date!!.setText(questionModel.date)
        viewHolder.time!!.setText(questionModel.time)
        val animation = AnimationUtils.loadAnimation(mcontext, R.anim.item_animation_from_right)
        animation.setDuration(250)
        viewHolder.itemView.startAnimation(animation)





    }
    inner class ViewHolder internal constructor(itemView: View) :
            ParentRecyclerViewHolder(itemView) {





        internal var time=itemView.findViewById<TextView>(R.id.time)
        internal var date=itemView.findViewById<TextView>(R.id.date)




    }
}