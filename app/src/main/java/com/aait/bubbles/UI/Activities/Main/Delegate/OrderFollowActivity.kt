package com.aait.bubbles.UI.Activities.Main.Delegate

import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.VideoView
import com.aait.bubbles.Base.ParentActivity
import com.aait.bubbles.Models.DelegateOrderDetailsResponse
import com.aait.bubbles.Network.Client
import com.aait.bubbles.Network.Service
import com.aait.bubbles.R
import com.aait.bubbles.Utils.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class OrderFollowActivity:ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_follow_order
    lateinit var back:ImageView
    lateinit var title:TextView
    lateinit var one:ImageView
    lateinit var two:ImageView
    lateinit var three:ImageView
    lateinit var four:ImageView
    lateinit var line:TextView
    lateinit var line1:TextView
    lateinit var line2:TextView
    lateinit var receive_client:Button
    lateinit var deliver_provider:Button
    lateinit var receive_provider:Button
    lateinit var deliver_client:Button
    lateinit var text:TextView
    var id = 0
    override fun initializeComponents() {
        id = intent.getIntExtra("id",0)
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        one = findViewById(R.id.one)
        two = findViewById(R.id.two)
        three = findViewById(R.id.three)
        four = findViewById(R.id.four)
        line = findViewById(R.id.line)
        line1 = findViewById(R.id.line1)
        line2 = findViewById(R.id.line2)
        receive_client = findViewById(R.id.receive_client)
        deliver_provider = findViewById(R.id.deliver_provider)
        receive_provider = findViewById(R.id.receive_provider)
        deliver_client = findViewById(R.id.deliver_client)
        text = findViewById(R.id.text)
        title.text = getString(R.string.follow_order)
        back.setOnClickListener { onBackPressed()
        finish()}
        getData(null)
        receive_client.setOnClickListener {
            getData("received_order_client")
        }
        deliver_provider.setOnClickListener {
            getData("delivery_order_provider")
        }
        receive_provider.setOnClickListener {
            getData("received_order_provider")
        }
        deliver_client.setOnClickListener {
            getData("delivery_order_client")
        }

    }

    fun getData(status:String?){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.DelegateOrderDetails(lang.appLanguage,"Bearer"+user.userData.token,id,status)
                ?.enqueue(object : Callback<DelegateOrderDetailsResponse> {
                    override fun onFailure(call: Call<DelegateOrderDetailsResponse>, t: Throwable) {
                        hideProgressDialog()
                        CommonUtil.handleException(mContext,t)
                        t.printStackTrace()
                    }

                    override fun onResponse(call: Call<DelegateOrderDetailsResponse>, response: Response<DelegateOrderDetailsResponse>) {
                        hideProgressDialog()
                        if (response.isSuccessful){
                            if (response.body()?.value.equals("1")){
                                if (response.body()?.data?.category.equals("store")){

                                    two.visibility = View.GONE
                                    line.visibility = View.GONE
                                    text.visibility = View.GONE


                                }else{
                                    two.visibility = View.VISIBLE
                                    line.visibility = View.VISIBLE
                                    text.visibility = View.VISIBLE
                                }

                                if (response.body()?.data?.status!!.equals("delegate_accepted")){


                                    if (response.body()?.data?.category.equals("category")) {
                                        one.setImageResource(R.mipmap.orderstatuesthree)
                                        two.setImageResource(R.mipmap.deliverystatuesone)
                                        three.setImageResource(R.mipmap.deliverystatues)
                                        four.setImageResource(R.mipmap.cartstatues)
                                        receive_client.visibility = View.VISIBLE
                                        deliver_provider.visibility = View.GONE
                                        receive_provider.visibility = View.GONE
                                        deliver_client.visibility = View.GONE
                                    }else{
                                        one.setImageResource(R.mipmap.orderstatuesthree)
                                        two.setImageResource(R.mipmap.deliverystatuesone)
                                        three.setImageResource(R.mipmap.deliverystatuesone)
                                        four.setImageResource(R.mipmap.cartstatues)
                                        receive_client.visibility = View.GONE
                                        deliver_provider.visibility = View.GONE
                                        receive_provider.visibility = View.VISIBLE
                                        deliver_client.visibility = View.GONE
                                    }
                                }else if (response.body()?.data?.status.equals("received_order_client")){
                                    one.setImageResource(R.mipmap.orderstatuesthree)
                                    two.setImageResource(R.mipmap.deliverystatuesone)
                                    three.setImageResource(R.mipmap.deliverystatues)
                                    four.setImageResource(R.mipmap.cartstatues)
                                    receive_client.visibility = View.GONE
                                    deliver_provider.visibility = View.VISIBLE
                                    receive_provider.visibility = View.GONE
                                    deliver_client.visibility = View.GONE
                                }
                                else if (response.body()?.data?.status.equals("delivery_order_provider")){
                                    one.setImageResource(R.mipmap.orderstatuesthree)
                                    two.setImageResource(R.mipmap.deliverystatuesthree)
                                    three.setImageResource(R.mipmap.deliverystatues)
                                    four.setImageResource(R.mipmap.cartstatues)
                                    receive_client.visibility = View.GONE
                                    deliver_provider.visibility = View.GONE
                                    receive_provider.visibility = View.VISIBLE
                                    deliver_client.visibility = View.GONE
                                }
                                else if (response.body()?.data?.status.equals("received_order_provider")){
                                    one.setImageResource(R.mipmap.orderstatuesthree)
                                    two.setImageResource(R.mipmap.deliverystatuesthree)
                                    three.setImageResource(R.mipmap.deliverystatuesthree)
                                    four.setImageResource(R.mipmap.cartstatues)
                                    receive_client.visibility = View.GONE
                                    deliver_provider.visibility = View.GONE
                                    receive_provider.visibility = View.GONE
                                    deliver_client.visibility = View.VISIBLE
                                }
                                else if (response.body()?.data?.status.equals("delivery_order_client")){
                                    one.setImageResource(R.mipmap.orderstatuesthree)
                                    two.setImageResource(R.mipmap.deliverystatuesthree)
                                    three.setImageResource(R.mipmap.deliverystatuesthree)
                                    four.setImageResource(R.mipmap.cartstatuesthree)
                                    receive_client.visibility = View.GONE
                                    deliver_provider.visibility = View.GONE
                                    receive_provider.visibility = View.GONE
                                    deliver_client.visibility = View.GONE
                                }else{
                                    one.setImageResource(R.mipmap.orderstatuesthree)
                                    two.setImageResource(R.mipmap.deliverystatuesthree)
                                    three.setImageResource(R.mipmap.deliverystatuesthree)
                                    four.setImageResource(R.mipmap.cartstatuesthree)
                                    receive_client.visibility = View.GONE
                                    deliver_provider.visibility = View.GONE
                                    receive_provider.visibility = View.GONE
                                    deliver_client.visibility = View.GONE
                                }
                            }else{
                                CommonUtil.makeToast(mContext,response.body()?.msg!!)
                            }
                        }
                    }

                })
    }
}