package com.aait.bubbles.UI.Activities.Main.Client

import android.content.Intent
import android.widget.Button
import android.widget.ImageView
import android.widget.RatingBar
import android.widget.TextView
import com.aait.bubbles.Base.ParentActivity
import com.aait.bubbles.Models.TermsResponse
import com.aait.bubbles.Network.Client
import com.aait.bubbles.Network.Service
import com.aait.bubbles.R
import com.aait.bubbles.Utils.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AddRateActivity:ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_add_rate
    lateinit var back:ImageView
    lateinit var title:TextView
    lateinit var rating:RatingBar
    lateinit var rating_del:RatingBar
    lateinit var rate:Button
    var id = 0

    override fun initializeComponents() {
        id = intent.getIntExtra("id",0)
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        rating = findViewById(R.id.rating)
        rating_del = findViewById(R.id.rating_del)
        rate = findViewById(R.id.rate)
        back.setOnClickListener { startActivity(Intent(this,MainActivity::class.java))
            finish()
        }
        title.text = getString(R.string.Service_evaluation)
        rate.setOnClickListener {
            showProgressDialog(getString(R.string.please_wait))
            Client.getClient()?.create(Service::class.java)?.AddRate("Bearer"+user.userData.token,lang.appLanguage,id,rating.rating.toInt(),rating_del.rating.toInt())
                    ?.enqueue(object : Callback<TermsResponse> {
                        override fun onFailure(call: Call<TermsResponse>, t: Throwable) {
                            hideProgressDialog()
                            CommonUtil.handleException(mContext,t)
                            t.printStackTrace()
                        }

                        override fun onResponse(call: Call<TermsResponse>, response: Response<TermsResponse>) {
                            hideProgressDialog()
                            if (response.isSuccessful){
                                if (response.body()?.value.equals("1")){
                                    CommonUtil.makeToast(mContext,response.body()?.data!!)
                                    startActivity(Intent(this@AddRateActivity,MainActivity::class.java))
                                }else{
                                    CommonUtil.makeToast(mContext,response.body()?.msg!!)
                                }
                            }
                        }

                    })
        }

    }
}