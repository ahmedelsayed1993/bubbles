package com.aait.bubbles.UI.Activities.Main.Client

import android.Manifest
import android.content.DialogInterface
import android.content.Intent
import android.location.Address
import android.location.Geocoder
import android.os.Build
import android.provider.Settings
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.aait.bubbles.Base.ParentActivity
import com.aait.bubbles.GPS.GPSTracker
import com.aait.bubbles.GPS.GpsTrakerListener
import com.aait.bubbles.Models.*
import com.aait.bubbles.Network.Client
import com.aait.bubbles.Network.Service
import com.aait.bubbles.R
import com.aait.bubbles.UI.Controllers.ProductSliderAdapter
import com.aait.bubbles.UI.Controllers.ProductSlidersAdapter
import com.aait.bubbles.Utils.CommonUtil
import com.aait.bubbles.Utils.DialogUtil
import com.aait.bubbles.Utils.PermissionUtils
import com.github.islamkhsh.CardSliderViewPager
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.Marker
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException
import java.util.*

class ProductDetailsActivity:ParentActivity(), GpsTrakerListener {
    override val layoutResource: Int
        get() = R.layout.activity_product_details
    lateinit var back:ImageView
    lateinit var title:TextView
    lateinit var viewPager:CardSliderViewPager
    lateinit var name:TextView
    lateinit var fav:ImageView
    lateinit var price:TextView
    lateinit var desc:TextView
    lateinit var plus:ImageView
    lateinit var minus:ImageView
    lateinit var count:TextView
    lateinit var add:Button
    private var mAlertDialog: AlertDialog? = null
    internal lateinit var geocoder: Geocoder
    internal lateinit var gps: GPSTracker
    internal var startTracker = false
    var mLang = ""
    var mLat = ""
    var result = ""
    var ID = ""
    var id = 0
    var homeModel = 0
    var num = 1
    var model = ArrayList<Model>()
    lateinit var productDetailsModel: ProductDetailsModel
    override fun initializeComponents() {
        id = intent.getIntExtra("id",0)
        ID =  Settings.Secure.getString(mContext.contentResolver, Settings.Secure.ANDROID_ID)
        homeModel = intent.getIntExtra("cat",0)
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        viewPager = findViewById(R.id.viewPager)
        name = findViewById(R.id.name)
        fav = findViewById(R.id.fav)
        price = findViewById(R.id.price)
        desc = findViewById(R.id.desc)
        plus = findViewById(R.id.plus)
        minus = findViewById(R.id.minus)
        count = findViewById(R.id.count)
        add = findViewById(R.id.add)
        back.setOnClickListener { onBackPressed()
        finish()}
        title.text = ""
        getLocationWithPermission(null)
        plus.setOnClickListener {
           num = num+1
            count.text = num.toString()
        }
        minus.setOnClickListener {
            if (num<=1){

            }else{
                num = num-1
                count.text = num.toString()
            }
        }

        fav.setOnClickListener {
            if (user.loginStatus!!){
                showProgressDialog(getString(R.string.please_wait))
                Client.getClient()?.create(Service::class.java)?.Favourite(lang.appLanguage,"Bearer"+user.userData.token
                        ,id)?.enqueue(object :Callback<FavResponse>{
                    override fun onFailure(call: Call<FavResponse>, t: Throwable) {
                        hideProgressDialog()
                        CommonUtil.handleException(mContext,t)
                        t.printStackTrace()
                    }

                    override fun onResponse(call: Call<FavResponse>, response: Response<FavResponse>) {
                        hideProgressDialog()
                        if (response.isSuccessful){
                            if (response.body()?.value.equals("1")){
                                getLocationWithPermission(null)
                                CommonUtil.makeToast(mContext,response.body()?.msg!!)
                            }else{
                                CommonUtil.makeToast(mContext,response.body()?.msg!!)
                            }
                        }
                    }

                })
            }else{
                CommonUtil.makeToast(mContext!!,getString(R.string.sorry_visitor))
            }
        }
        add.setOnClickListener {
            model.add(Model(id,productDetailsModel.price,num))
            showProgressDialog(getString(R.string.please_wait))
            Client.getClient()?.create(Service::class.java)?.AddToCart(lang.appLanguage,ID,homeModel!!
                    , Gson().toJson(model))?.enqueue(object :Callback<CartResponse>{
                override fun onFailure(call: Call<CartResponse>, t: Throwable) {
                    hideProgressDialog()
                    CommonUtil.handleException(mContext,t)
                    t.printStackTrace()
                }

                override fun onResponse(call: Call<CartResponse>, response: Response<CartResponse>) {
                    hideProgressDialog()
                    if (response.isSuccessful){
                        if (response.body()?.value.equals("1")){
                            val intent = Intent(this@ProductDetailsActivity,StoreActivity::class.java)
                            intent.putExtra("category",homeModel)
                            startActivity(intent)
                            finish()
                        }else{
                            CommonUtil.makeToast(mContext,response.body()?.msg!!)

                        }
                    }
                }

            })

        }

    }

    override fun onTrackerSuccess(lat: Double?, log: Double?) {
        if (startTracker) {
            if (lat != 0.0 && log != 0.0) {
                hideProgressDialog()
                // putMapMarker(lat, log)
            }
        }
    }

    override fun onStartTracker() {
        startTracker = true
    }

    fun getLocationWithPermission(cat:Int?) {
        gps = GPSTracker(mContext!!, this)
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!(PermissionUtils.hasPermissions(mContext, Manifest.permission.ACCESS_FINE_LOCATION)&&
                            (PermissionUtils.hasPermissions(mContext,
                                    Manifest.permission.ACCESS_COARSE_LOCATION)))) {
                CommonUtil.PrintLogE("Permission not granted")
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(
                            PermissionUtils.GPS_PERMISSION,
                            800
                    )
                    Log.e("GPS", "1")
                }
            } else {
                getCurrentLocation(cat)
                Log.e("GPS", "2")
            }
        } else {
            Log.e("GPS", "3")
            getCurrentLocation(cat)
        }

    }

    internal fun getCurrentLocation(cat:Int?) {
        gps.getLocation()
        if (!gps.canGetLocation()) {
            mAlertDialog = DialogUtil.showAlertDialog(mContext!!,
                    getString(R.string.gps_detecting),
                    DialogInterface.OnClickListener { dialogInterface, i ->
                        mAlertDialog?.dismiss()
                        val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                        startActivityForResult(intent, 300)
                    })
        } else {
            if (gps.getLatitude() !== 0.0 && gps.getLongitude() !== 0.0) {
                // putMapMarker(gps.getLatitude(), gps.getLongitude())
                if (user.loginStatus!!) {
                    getData("Bearer"+user.userData.token,gps.getLatitude().toString(), gps.getLongitude().toString())
                }else{
                    getData(null,gps.getLatitude().toString(), gps.getLongitude().toString())
                }
                mLat = gps.getLatitude().toString()
                mLang = gps.getLongitude().toString()
                val addresses: List<Address>
                geocoder = Geocoder(mContext!!, Locale.getDefault())
                try {
                    addresses = geocoder.getFromLocation(
                            java.lang.Double.parseDouble(mLat),
                            java.lang.Double.parseDouble(mLang),
                            1
                    )
                    if (addresses.isEmpty()) {
                        Toast.makeText(
                                mContext,
                                resources.getString(R.string.detect_location),
                                Toast.LENGTH_SHORT
                        ).show()
                    } else {
                        result = addresses[0].getAddressLine(0)
                        Log.e("address",result)
                        // CommonUtil.makeToast(mContext,addresses[0].getAddressLine(0))



                    }

                } catch (e: IOException) {
                }


                //putMapMarker(gps.getLatitude(), gps.getLongitude())


            }
        }
    }
    fun getData(token:String?,lat:String,lng:String){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.getProduct(token,lang.appLanguage,id,lat,lng)?.enqueue(object:
                Callback<ProductDetailsResponse> {
            override fun onFailure(call: Call<ProductDetailsResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
            }

            override fun onResponse(
                    call: Call<ProductDetailsResponse>,
                    response: Response<ProductDetailsResponse>
            ) {
                hideProgressDialog()

                if(response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        productDetailsModel = response.body()?.data!!
                        initSliderAds(response.body()?.data?.images!!)
                        name.text = response.body()?.data?.name
                        price.text = response.body()?.data?.price+getString(R.string.rs)
                        desc.text = response.body()?.data?.desc
                        if (response.body()?.data?.favorite==0){
                            fav.setImageResource(R.mipmap.fav)
                        }else if (response.body()?.data?.favorite==1){
                            fav.setImageResource(R.mipmap.favactive)
                        }

                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }

    fun initSliderAds(list:ArrayList<ImageModel>){
        if(list.isEmpty()){
            viewPager.visibility= View.GONE


        }
        else{
            viewPager.visibility= View.VISIBLE


            viewPager.adapter= ProductSlidersAdapter(mContext!!,list)

        }
    }
}