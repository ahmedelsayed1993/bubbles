package com.aait.bubbles.UI.Activities.Main.Delegate

import android.content.Intent
import android.net.Uri
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aait.bubbles.Base.ParentActivity
import com.aait.bubbles.Listeners.OnItemClickListener
import com.aait.bubbles.Models.*
import com.aait.bubbles.Network.Client
import com.aait.bubbles.Network.Service
import com.aait.bubbles.R
import com.aait.bubbles.UI.Controllers.OrderEditProductAdapter
import com.aait.bubbles.UI.Controllers.OrderProductAdapter
import com.aait.bubbles.UI.Controllers.ServiceAdapter
import com.aait.bubbles.UI.Controllers.ServicesAdapter
import com.aait.bubbles.Utils.CommonUtil
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class OrderAcceptedActivity : ParentActivity(),OnItemClickListener {
    override val layoutResource: Int
        get() = R.layout.activity_order_accepted
    var id = 0
    lateinit var title: TextView
    lateinit var back: ImageView
    lateinit var name: TextView
    lateinit var phone: TextView
    lateinit var receive_date: TextView
    lateinit var delivery_date: TextView
    lateinit var orders: RecyclerView
    lateinit var location: TextView
    lateinit var payment: TextView
    lateinit var notes: TextView
    lateinit var prods_value: TextView
    lateinit var added_value: TextView
    lateinit var added: TextView
    lateinit var delivery_value: TextView
    lateinit var total: TextView
    lateinit var accept: Button
    lateinit var refuse: Button
    lateinit var orderProductAdapter: OrderEditProductAdapter
    lateinit var linearLayoutManager: LinearLayoutManager
    var cartModels = ArrayList<CartModel>()
    lateinit var service_lay: LinearLayout
    lateinit var services:RecyclerView
    lateinit var ttotal:TextView
    lateinit var confirm:Button
    lateinit var cancel:Button
    lateinit var mlinearLayoutManager: LinearLayoutManager
    lateinit var servicesAdapter: ServiceAdapter
    var servicesModels = ArrayList<ServicesModel>()
    var tot = 0
    var models = ArrayList<Model>()
    var lat = ""
    var lng = ""
    var product = 0
    override fun initializeComponents() {
        id = intent.getIntExtra("id",0)
        title = findViewById(R.id.title)
        back = findViewById(R.id.back)
        name = findViewById(R.id.name)
        phone = findViewById(R.id.phone)
        receive_date = findViewById(R.id.receive_date)
        delivery_date = findViewById(R.id.delivery_date)
        orders = findViewById(R.id.orders)
        location = findViewById(R.id.location)
        payment = findViewById(R.id.payment)
        notes = findViewById(R.id.notes)
        prods_value = findViewById(R.id.prods_value)
        added_value = findViewById(R.id.added_value)
        added = findViewById(R.id.added)
        delivery_value = findViewById(R.id.delivery_value)
        total = findViewById(R.id.total)
        accept = findViewById(R.id.accept)
        service_lay = findViewById(R.id.service_lay)
        services = findViewById(R.id.services)
        ttotal = findViewById(R.id.Total)
        confirm = findViewById(R.id.confirm)
        cancel = findViewById(R.id.cancel)
        back.setOnClickListener { onBackPressed()
            finish()}
        title.text = getString(R.string.order_details)
        linearLayoutManager = LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL,false)
        orderProductAdapter = OrderEditProductAdapter(mContext,cartModels, R.layout.recycle_edit_order)
        orderProductAdapter.setOnItemClickListener(this)
        orders.layoutManager = linearLayoutManager
        orders.adapter = orderProductAdapter
        mlinearLayoutManager = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        servicesAdapter = ServiceAdapter(mContext,servicesModels,R.layout.recycle_services)
        servicesAdapter.setOnItemClickListener(this)
        services.layoutManager = mlinearLayoutManager
        services.adapter = servicesAdapter
        getData()
        location.setOnClickListener {
            startActivity(
                    Intent(
                            Intent.ACTION_VIEW,
                            Uri.parse("http://maps.google.com/maps?saddr=" + user.userData.lat + "," + user.userData.lng + "&daddr=" + lat + "," + lng)
                    )
            )

        }
        accept.setOnClickListener {
            val intent = Intent(this,OrderFollowActivity::class.java)
            intent.putExtra("id",id)
            startActivity(intent)
        }
        confirm.setOnClickListener {
            models.clear()
            for (i in 0..servicesAdapter.data?.size-1){
                if (servicesAdapter.data?.get(i).count==0){

                }else{
                    models.add(Model(product,servicesAdapter.data?.get(i)?.service_id,servicesAdapter.data?.get(i).price,servicesAdapter.data?.get(i)?.count!!.toInt()))
                }
            }
            Log.e("model", Gson().toJson(models))
            if (models.isEmpty()){
                CommonUtil.makeToast(mContext,getString(R.string.choose_type))
            }else{
                Client.getClient()?.create(Service::class.java)?.EditServices("Bearer"+user.userData.token,lang.appLanguage,product,id,Gson().toJson(models))?.enqueue(
                        object :Callback<ServicesResponse>{
                            override fun onFailure(call: Call<ServicesResponse>, t: Throwable) {
                                hideProgressDialog()
                                CommonUtil.handleException(mContext,t)
                                t.printStackTrace()
                                service_lay.visibility = View.GONE
                            }

                            override fun onResponse(call: Call<ServicesResponse>, response: Response<ServicesResponse>) {
                                hideProgressDialog()
                                if (response.isSuccessful){
                                    if (response.body()?.value.equals("1")){
                                        service_lay.visibility = View.GONE
                                       getData()
                                    }else{
                                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                                        service_lay.visibility =  View.GONE
                                    }
                                }
                            }

                        }
                )
            }
        }
        cancel.setOnClickListener { service_lay.visibility = View.GONE }


    }

    fun getData(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.DelegateOrderDetails(lang.appLanguage,"Bearer"+user.userData.token,id,null)
                ?.enqueue(object : Callback<DelegateOrderDetailsResponse> {
                    override fun onFailure(call: Call<DelegateOrderDetailsResponse>, t: Throwable) {
                        hideProgressDialog()
                        CommonUtil.handleException(mContext,t)
                        t.printStackTrace()
                    }

                    override fun onResponse(call: Call<DelegateOrderDetailsResponse>, response: Response<DelegateOrderDetailsResponse>) {
                        hideProgressDialog()
                        if (response.isSuccessful){
                            if (response.body()?.value.equals("1")){
                                name.text = response.body()?.data?.username
                                phone.text = response.body()?.data?.phone
                                delivery_date.text = response.body()?.data?.delivery_date
                                receive_date.text = response.body()?.data?.received_date
                                location.text = response.body()?.data?.address
                                lat = response.body()?.data?.lat!!
                                lng = response.body()?.data?.lng!!
                                notes.text = response.body()?.data?.notes
                                payment.text = response.body()?.data?.payment
                                prods_value.text = response.body()?.data?.total+getString(R.string.rs)
                                added_value.text = getString(R.string.Value_Added)+"("+response.body()?.data?.tax+"%)"
                                added.text = response.body()?.data?.total_tax+getString(R.string.rs)
                                delivery_value.text = response.body()?.data?.total_delivery+getString(R.string.rs)
                                total.text = response.body()?.data?.final_total+getString(R.string.rs)
                                orderProductAdapter.updateAll(response.body()?.data?.products!!)
                            }else{
                                CommonUtil.makeToast(mContext,response.body()?.msg!!)
                            }
                        }
                    }

                })
    }
    fun showService(Id:Int){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.EditServices("Bearer"+user.userData.token,lang.appLanguage,Id,id,null)?.enqueue(
                object :Callback<ServicesResponse>{
                    override fun onFailure(call: Call<ServicesResponse>, t: Throwable) {
                        hideProgressDialog()
                        CommonUtil.handleException(mContext,t)
                        t.printStackTrace()
                        service_lay.visibility = View.GONE
                    }

                    override fun onResponse(call: Call<ServicesResponse>, response: Response<ServicesResponse>) {
                        hideProgressDialog()
                        if (response.isSuccessful){
                            if (response.body()?.value.equals("1")){
                                service_lay.visibility = View.VISIBLE
                                servicesAdapter.updateAll(response.body()?.data!!)
                                for (i in 0..response.body()?.data?.size!!-1){
                                    tot = tot + ((response.body()?.data?.get(i)?.count!!.toInt())*(response.body()?.data?.get(i)?.price!!.toInt()))
                                }
                                Log.e("tot",tot.toString())
                                ttotal.text = tot.toString()+getString(R.string.rs)
                            }else{
                                CommonUtil.makeToast(mContext,response.body()?.msg!!)
                                service_lay.visibility =  View.GONE
                            }
                        }
                    }

                }
        )

    }

    override fun onItemClick(view: View, position: Int) {
        if (view.id == R.id.edit){
            product = cartModels.get(position).product_id!!
            showService(cartModels.get(position).product_id!!)
        }else  if (view.id == R.id.plus){
            tot = 0
            servicesModels.get(position).count  = servicesModels.get(position).count!!+1
            servicesAdapter.notifyDataSetChanged()
            for (i in 0..servicesModels?.size!!-1){
                tot = tot + ((servicesModels?.get(i)?.count!!.toInt())*(servicesModels?.get(i)?.price!!.toInt()))
            }
            ttotal.text = tot.toString()+getString(R.string.rs)
        }else if (view.id == R.id.minus){
            tot = 0
            if (servicesModels.get(position).count!!<=0){

            }else{
                servicesModels.get(position).count  = servicesModels.get(position).count!!-1
                servicesAdapter.notifyDataSetChanged()
            }
            for (i in 0..servicesModels?.size!!-1){
                tot = tot + ((servicesModels?.get(i)?.count!!.toInt())*(servicesModels?.get(i)?.price!!.toInt()))
            }
            ttotal.text = tot.toString()+getString(R.string.rs)
        }

    }
}