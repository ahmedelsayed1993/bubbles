package com.aait.bubbles.UI.Activities.AppInfo

import android.widget.ImageView
import android.widget.TextView
import com.aait.bubbles.Base.ParentActivity
import com.aait.bubbles.Models.TermsResponse
import com.aait.bubbles.Network.Client
import com.aait.bubbles.Network.Service
import com.aait.bubbles.R
import com.aait.bubbles.Utils.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class TermsActivity : ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_about_app
    lateinit var back: ImageView
    lateinit var title: TextView
    lateinit var about: TextView

    override fun initializeComponents() {
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        about = findViewById(R.id.about)
        getData()
        back.setOnClickListener { onBackPressed()
            finish()}
        title.text = getString(R.string.usage_policy)
    }
    fun getData(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Terms(lang.appLanguage)?.enqueue(object:
                Callback<TermsResponse> {
            override fun onFailure(call: Call<TermsResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
            }

            override fun onResponse(call: Call<TermsResponse>, response: Response<TermsResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if(response.body()?.value.equals("1")){
                        about.text = response.body()?.data!!
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
}