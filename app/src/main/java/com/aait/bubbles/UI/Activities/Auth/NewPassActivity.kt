package com.aait.bubbles.UI.Activities.Auth

import android.content.Intent
import android.widget.Button
import android.widget.EditText
import com.aait.bubbles.Base.ParentActivity
import com.aait.bubbles.Models.UserModel
import com.aait.bubbles.Models.UserResponse
import com.aait.bubbles.Network.Client
import com.aait.bubbles.Network.Service
import com.aait.bubbles.R
import com.aait.bubbles.Utils.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class NewPassActivity:ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_new_pass
    lateinit var code: EditText
    lateinit var password: EditText
    lateinit var confirm_password: EditText
    lateinit var confirm: Button
    lateinit var userModel: UserModel
    override fun initializeComponents() {
        userModel = intent.getSerializableExtra("data") as UserModel
        code = findViewById(R.id.code)
        password = findViewById(R.id.password)
        confirm = findViewById(R.id.confirm)
        confirm_password = findViewById(R.id.confirm_pass)

        confirm.setOnClickListener{
            if (CommonUtil.checkEditError(code,getString(R.string.verification_code))||
                    CommonUtil.checkEditError(password,getString(R.string.new_pass))||
                    CommonUtil.checkLength(password,getString(R.string.password_length),6)||
                    CommonUtil.checkEditError(confirm_password,getString(R.string.confirm_new_pass))){
                return@setOnClickListener
            }else{
                if (!password.text.toString().equals(confirm_password.text.toString())){
                    confirm_password.error = getString(R.string.password_not_match)
                }else{
                    NewPassword()
                }
            }
        }

    }
    fun NewPassword(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.NewPass("Bearer "+userModel.token,password.text.toString(),code.text.toString(),lang.appLanguage)?.enqueue(object :
                Callback<UserResponse> {
            override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
            }

            override fun onResponse(call: Call<UserResponse>, response: Response<UserResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        val intent = Intent(this@NewPassActivity, LoginActivity::class.java)
                        intent.putExtra("type",response.body()?.data?.user_type)
                        startActivity(intent)
                    }else if(response.body()?.value.equals("401")){
                        user.loginStatus=false
                        user.Logout()
                        val intent = Intent(this@NewPassActivity, LoginActivity::class.java)
                        intent.putExtra("type",response.body()?.data?.user_type)
                        startActivity(intent)
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
}