package com.aait.bubbles.UI.Activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.util.Log
import com.aait.bubbles.Base.ParentActivity
import com.aait.bubbles.R
import com.aait.bubbles.UI.Activities.Main.Client.MainActivity

class SplashActivity : ParentActivity(){
    override val layoutResource: Int
        get() = R.layout.activity_splash

    var isSplashFinishid = false
    override fun initializeComponents() {

        Handler().postDelayed({
            // logo.startAnimation(logoAnimation2)
            Handler().postDelayed({
                isSplashFinishid=true
                if (user.loginStatus==true){
                    if (user.userData.user_type.equals("user")) {
                        var intent = Intent(this@SplashActivity, MainActivity::class.java)
                        startActivity(intent)
                        finish()

                    }else{
                        var intent = Intent(this@SplashActivity, com.aait.bubbles.UI.Activities.Main.Delegate.MainActivity::class.java)
                        startActivity(intent)
                        finish()
                    }

                }else {
                    var intent = Intent(this@SplashActivity, IntroOneActivity::class.java)
                    startActivity(intent)
                    finish()
                }
            }, 2100)
        }, 1800)
    }

}