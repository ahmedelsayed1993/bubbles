package com.aait.bubbles.UI.Activities.Main.Client

import android.util.Log
import com.aait.bubbles.Base.ParentActivity
import com.aait.bubbles.R
import com.vivekkaushik.datepicker.DatePickerTimeline
import com.vivekkaushik.datepicker.OnDateSelectedListener
import com.vivekkaushik.datepicker.TimelineView
import java.util.*

class CalendarActivity:ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_calendar
     lateinit var datePickerTimeline:DatePickerTimeline

    override fun initializeComponents() {
       datePickerTimeline = findViewById(R.id.datePickerTimeline)



        datePickerTimeline.setInitialDate(Calendar.getInstance().get(Calendar.YEAR),Calendar.getInstance().get(Calendar.MONTH),Calendar.getInstance().get(Calendar.DAY_OF_MONTH))
// Set a date Selected Listener
// Set a date Selected Listener
        datePickerTimeline.setOnDateSelectedListener(object : OnDateSelectedListener {
            override fun onDateSelected(year: Int, month: Int, day: Int, dayOfWeek: Int) {
                Log.e("date",year.toString()+"-"+(month+1).toString()+"-"+day)
                // Do Something
            }

            override fun onDisabledDateSelected(
                year: Int,
                month: Int,
                day: Int,
                dayOfWeek: Int,
                isDisabled: Boolean
            ) {
                // Do Something
            }
        })

// Disable date

// Disable date
        val dates: Array<Date> = arrayOf<Date>(Calendar.getInstance().getTime())
        datePickerTimeline.deactivateDates(dates)
    }
}