package com.aait.bubbles.UI.Controllers

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aait.bubbles.Base.ParentRecyclerAdapter
import com.aait.bubbles.Base.ParentRecyclerViewHolder
import com.aait.bubbles.Models.CartModel
import com.aait.bubbles.Models.ServicesModel
import com.aait.bubbles.R
import com.bumptech.glide.Glide

class OrderEditProductAdapter (context: Context, data: MutableList<CartModel>, layoutId: Int) :
        ParentRecyclerAdapter<CartModel>(context, data, layoutId) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParentRecyclerViewHolder {
        val itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false)
        return ViewHolder(itemView)
    }
    var TextView.textColor: Int
        get() = currentTextColor
        set(v) = setTextColor(v)



    override fun onBindViewHolder(holder: ParentRecyclerViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder
        val questionModel = data.get(position)
        viewHolder.name!!.setText(questionModel.name)
        Glide.with(mcontext).load(questionModel.image).into(viewHolder.image)

        viewHolder.productServicesAdapter = OrderServicesAdapter(mcontext,viewHolder.servicesModels, R.layout.recycle_order_services)
        viewHolder.services.layoutManager = LinearLayoutManager(mcontext,
                LinearLayoutManager.VERTICAL,false)
        viewHolder.services.adapter = viewHolder.productServicesAdapter
        viewHolder.productServicesAdapter.updateAll(questionModel.services!!)
        val animation = AnimationUtils.loadAnimation(mcontext, R.anim.item_animation_from_bottom_slow)
        animation.setDuration(250)
        viewHolder.itemView.startAnimation(animation)
        viewHolder.edit.setOnClickListener(View.OnClickListener { view -> onItemClickListener.onItemClick(view,position)

        })


    }
    inner class ViewHolder internal constructor(itemView: View) :
            ParentRecyclerViewHolder(itemView) {




        lateinit var productServicesAdapter: OrderServicesAdapter
        internal var servicesModels = ArrayList<ServicesModel>()
        internal var name=itemView.findViewById<TextView>(R.id.name)
        internal var image = itemView.findViewById<ImageView>(R.id.image)
        internal var services = itemView.findViewById<RecyclerView>(R.id.services)
        internal var edit = itemView.findViewById<ImageView>(R.id.edit)





    }
}