package com.aait.bubbles.UI.Activities.AppInfo

import android.content.Intent
import android.net.Uri
import android.util.Log
import android.widget.ImageView
import android.widget.TextView
import com.aait.bubbles.Base.ParentActivity
import com.aait.bubbles.Models.CallUsResponse
import com.aait.bubbles.Models.TermsResponse
import com.aait.bubbles.Network.Client
import com.aait.bubbles.Network.Service
import com.aait.bubbles.R
import com.aait.bubbles.Utils.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class CallUsActivity :ParentActivity(){
    override val layoutResource: Int
        get() = R.layout.activity_call_us
    lateinit var back:ImageView
    lateinit var title:TextView
    lateinit var email:TextView
    lateinit var phone:TextView
    lateinit var whats:TextView

    lateinit var twitter:ImageView
    lateinit var face:ImageView
    lateinit var insta:ImageView

    var twit = ""
    var facebook = ""
    var instagram = ""

    override fun initializeComponents() {
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        email = findViewById(R.id.email)
        phone = findViewById(R.id.phone)
        whats = findViewById(R.id.whats)

        twitter = findViewById(R.id.twitter)
        face = findViewById(R.id.face)
        insta = findViewById(R.id.insta)
        back.setOnClickListener { onBackPressed()
            finish() }
        title.text = getString(R.string.call_us)
        getData()

        face.setOnClickListener { if (facebook.startsWith("http"))
        {
            Log.e("here", "333")
            val i = Intent(Intent.ACTION_VIEW)
            i.data = Uri.parse(facebook)
            startActivity(i)

        } else {
            Log.e("here", "4444")
            val url = "https://$facebook"
            val i = Intent(Intent.ACTION_VIEW)
            i.data = Uri.parse(url)
            startActivity(i)
        }
        }

        twitter.setOnClickListener { if (!twit.equals(""))
        {
            if (twit.startsWith("http"))
            {
                Log.e("here", "333")
                val i = Intent(Intent.ACTION_VIEW)
                i.data = Uri.parse(twit)
                startActivity(i)

            } else {
                Log.e("here", "4444")
                val url = "https://$twit"
                val i = Intent(Intent.ACTION_VIEW)
                i.data = Uri.parse(url)
                startActivity(i)
            }
        } }
        insta.setOnClickListener { if (!instagram.equals(""))
        {
            if (instagram.startsWith("http"))
            {
                Log.e("here", "333")
                val i = Intent(Intent.ACTION_VIEW)
                i.data = Uri.parse(instagram)
                startActivity(i)

            } else {
                Log.e("here", "4444")
                val url = "https://$instagram"
                val i = Intent(Intent.ACTION_VIEW)
                i.data = Uri.parse(url)
                startActivity(i)
            }
        }  }

    }

    fun getData(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.CallUs(lang.appLanguage)?.enqueue(object:
            Callback<CallUsResponse> {
            override fun onFailure(call: Call<CallUsResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
            }

            override fun onResponse(call: Call<CallUsResponse>, response: Response<CallUsResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if(response.body()?.value.equals("1")){
                        email.text = response.body()?.data?.email!!
                        phone.text = response.body()?.data?.phone!!
                        whats.text = response.body()?.data?.whatsapp!!
                        for(i in 0..response.body()?.socials?.size!!-1){
                            if(response.body()?.socials?.get(i)?.name.equals("snapchat")){
                                facebook = response.body()?.socials?.get(i)?.link!!

                            }else if(response.body()?.socials?.get(i)?.name.equals("twitter")){
                                twit = response.body()?.socials?.get(i)?.link!!
                            }
                            else if(response.body()?.socials?.get(i)?.name.equals("instagram")){
                                instagram = response.body()?.socials?.get(i)?.link!!
                            }
                        }
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
}