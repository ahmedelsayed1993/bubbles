package com.aait.bubbles.UI.Activities.Auth

import android.content.Intent
import android.widget.Button
import android.widget.ImageView
import com.aait.bubbles.Base.ParentActivity
import com.aait.bubbles.R


class ChooseUserActivity:ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_choose_user
    lateinit var back:ImageView
     lateinit var client:Button
    lateinit var delegate:Button
    override fun initializeComponents() {
        back = findViewById(R.id.back)
        client = findViewById(R.id.user)
        delegate = findViewById(R.id.delegate)
        back.setOnClickListener { onBackPressed()
        finish()}
        client.setOnClickListener { startActivity(Intent(this,RegisterActivity::class.java)) }
        delegate.setOnClickListener { startActivity(Intent(this,DelegateRegisterActivity::class.java)) }

    }
}