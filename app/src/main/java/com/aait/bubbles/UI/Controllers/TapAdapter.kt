package com.aait.bubbles.UI.Controllers

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.aait.bubbles.R
import com.aait.bubbles.UI.Fragments.Delegate.CurrentFragment
import com.aait.bubbles.UI.Fragments.Delegate.FinishedFragment


class TapAdapter  (
        private val context: Context,
        fm: FragmentManager
) : FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        return if (position == 0) {
            CurrentFragment()
        } else {
            FinishedFragment()
        }

    }

    override fun getCount(): Int {
        return 2
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return if (position == 0) {
            context.getString(R.string.under_process)
        }else {
            context.getString(R.string.finished)
        }

    }
}
