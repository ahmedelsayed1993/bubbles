package com.aait.bubbles.UI.Fragments

import android.os.Bundle
import android.view.View
import androidx.viewpager.widget.ViewPager
import com.aait.bubbles.Base.BaseFragment
import com.aait.bubbles.R
import com.aait.bubbles.UI.Controllers.OrderTapAdapter
import com.google.android.material.tabs.TabLayout

class OrdersFragment:BaseFragment() {
    override val layoutResource: Int
        get() = R.layout.fragment_orders
    companion object {
        fun newInstance(): OrdersFragment {
            val args = Bundle()
            val fragment = OrdersFragment()
            fragment.setArguments(args)
            return fragment
        }
    }
    lateinit var orders: TabLayout
    lateinit var ordersViewPager: ViewPager
    private var mAdapter: OrderTapAdapter? = null
    override fun initializeComponents(view: View) {
        orders = view.findViewById(R.id.orders)
        ordersViewPager = view.findViewById(R.id.ordersViewPager)
        mAdapter = OrderTapAdapter(mContext!!,childFragmentManager)
        ordersViewPager.setAdapter(mAdapter)
        orders!!.setupWithViewPager(ordersViewPager)
    }
}