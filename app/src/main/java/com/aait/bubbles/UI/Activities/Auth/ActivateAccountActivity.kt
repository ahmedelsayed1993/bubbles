package com.aait.bubbles.UI.Activities.Auth

import android.content.Intent
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import com.aait.bubbles.Base.ParentActivity
import com.aait.bubbles.Models.UserModel
import com.aait.bubbles.Models.UserResponse
import com.aait.bubbles.Network.Client
import com.aait.bubbles.Network.Service
import com.aait.bubbles.R
import com.aait.bubbles.Utils.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ActivateAccountActivity:ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_activate_code
    lateinit var confirm: Button
    lateinit var code:EditText
    lateinit var resend: TextView

    lateinit var userModel: UserModel

    override fun initializeComponents() {
        userModel = intent.getSerializableExtra("user") as UserModel
        confirm = findViewById(R.id.confirm)
        code = findViewById(R.id.code)

        resend = findViewById(R.id.resend)


        confirm.setOnClickListener {
            if (CommonUtil.checkEditError(code,getString(R.string.activation_code))){
                return@setOnClickListener
            }else{

                check()

            } }
        resend.setOnClickListener { Resend() }


    }
    fun check(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.CheckCode("Bearer "+userModel.token!!,code.text.toString(),lang.appLanguage)?.enqueue(object :
                Callback<UserResponse> {
            override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
            }

            override fun onResponse(call: Call<UserResponse>, response: Response<UserResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
//                        user.loginStatus = true
//                        user.userData = response.body()?.data!!
                        val intent = Intent(this@ActivateAccountActivity, LoginActivity::class.java)
                        intent.putExtra("type",response.body()?.data?.user_type)
                        startActivity(intent)
                        finish()
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }
        })
    }

    fun Resend(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Resend("Bearer "+userModel.token!!,lang.appLanguage)?.enqueue(object :
                Callback<UserResponse> {
            override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                    call: Call<UserResponse>,
                    response: Response<UserResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }

}