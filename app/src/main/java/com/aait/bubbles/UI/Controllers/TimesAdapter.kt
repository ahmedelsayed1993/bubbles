package com.aait.bubbles.UI.Controllers

import android.content.Context
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.TextView
import androidx.annotation.RequiresApi
import com.aait.bubbles.Base.ParentRecyclerAdapter
import com.aait.bubbles.Base.ParentRecyclerViewHolder
import com.aait.bubbles.Models.SubCatModel
import com.aait.bubbles.Models.TimeModel
import com.aait.bubbles.R

class TimesAdapter (context: Context, data: MutableList<TimeModel>, layoutId: Int) :
    ParentRecyclerAdapter<TimeModel>(context, data, layoutId) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParentRecyclerViewHolder {
        val itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false)
        return ViewHolder(itemView)
    }
    var TextView.textColor: Int
        get() = currentTextColor
        set(v) = setTextColor(v)
    var selected :Int = 0

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onBindViewHolder(holder: ParentRecyclerViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder
        val questionModel = data.get(position)
        viewHolder.name!!.setText(questionModel.time)
        questionModel.selected = (selected==position)
        if(questionModel.selected!!){
            viewHolder.name.background = mcontext.getDrawable(R.drawable.babay_blue_button)
            viewHolder.name.textColor = mcontext.resources.getColor(R.color.colorWhite)
        }else{
            viewHolder.name.background = mcontext.getDrawable(R.drawable.gray_shape)
            viewHolder.name.textColor = mcontext.resources.getColor(R.color.colorPrimary)
        }
        val animation = AnimationUtils.loadAnimation(mcontext, R.anim.item_animation_from_right)
        animation.setDuration(250)
        viewHolder.itemView.startAnimation(animation)

        viewHolder.name.setOnClickListener(View.OnClickListener { view -> onItemClickListener.onItemClick(view,position)

        })




    }
    inner class ViewHolder internal constructor(itemView: View) :
        ParentRecyclerViewHolder(itemView) {





        internal var name=itemView.findViewById<TextView>(R.id.time)





    }
}