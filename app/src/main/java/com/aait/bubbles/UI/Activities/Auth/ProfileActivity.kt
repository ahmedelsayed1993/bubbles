package com.aait.bubbles.UI.Activities.Auth

import android.app.Dialog
import android.content.Intent
import android.os.Build
import android.view.Gravity
import android.view.ViewGroup
import android.view.Window
import android.widget.*
import com.aait.bubbles.Base.ParentActivity
import com.aait.bubbles.Models.BaseResponse
import com.aait.bubbles.Models.UserResponse
import com.aait.bubbles.Network.Client
import com.aait.bubbles.Network.Service
import com.aait.bubbles.R
import com.aait.bubbles.UI.Activities.LocationActivity
import com.aait.bubbles.Utils.CommonUtil
import com.aait.bubbles.Utils.PermissionUtils
import com.bumptech.glide.Glide
import com.fxn.pix.Options
import com.fxn.pix.Pix
import com.fxn.utility.ImageQuality
import de.hdodenhof.circleimageview.CircleImageView
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File


class ProfileActivity :ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_profile
    var lat = ""
    var lng = ""
    var result = ""
    lateinit var pass:TextView
    lateinit var save: Button
    lateinit var phone: EditText
    lateinit var name: EditText
    lateinit var email: EditText
    lateinit var location: TextView
    lateinit var image:CircleImageView
    lateinit var title:TextView
    lateinit var back:ImageView
    internal var returnValue: java.util.ArrayList<String>? = java.util.ArrayList()
    internal var options = Options.init()
            .setRequestCode(100)                                                 //Request code for activity results
            .setCount(1)                                                         //Number of images to restict selection count
            .setFrontfacing(false)                                                //Front Facing camera on start
            .setImageQuality(ImageQuality.HIGH)                                  //Image Quality
            .setPreSelectedUrls(returnValue)                                     //Pre selected Image Urls
            .setScreenOrientation(Options.SCREEN_ORIENTATION_PORTRAIT)           //Orientaion
            .setPath("/pix/images")
    private var ImageBasePath: String? = null

    override fun initializeComponents() {
        title = findViewById(R.id.title)
        back = findViewById(R.id.back)
        save = findViewById(R.id.save)
        phone = findViewById(R.id.phone)
        name = findViewById(R.id.name)
        email = findViewById(R.id.email)
        location = findViewById(R.id.location)
        image = findViewById(R.id.image)
        pass = findViewById(R.id.change_pass)
        title.text = getString(R.string.profile)
        back.setOnClickListener { onBackPressed()
        finish()}
        location.setOnClickListener {
            startActivityForResult(Intent(this, LocationActivity::class.java),1)
        }
        pass.setOnClickListener {
            val dialog = Dialog(mContext)
            dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog?.window!!.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
            dialog ?.setCancelable(true)
            dialog ?.setContentView(R.layout.dialog_change_password)
            val old_pass = dialog?.findViewById<EditText>(R.id.old_pass)
            val new_pass = dialog?.findViewById<EditText>(R.id.new_pass)
            val confirm_pass = dialog?.findViewById<EditText>(R.id.confirm_pass)

            val save = dialog?.findViewById<Button>(R.id.confirm)


            save?.setOnClickListener {
                if (CommonUtil.checkEditError(old_pass,getString(R.string.old_password))||
                        CommonUtil.checkEditError(new_pass,getString(R.string.new_password))||
                        CommonUtil.checkLength(new_pass,getString(R.string.password_length),6)||
                        CommonUtil.checkEditError(confirm_pass,getString(R.string.confirm_password))){
                    return@setOnClickListener
                }else{
                    if (!new_pass.text.toString().equals(confirm_pass.text.toString())){
                        confirm_pass.error = getString(R.string.password_not_match)
                    }else{
                        showProgressDialog(getString(R.string.please_wait))
                        Client.getClient()?.create(Service::class.java)?.resetPassword(lang.appLanguage,"Bearer "+user.userData.token,old_pass.text.toString(),new_pass.text.toString())?.enqueue(
                                object :Callback<BaseResponse>{
                                    override fun onFailure(call: Call<BaseResponse>, t: Throwable) {
                                        CommonUtil.handleException(mContext,t)
                                        t.printStackTrace()
                                        hideProgressDialog()
                                    }

                                    override fun onResponse(
                                            call: Call<BaseResponse>,
                                            response: Response<BaseResponse>
                                    ) {
                                        hideProgressDialog()
                                        if (response.isSuccessful){
                                            if (response.body()?.value.equals("1")){
                                                CommonUtil.makeToast(mContext,response.body()?.msg!!)
                                                dialog?.dismiss()
                                            }else if(response.body()?.value.equals("401")){
                                                user.loginStatus=false
                                                user.Logout()
                                                startActivity(Intent(this@ProfileActivity, LoginActivity::class.java))
                                            }else{
                                                CommonUtil.makeToast(mContext,response.body()?.msg!!)

                                            }
                                        }
                                    }
                                }
                        )
                    }
                }

            }
            dialog?.show()
        }

        save.setOnClickListener {  if(CommonUtil.checkEditError(name,getString(R.string.enter_name))||
                CommonUtil.checkEditError(phone,getString(R.string.enter_phone))||
                CommonUtil.checkLength(phone,getString(R.string.phone_length),9)||
                CommonUtil.checkEditError(email,getString(R.string.email))||
                ! CommonUtil.isEmailValid(email,getString(R.string.correct_email))||
                CommonUtil.checkTextError(location,getString(R.string.enter_location))){
            return@setOnClickListener
        } else{
            showProgressDialog(getString(R.string.please_wait))
            Client.getClient()?.create(Service::class.java)?.Edit("Bearer "+user.userData.token!!,lang.appLanguage,name.text.toString(),phone.text.toString(),email.text.toString()
                    ,lat,lng,location.text.toString())
                    ?.enqueue(object: Callback<UserResponse> {
                        override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                            CommonUtil.handleException(mContext,t)
                            t.printStackTrace()
                            hideProgressDialog()
                        }

                        override fun onResponse(
                                call: Call<UserResponse>,
                                response: Response<UserResponse>
                        ) {
                            hideProgressDialog()
                            if (response.isSuccessful){
                                if(response.body()?.value.equals("1")){
                                    CommonUtil.makeToast(mContext!!,getString(R.string.data_updated))
                                    user.userData = response.body()?.data!!
                                    Glide.with(mContext).asBitmap().load(response.body()?.data?.avatar).into(image)
                                    name.setText( response.body()?.data?.name)
                                    // user_name.text = response.body()?.data?.name
                                    phone.setText(response.body()?.data?.phone)
                                    email.setText(response.body()?.data?.email)
                                    lat = response.body()?.data?.lat!!
                                    lng = response.body()?.data?.lng!!
                                    result = response.body()?.data?.address!!
                                    location.text = response.body()?.data?.address

                                }else{
                                    CommonUtil.makeToast(mContext,response.body()?.msg!!)
                                }
                            }
                        }

                    })
        }
        }
        image.setOnClickListener {
            if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
                if (!(PermissionUtils.hasPermissions(mContext,
                                android.Manifest.permission.CAMERA)&& PermissionUtils.hasPermissions(mContext,
                                android.Manifest.permission.READ_EXTERNAL_STORAGE)&& PermissionUtils.hasPermissions(mContext,
                                android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                )) {
                    CommonUtil.PrintLogE("Permission not granted")
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(
                                PermissionUtils.IMAGE_PERMISSIONS,
                                400
                        )
                    }
                } else {
                    Pix.start(this, options)
                    CommonUtil.PrintLogE("Permission is granted before")
                }
            } else {
                CommonUtil.PrintLogE("SDK minimum than 23")
                Pix.start(this, options)
            }
        }
        getData()

    }

    fun getData(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Edit("Bearer "+user.userData.token!!,lang.appLanguage,null,null,null
        ,null,null,null)
                ?.enqueue(object: Callback<UserResponse> {
                    override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                        CommonUtil.handleException(mContext,t)
                        t.printStackTrace()
                        hideProgressDialog()
                    }

                    override fun onResponse(
                            call: Call<UserResponse>,
                            response: Response<UserResponse>
                    ) {
                        hideProgressDialog()
                        if (response.isSuccessful){
                            if(response.body()?.value.equals("1")){
                                user.userData = response.body()?.data!!
                                 Glide.with(mContext).asBitmap().load(response.body()?.data?.avatar).into(image)
                                name.setText( response.body()?.data?.name)
                                // user_name.text = response.body()?.data?.name
                                phone.setText(response.body()?.data?.phone)
                                email.setText(response.body()?.data?.email)
                                lat = response.body()?.data?.lat!!
                                lng = response.body()?.data?.lng!!
                                result = response.body()?.data?.address!!
                                location.text = response.body()?.data?.address

                            }else{
                                CommonUtil.makeToast(mContext,response.body()?.msg!!)
                            }
                        }
                    }

                })
    }

    fun upLoad(path:String){
        showProgressDialog(getString(R.string.please_wait))
        var filePart: MultipartBody.Part? = null
        val ImageFile = File(path)
        val fileBody = RequestBody.create(MediaType.parse("*/*"), ImageFile)
        filePart = MultipartBody.Part.createFormData("avatar", ImageFile.name, fileBody)
        Client.getClient()?.create(Service::class.java)?.AddImage("Bearer "+user.userData.token,lang.appLanguage,filePart)?.enqueue(object :Callback<UserResponse>{
            override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(call: Call<UserResponse>, response: Response<UserResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        CommonUtil.makeToast(mContext!!,getString(R.string.data_updated))
                        user.userData = response.body()?.data!!
                        Glide.with(mContext).asBitmap().load(response.body()?.data?.avatar).into(image)
                        name.setText( response.body()?.data?.name)
                        // user_name.text = response.body()?.data?.name
                        phone.setText(response.body()?.data?.phone)
                        email.setText(response.body()?.data?.email)
                        lat = response.body()?.data?.lat!!
                        lng = response.body()?.data?.lng!!
                        result = response.body()?.data?.address!!
                        location.text = response.body()?.data?.address
                    }else if(response.body()?.value.equals("401")){
                        user.loginStatus=false
                        user.Logout()
                        startActivity(Intent(this@ProfileActivity, LoginActivity::class.java))
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 100) {
            if (resultCode == 0) {

            } else {
                returnValue = data!!.getStringArrayListExtra(Pix.IMAGE_RESULTS)

                ImageBasePath = returnValue!![0]

                Glide.with(mContext).load(ImageBasePath).into(image)

                if (ImageBasePath != null) {
                    upLoad(ImageBasePath!!)
                    //ID.text = getString(R.string.Image_attached)
                }
            }
        }else{
            if (resultCode == 1) {
                if (data?.getStringExtra("result") != null) {
                    result = data?.getStringExtra("result").toString()
                    lat = data?.getStringExtra("lat").toString()
                    lng = data?.getStringExtra("lng").toString()
                    location.text = result
                } else {
                    result = ""
                    lat = ""
                    lng = ""
                    location.text = ""
                }
            }
        }
    }
}