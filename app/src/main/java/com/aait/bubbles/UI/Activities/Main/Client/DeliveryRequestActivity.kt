package com.aait.bubbles.UI.Activities.Main.Client

import android.Manifest
import android.content.DialogInterface
import android.content.Intent
import android.location.Address
import android.location.Geocoder
import android.os.Build
import android.provider.Settings
import android.util.Log
import android.view.View
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aait.bubbles.Base.ParentActivity
import com.aait.bubbles.GPS.GPSTracker
import com.aait.bubbles.GPS.GpsTrakerListener
import com.aait.bubbles.Listeners.OnItemClickListener
import com.aait.bubbles.Models.*
import com.aait.bubbles.Network.Client
import com.aait.bubbles.Network.Service
import com.aait.bubbles.R
import com.aait.bubbles.UI.Controllers.AdditionAdapter
import com.aait.bubbles.UI.Controllers.CartProductAdapter
import com.aait.bubbles.UI.Controllers.ExtraAdapter
import com.aait.bubbles.UI.Controllers.TimesAdapter
import com.aait.bubbles.Utils.CommonUtil
import com.aait.bubbles.Utils.DialogUtil
import com.aait.bubbles.Utils.PermissionUtils
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.Marker
import com.google.gson.Gson
import com.vivekkaushik.datepicker.DatePickerTimeline
import com.vivekkaushik.datepicker.OnDateSelectedListener
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.*
import kotlin.collections.ArrayList

class DeliveryRequestActivity:ParentActivity() ,OnItemClickListener, GpsTrakerListener {
    override val layoutResource: Int
        get() = R.layout.activity_request_delivery
    private var mAlertDialog: AlertDialog? = null
    internal  var googleMap: GoogleMap?=null
    internal lateinit var myMarker: Marker
    internal lateinit var geocoder: Geocoder
    internal lateinit var gps: GPSTracker
    internal var startTracker = false
    var mLang = ""
    var mLat = ""
    var result = ""
    var category = 0
    lateinit var myAddressModel: MyAddressModel
    lateinit var location_lay: LinearLayout
    lateinit var location: TextView
    lateinit var receive_date: TextView
    lateinit var edit_start: TextView
    lateinit var process: Button
    lateinit var back: ImageView
    lateinit var title: TextView
    lateinit var time_lay: LinearLayout
    lateinit var datePickerTimeline: DatePickerTimeline
    lateinit var times_list: RecyclerView
    lateinit var timelinearLayoutManager: LinearLayoutManager
    var times = ArrayList<TimeModel>()
    lateinit var timesAdapter: TimesAdapter
    var time = ""
    var date = ""
    var state = ""
    lateinit var timeModel: TimeModel
    lateinit var confirm: Button
    lateinit var notes:EditText
    lateinit var bill_number:EditText
    @RequiresApi(Build.VERSION_CODES.O)
    override fun initializeComponents() {
        category = intent.getIntExtra("cat",0)
        location_lay = findViewById(R.id.location_lay)
        location = findViewById(R.id.location)
        receive_date = findViewById(R.id.receive_date)
        edit_start = findViewById(R.id.edit_start)
        notes = findViewById(R.id.notes)
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        process = findViewById(R.id.process)
        bill_number = findViewById(R.id.bill_number)
        time_lay = findViewById(R.id.time_lay)
        datePickerTimeline = findViewById(R.id.datePickerTimeline)
        times_list = findViewById(R.id.times_list)
        confirm = findViewById(R.id.confirm)
        back.setOnClickListener { onBackPressed()
            finish()}
        title.text = getString(R.string.basket)
        timelinearLayoutManager = LinearLayoutManager(mContext,LinearLayoutManager.HORIZONTAL,false)
        timesAdapter = TimesAdapter(mContext,ArrayList<TimeModel>(),R.layout.recycle_times)
        timesAdapter.setOnItemClickListener(this)
        times_list.layoutManager = timelinearLayoutManager
        times_list.adapter = timesAdapter
        times.add(TimeModel("00:00"+getString(R.string.am),"00:00"))
        times.add(TimeModel("01:00"+getString(R.string.am),"01:00"))
        times.add(TimeModel("02:00"+getString(R.string.am),"02:00"))
        times.add(TimeModel("03:00"+getString(R.string.am),"03:00"))
        times.add(TimeModel("04:00"+getString(R.string.am),"04:00"))
        times.add(TimeModel("05:00"+getString(R.string.am),"05:00"))
        times.add(TimeModel("06:00"+getString(R.string.am),"06:00"))
        times.add(TimeModel("07:00"+getString(R.string.am),"07:00"))
        times.add(TimeModel("08:00"+getString(R.string.am),"08:00"))
        times.add(TimeModel("09:00"+getString(R.string.am),"09:00"))
        times.add(TimeModel("10:00"+getString(R.string.am),"10:00"))
        times.add(TimeModel("11:00"+getString(R.string.am),"11:00"))
        times.add(TimeModel("12:00"+getString(R.string.am),"12:00"))
        times.add(TimeModel("01:00"+getString(R.string.pm),"13:00"))
        times.add(TimeModel("02:00"+getString(R.string.pm),"14:00"))
        times.add(TimeModel("03:00"+getString(R.string.pm),"15:00"))
        times.add(TimeModel("04:00"+getString(R.string.pm),"16:00"))
        times.add(TimeModel("05:00"+getString(R.string.pm),"17:00"))
        times.add(TimeModel("06:00"+getString(R.string.pm),"18:00"))
        times.add(TimeModel("07:00"+getString(R.string.pm),"19:00"))
        times.add(TimeModel("08:00"+getString(R.string.pm),"20:00"))
        times.add(TimeModel("09:00"+getString(R.string.pm),"21:00"))
        times.add(TimeModel("10:00"+getString(R.string.pm),"22:00"))
        times.add(TimeModel("11:00"+getString(R.string.pm),"23:00"))
        times.add(TimeModel("12:00"+getString(R.string.pm),"24:00"))
        timeModel = times.get(0)
        receive_date.text = (LocalDateTime.now()).format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm"))
        date = (LocalDateTime.now()).format(DateTimeFormatter.ofPattern("yyyy-MM-dd"))
        time = (LocalDateTime.now()).format(DateTimeFormatter.ofPattern("HH:mm"))

        getLocationWithPermission(null)
        datePickerTimeline.setInitialDate(Calendar.getInstance().get(Calendar.YEAR), Calendar.getInstance().get(Calendar.MONTH), Calendar.getInstance().get(Calendar.DAY_OF_MONTH))
// Set a date Selected Listener
// Set a date Selected Listener
        datePickerTimeline.setOnDateSelectedListener(object : OnDateSelectedListener {
            override fun onDateSelected(year: Int, month: Int, day: Int, dayOfWeek: Int) {
                Log.e("date",year.toString()+"-"+(month+1).toString()+"-"+day)
                date = year.toString()+"-"+(month+1).toString()+"-"+day
                // Do Something
            }

            override fun onDisabledDateSelected(
                    year: Int,
                    month: Int,
                    day: Int,
                    dayOfWeek: Int,
                    isDisabled: Boolean
            ) {
                // Do Something
            }
        })

// Disable date

// Disable date
        val dates: Array<Date> = arrayOf<Date>(Calendar.getInstance().getTime())
        datePickerTimeline.deactivateDates(dates)
        process.setOnClickListener {
           if (CommonUtil.checkEditError(bill_number,getString(R.string.bill_number))){
               return@setOnClickListener
           }else{
               showProgressDialog(getString(R.string.please_wait))
               Client.getClient()?.create(Service::class.java)?.RequestDelivery("Bearer"+user.userData.token,lang.appLanguage
               ,category,bill_number.text.toString(),result,mLat,mLang,date,time,notes.text.toString())?.enqueue(
                   object :
                       Callback<TermsResponse> {
                       override fun onResponse(
                           call: Call<TermsResponse>,
                           response: Response<TermsResponse>
                       ) {
                           hideProgressDialog()
                           if (response.isSuccessful){
                               if (response.body()?.value.equals("1")){
                                   CommonUtil.makeToast(mContext,response.body()?.data!!)
                                   startActivity(Intent(this@DeliveryRequestActivity,BackToMainActivity::class.java))
                                   finish()
                               }else{
                                   CommonUtil.makeToast(mContext,response.body()?.msg!!)
                               }
                           }
                       }

                       override fun onFailure(call: Call<TermsResponse>, t: Throwable) {
                           hideProgressDialog()
                           CommonUtil.handleException(mContext,t)
                           t.printStackTrace()
                       }

                   })
           }
        }
        edit_start.setOnClickListener { state = "start"
            timesAdapter.updateAll(times)
            time_lay.visibility = View.VISIBLE}

        confirm.setOnClickListener {
            time_lay.visibility = View.GONE
            if (state.equals("start")){
                receive_date.text = date+" "+timeModel.actual
            }
        }
        location_lay.setOnClickListener { startActivityForResult(Intent(this,MyAddressesActivity::class.java),1) }


    }

    override fun onItemClick(view: View, position: Int) {
        if (view.id == R.id.time){
            timesAdapter.selected = position
            times.get(position).selected = true
            timeModel = times.get(position)
            timesAdapter.notifyDataSetChanged()
        }
    }

    override fun onTrackerSuccess(lat: Double?, log: Double?) {
        if (startTracker) {
            if (lat != 0.0 && log != 0.0) {
                hideProgressDialog()
                // putMapMarker(lat, log)
            }
        }
    }

    override fun onStartTracker() {
        startTracker = true
    }
    fun getLocationWithPermission(cat:Int?) {
        gps = GPSTracker(mContext!!, this)
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!(PermissionUtils.hasPermissions(mContext, Manifest.permission.ACCESS_FINE_LOCATION)&&
                            (PermissionUtils.hasPermissions(mContext,
                                    Manifest.permission.ACCESS_COARSE_LOCATION)))) {
                CommonUtil.PrintLogE("Permission not granted")
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(
                            PermissionUtils.GPS_PERMISSION,
                            800
                    )
                    Log.e("GPS", "1")
                }
            } else {
                getCurrentLocation(cat)
                Log.e("GPS", "2")
            }
        } else {
            Log.e("GPS", "3")
            getCurrentLocation(cat)
        }

    }

    internal fun getCurrentLocation(cat:Int?) {
        gps.getLocation()
        if (!gps.canGetLocation()) {
            mAlertDialog = DialogUtil.showAlertDialog(mContext!!,
                    getString(R.string.gps_detecting),
                    DialogInterface.OnClickListener { dialogInterface, i ->
                        mAlertDialog?.dismiss()
                        val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                        startActivityForResult(intent, 300)
                    })
        } else {
            if (gps.getLatitude() !== 0.0 && gps.getLongitude() !== 0.0) {
                // putMapMarker(gps.getLatitude(), gps.getLongitude())

                mLat = gps.getLatitude().toString()
                mLang = gps.getLongitude().toString()
                val addresses: List<Address>
                geocoder = Geocoder(mContext!!, Locale.getDefault())
                try {
                    addresses = geocoder.getFromLocation(
                            java.lang.Double.parseDouble(mLat),
                            java.lang.Double.parseDouble(mLang),
                            1
                    )
                    if (addresses.isEmpty()) {
                        Toast.makeText(
                                mContext,
                                resources.getString(R.string.detect_location),
                                Toast.LENGTH_SHORT
                        ).show()
                    } else {
                        result = addresses[0].getAddressLine(0)
                        Log.e("address",result)
                       // getData(gps.getLatitude().toString(), gps.getLongitude().toString(),result)
                        location.text = result
                        // CommonUtil.makeToast(mContext,addresses[0].getAddressLine(0))



                    }

                } catch (e: IOException) {
                }


                //putMapMarker(gps.getLatitude(), gps.getLongitude())


            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == 1) {
            if (data?.getStringExtra("result") != null) {
                result = data?.getStringExtra("result")!!
                mLat = data?.getStringExtra("lat")!!
                mLang = data?.getStringExtra("lng")!!
                location.text = result
            } else {
                result = result
                mLat = mLat
                mLang = mLang
                location.text = result
            }
        }
    }
}