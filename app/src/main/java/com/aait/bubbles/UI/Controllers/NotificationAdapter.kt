package com.aait.bubbles.UI.Controllers

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RatingBar
import android.widget.TextView
import com.aait.bubbles.Base.ParentRecyclerAdapter
import com.aait.bubbles.Base.ParentRecyclerViewHolder

import com.aait.bubbles.Models.NotificationModel
import com.aait.bubbles.R
import com.bumptech.glide.Glide
import com.daimajia.swipe.SwipeLayout
import de.hdodenhof.circleimageview.CircleImageView

class NotificationAdapter (context: Context, data: MutableList<NotificationModel>, layoutId: Int) :
    ParentRecyclerAdapter<NotificationModel>(context, data, layoutId) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParentRecyclerViewHolder {
        val itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false)
        return ViewHolder(itemView)
    }
    var TextView.textColor: Int
        get() = currentTextColor
        set(v) = setTextColor(v)



    override fun onBindViewHolder(holder: ParentRecyclerViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder
        val questionModel = data.get(position)
        viewHolder.content!!.setText(questionModel.content)
        viewHolder.time.text = questionModel.created


        // viewHolder.itemView.animation = mcontext.resources.
        // val animation = mcontext.resources.getAnimation(R.anim.item_animation_from_right)
//        val animation = AnimationUtils.loadAnimation(mcontext, R.anim.item_animation_from_right)
//        animation.setDuration(750)
//        viewHolder.itemView.startAnimation(animation)
        viewHolder.swipe.addDrag(
            SwipeLayout.DragEdge.Right,
            viewHolder.swipe.findViewById(R.id.bottom_wraper)
        )
        viewHolder.swipe.addSwipeListener(object : SwipeLayout.SwipeListener {
            override fun onStartOpen(layout: SwipeLayout) {

            }

            override fun onOpen(layout: SwipeLayout) {

            }

            override fun onStartClose(layout: SwipeLayout) {

            }

            override fun onClose(layout: SwipeLayout) {

            }

            override fun onUpdate(layout: SwipeLayout, leftOffset: Int, topOffset: Int) {

            }

            override fun onHandRelease(layout: SwipeLayout, xvel: Float, yvel: Float) {

            }
        })
        viewHolder.delete.setOnClickListener(View.OnClickListener { view ->
            onItemClickListener.onItemClick(
                view,
                position
            ) })


        viewHolder.lay.setOnClickListener(View.OnClickListener { view -> onItemClickListener.onItemClick(view,position)

        })




    }
    inner class ViewHolder internal constructor(itemView: View) :
        ParentRecyclerViewHolder(itemView) {





        internal var content=itemView.findViewById<TextView>(R.id.content)
        internal var lay = itemView.findViewById<LinearLayout>(R.id.lay)
        internal var time = itemView.findViewById<TextView>(R.id.time)
        internal var swipe=itemView.findViewById<SwipeLayout>(R.id.swipe)
        internal var delete=itemView.findViewById<TextView>(R.id.delete)




    }
}