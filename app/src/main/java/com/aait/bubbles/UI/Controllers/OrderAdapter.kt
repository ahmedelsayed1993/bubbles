package com.aait.bubbles.UI.Controllers

import android.content.Context
import android.content.Intent
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.RequiresApi
import com.aait.bubbles.Base.ParentRecyclerAdapter
import com.aait.bubbles.Base.ParentRecyclerViewHolder
import com.aait.bubbles.Models.OrderModel
import com.aait.bubbles.Models.ServicesModel
import com.aait.bubbles.Models.TermsResponse
import com.aait.bubbles.Network.Client
import com.aait.bubbles.Network.Service
import com.aait.bubbles.R
import com.aait.bubbles.UI.Activities.Main.Client.MainActivity
import com.aait.bubbles.Utils.CommonUtil
import com.bumptech.glide.Glide
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class OrderAdapter  (context: Context, data: MutableList<OrderModel>, layoutId: Int) :
        ParentRecyclerAdapter<OrderModel>(context, data, layoutId) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParentRecyclerViewHolder {
        val itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false)
        return ViewHolder(itemView)
    }
    var TextView.textColor: Int
        get() = currentTextColor
        set(v) = setTextColor(v)


    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onBindViewHolder(holder: ParentRecyclerViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder
        val questionModel = data.get(position)
        viewHolder.num!!.setText(mcontext.getString(R.string.order_number)+questionModel.id.toString())
        viewHolder.price.text = questionModel.total+mcontext.getString(R.string.rs)
        viewHolder.time.text = mcontext.getString(R.string.order_time)+ questionModel.time
        viewHolder.date.text = mcontext.getString(R.string.order_date)+questionModel.date
        viewHolder.image.setImageResource(R.mipmap.logoo)
        viewHolder.itemView.setOnClickListener(View.OnClickListener {
            view -> onItemClickListener.onItemClick(view,position)

        })
    }
    inner class ViewHolder internal constructor(itemView: View) :
            ParentRecyclerViewHolder(itemView) {

        internal var image=itemView.findViewById<ImageView>(R.id.image)
        internal var num=itemView.findViewById<TextView>(R.id.num)
        internal var price=itemView.findViewById<TextView>(R.id.price)
        internal var date=itemView.findViewById<TextView>(R.id.date)
        internal var time = itemView.findViewById<TextView>(R.id.time)

    }
}